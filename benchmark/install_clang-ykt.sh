#!/bin/bash

PATH_SOURCES=$PWD/ibm/sources
PATH_BUILD=$PWD/ibm/build
PATH_CLANG_YKT=$PWD/ibm/clang-ykt

echo "Creating directories $PATH_SOURCES, $PATH_BUILD and $PATH_CLANG_YKT"
mkdir -p $PATH_SOURCES
mkdir -p $PATH_BUILD
# Compiler will be installed in this directory.
mkdir -p $PATH_CLANG_YKT

echo "Clone sources"
#LLVM
if [ ! -d "$PATH_SOURCES/llvm" ] ; then
    echo "Clone llvm"
    cd $PATH_SOURCES && git clone https://github.com/clang-ykt/llvm
else
    echo "Pull llvm"
    cd "$PATH_SOURCES/llvm"
    git pull https://github.com/clang-ykt/llvm
fi

#CLANG
if [ ! -d "$PATH_SOURCES/llvm/tools/clang" ] ; then
    echo "Clone clang"
    cd $PATH_SOURCES/llvm/tools && git clone https://github.com/clang-ykt/clang
else
    echo "Pull llvm"
    cd "$PATH_SOURCES/llvm/tools/clang"
    git pull https://github.com/clang-ykt/clang
fi

#openMP
if [ ! -d "$PATH_SOURCES/llvm/projects/openmp" ] ; then
    echo "Clone openmp"
    cd $PATH_SOURCES/llvm/projects && git clone https://github.com/clang-ykt/openmp
else
    echo "Pull llvm"
    cd "$PATH_SOURCES/llvm/projects/openmp"
    git pull https://github.com/clang-ykt/openmp
fi


echo "Build Clang-ykt"
cd $PATH_BUILD
# Nvidia Jetyon TX2 has Compute capability 6.2 --> SM62
cmake  -DCMAKE_BUILD_TYPE=RELEASE \
    -DCMAKE_INSTALL_PREFIX=$PATH_CLANG_YKT \
    -DLLVM_ENABLE_BACKTRACES=ON \
    -DLLVM_ENABLE_WERROR=OFF \
    -DBUILD_SHARED_LIBS=OFF \
    -DLLVM_ENABLE_RTTI=ON \
    -DOPENMP_ENABLE_LIBOMPTARGET=ON \
    -DCMAKE_C_FLAGS='-DOPENMP_NVPTX_COMPUTE_CAPABILITY=62' \
    -DCMAKE_CXX_FLAGS='-DOPENMP_NVPTX_COMPUTE_CAPABILITY=62' \
    -DLIBOMPTARGET_NVPTX_COMPUTE_CAPABILITY=62 \
    -DCLANG_OPENMP_NVPTX_DEFAULT_ARCH=sm_62 \
    -DLIBOMPTARGET_NVPTX_ENABLE_BCLIB=true \
    -G \
    Ninja \
    ../sources/llvm

ninja
