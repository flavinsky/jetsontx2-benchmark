

ompoffload gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            4.171E-04        9.987E-04           5.162E-03        3.942E-02             3.191E-01
Conf. 95% [s]       7.202E-06        5.900E-05           8.502E-05        6.510E-05             2.242E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      4.171E-04 &       9.987E-04 &          5.162E-03 &       3.942E-02 &            3.191E-01 \\
 Conf. 95\% [s] &      7.202E-06 &       5.900E-05 &          8.502E-05 &       6.510E-05 &            2.242E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload restrict gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            3.897E-04        7.525E-04           3.636E-03        2.640E-02             2.147E-01
Conf. 95% [s]       9.279E-06        4.744E-06           5.755E-05        3.732E-05             6.972E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      3.897E-04 &       7.525E-04 &          3.636E-03 &       2.640E-02 &            2.147E-01 \\
 Conf. 95\% [s] &      9.279E-06 &       4.744E-06 &          5.755E-05 &       3.732E-05 &            6.972E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            3.677E-04        7.378E-04           3.619E-03        2.653E-02             2.097E-01
Conf. 95% [s]       3.601E-06        1.042E-05           1.181E-05        3.319E-05             8.777E-05
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      3.677E-04 &       7.378E-04 &          3.619E-03 &       2.653E-02 &            2.097E-01 \\
 Conf. 95\% [s] &      3.601E-06 &       1.042E-05 &          1.181E-05 &       3.319E-05 &            8.777E-05 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda restrict gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            3.680E-04        7.319E-04           3.578E-03        2.626E-02             2.073E-01
Conf. 95% [s]       3.652E-06        4.993E-06           1.493E-05        3.926E-05             8.636E-05
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      3.680E-04 &       7.319E-04 &          3.578E-03 &       2.626E-02 &            2.073E-01 \\
 Conf. 95\% [s] &      3.652E-06 &       4.993E-06 &          1.493E-05 &       3.926E-05 &            8.636E-05 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload gemm / ompoffload gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            100.03          100.35              100.027         100                  100
Conf. 95% [%]          2.4426          8.37656             2.32986         0.233531             0.0993369
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       100.03   &       100.35    &          100.027   &      100        &          100         \\
 Conf. 95\% [\%] &         2.4426 &         8.37656 &            2.32986 &        0.233531 &            0.0993369 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload gemm / ompoffload restrict gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           107.081          132.716             142             149.329               148.666
Conf. 95% [%]         3.14926          7.88471             3.24341         0.324599              0.494062
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      107.081   &       132.716   &          142       &      149.329    &           148.666    \\
 Conf. 95\% [\%] &        3.14926 &         7.88471 &            3.24341 &        0.324599 &             0.494062 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload gemm / cuda gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           113.447          135.389             142.63          148.605               152.174
Conf. 95% [%]         2.25206          8.22271             2.39498         0.307862              0.124427
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      113.447   &       135.389   &          142.63    &      148.605    &           152.174    \\
 Conf. 95\% [\%] &        2.25206 &         8.22271 &            2.39498 &        0.307862 &             0.124427 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload gemm / cuda restrict gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           113.337          136.449             144.269         150.129               153.922
Conf. 95% [%]         2.25721          8.11425             2.45134         0.334414              0.125702
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      113.337   &       136.449   &          144.269   &      150.129    &           153.922    \\
 Conf. 95\% [\%] &        2.25721 &         8.11425 &            2.45134 &        0.334414 &             0.125702 \\
\hline
\end{tabular}
----------------------------------------------------------------
