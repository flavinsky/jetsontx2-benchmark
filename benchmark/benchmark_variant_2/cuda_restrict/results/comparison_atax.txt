

ompoffload atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            2.041E-03        6.971E-03           3.140E-02        1.231E-01             4.930E-01
Conf. 95% [s]       1.707E-05        4.097E-05           1.187E-04        4.930E-04             1.164E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      2.041E-03 &       6.971E-03 &          3.140E-02 &       1.231E-01 &            4.930E-01 \\
 Conf. 95\% [s] &      1.707E-05 &       4.097E-05 &          1.187E-04 &       4.930E-04 &            1.164E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload restrict atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.789E-03        5.864E-03           2.589E-02        9.976E-02             4.075E-01
Conf. 95% [s]       3.185E-05        3.379E-05           9.609E-05        2.995E-04             5.362E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.789E-03 &       5.864E-03 &          2.589E-02 &       9.976E-02 &            4.075E-01 \\
 Conf. 95\% [s] &      3.185E-05 &       3.379E-05 &          9.609E-05 &       2.995E-04 &            5.362E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.798E-03        6.280E-03           2.914E-02        1.184E-01             4.803E-01
Conf. 95% [s]       6.675E-06        3.075E-05           8.062E-05        3.499E-04             6.926E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.798E-03 &       6.280E-03 &          2.914E-02 &       1.184E-01 &            4.803E-01 \\
 Conf. 95\% [s] &      6.675E-06 &       3.075E-05 &          8.062E-05 &       3.499E-04 &            6.926E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda restrict atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.549E-03        5.085E-03           2.218E-02        8.989E-02             3.763E-01
Conf. 95% [s]       8.431E-06        3.905E-05           1.032E-04        3.869E-04             1.167E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.549E-03 &       5.085E-03 &          2.218E-02 &       8.989E-02 &            3.763E-01 \\
 Conf. 95\% [s] &      8.431E-06 &       3.905E-05 &          1.032E-04 &       3.869E-04 &            1.167E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / ompoffload atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           100.007         100.003             100.001          100.002               100.001
Conf. 95% [%]         1.18342         0.831316            0.534546         0.566462              0.333892
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      100.007   &      100.003    &         100.001    &      100.002    &           100.001    \\
 Conf. 95\% [\%] &        1.18342 &        0.831316 &           0.534546 &        0.566462 &             0.333892 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / ompoffload restrict atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           114.069          118.882            121.266          123.385               120.986
Conf. 95% [%]         2.24352          0.97861            0.642376         0.617588              0.327013
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      114.069   &       118.882   &         121.266    &      123.385    &           120.986    \\
 Conf. 95\% [\%] &        2.24352 &         0.97861 &           0.642376 &        0.617588 &             0.327013 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / cuda atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           113.522         110.99              107.733          103.99                102.64
Conf. 95% [%]         1.03924         0.849096            0.504607         0.517661              0.283959
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      113.522   &      110.99     &         107.733    &      103.99     &           102.64     \\
 Conf. 95\% [\%] &        1.03924 &        0.849096 &           0.504607 &        0.517661 &             0.283959 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / cuda restrict atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           131.761           137.1             141.583          136.941               131.003
Conf. 95% [%]         1.31529           1.3259            0.849001         0.805185              0.510524
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      131.761   &        137.1    &         141.583    &      136.941    &           131.003    \\
 Conf. 95\% [\%] &        1.31529 &          1.3259 &           0.849001 &        0.805185 &             0.510524 \\
\hline
\end{tabular}
----------------------------------------------------------------
