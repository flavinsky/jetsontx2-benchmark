#!/bin/bash
# This script runs the python script process_data.py for this folder to generate the data
rm -rf fig
rm -rf results
mkdir results
python3 ../process_data.py ompoffload_gemm.csv ompoffload_restrict_gemm.csv cuda_gemm.csv cuda_restrict_gemm.csv > results/comparison_gemm.txt
python3 ../process_data.py ompoffload_atax.csv ompoffload_restrict_atax.csv cuda_atax.csv cuda_restrict_atax.csv > results/comparison_atax.txt
python3 ../process_data.py ompoffload_doitgen.csv ompoffload_restrict_doitgen.csv cuda_doitgen.csv cuda_restrict_doitgen.csv > results/comparison_doitgen.txt
python3 ../process_data.py ompoffload_gramschmidt.csv ompoffload_restrict_gramschmidt.csv cuda_gramschmidt.csv cuda_restrict_gramschmidt.csv > results/comparison_gramschmidt.txt
python3 ../process_data.py ompoffload_correlation.csv ompoffload_restrict_correlation.csv cuda_correlation.csv cuda_restrict_correlation.csv > results/comparison_correlation.txt
