

omp gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            4.659E-04        4.873E-03           4.946E-02        5.510E-01             9.727E+00
Conf. 95% [s]       1.097E-04        5.705E-04           2.760E-03        8.875E-03             1.941E-01
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      4.659E-04 &       4.873E-03 &          4.946E-02 &       5.510E-01 &            9.727E+00 \\
 Conf. 95\% [s] &      1.097E-04 &       5.705E-04 &          2.760E-03 &       8.875E-03 &            1.941E-01 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            5.440E-04        1.012E-03           5.300E-03        3.952E-02             3.206E-01
Conf. 95% [s]       1.879E-04        5.794E-05           2.180E-05        4.582E-05             5.354E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      5.440E-04 &       1.012E-03 &          5.300E-03 &       3.952E-02 &            3.206E-01 \\
 Conf. 95\% [s] &      1.879E-04 &       5.794E-05 &          2.180E-05 &       4.582E-05 &            5.354E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            4.043E-04        7.898E-04           3.683E-03        2.666E-02             2.107E-01
Conf. 95% [s]       6.527E-06        7.497E-06           2.494E-05        3.537E-05             1.128E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      4.043E-04 &       7.898E-04 &          3.683E-03 &       2.666E-02 &            2.107E-01 \\
 Conf. 95\% [s] &      6.527E-06 &       7.497E-06 &          2.494E-05 &       3.537E-05 &            1.128E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


omp gemm / omp gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            105.867          101.39               100.312        100.026               100.04
Conf. 95% [%]         34.7539          16.7313               7.911          2.27853               2.82332
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       105.867  &        101.39   &            100.312 &       100.026   &            100.04    \\
 Conf. 95\% [\%] &        34.7539 &         16.7313 &              7.911 &         2.27853 &              2.82332 \\
\hline
\end{tabular}
----------------------------------------------------------------


omp gemm / ompoffload gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]             97.2517         482.905             933.149          1394.1                3034.51
Conf. 95% [%]         39.8719          62.8516             52.2184           22.515                60.774
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &        97.2517 &        482.905  &           933.149  &        1394.1   &             3034.51  \\
 Conf. 95\% [\%] &        39.8719 &         62.8516 &            52.2184 &          22.515 &               60.774 \\
\hline
\end{tabular}
----------------------------------------------------------------


omp gemm / cuda gemm

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            115.274           617.049           1342.93          2066.42               4615.72
Conf. 95% [%]         27.1973           72.483             75.4945          33.3995               92.1532
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       115.274  &         617.049 &          1342.93   &       2066.42   &            4615.72   \\
 Conf. 95\% [\%] &        27.1973 &          72.483 &            75.4945 &         33.3995 &              92.1532 \\
\hline
\end{tabular}
----------------------------------------------------------------
