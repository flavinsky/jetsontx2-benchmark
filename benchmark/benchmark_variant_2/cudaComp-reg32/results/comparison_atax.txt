

ompoffload8 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            2.009E-03        6.873E-03           3.102E-02        1.228E-01             4.940E-01
Conf. 95% [s]       3.395E-05        3.608E-05           9.911E-05        4.676E-04             1.341E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      2.009E-03 &       6.873E-03 &          3.102E-02 &       1.228E-01 &            4.940E-01 \\
 Conf. 95\% [s] &      3.395E-05 &       3.608E-05 &          9.911E-05 &       4.676E-04 &            1.341E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload9 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            2.095E-03        7.011E-03           3.172E-02        1.227E-01             4.938E-01
Conf. 95% [s]       9.797E-05        4.556E-05           1.047E-04        3.047E-04             1.122E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      2.095E-03 &       7.011E-03 &          3.172E-02 &       1.227E-01 &            4.938E-01 \\
 Conf. 95\% [s] &      9.797E-05 &       4.556E-05 &          1.047E-04 &       3.047E-04 &            1.122E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda8 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            2.008E-03        6.705E-03           3.007E-02        1.203E-01             4.941E-01
Conf. 95% [s]       2.305E-05        1.285E-05           2.764E-05        1.106E-04             1.096E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      2.008E-03 &       6.705E-03 &          3.007E-02 &       1.203E-01 &            4.941E-01 \\
 Conf. 95\% [s] &      2.305E-05 &       1.285E-05 &          2.764E-05 &       1.106E-04 &            1.096E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda9 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.847E-03        6.394E-03           2.933E-02        1.181E-01             4.810E-01
Conf. 95% [s]       1.151E-05        3.065E-05           6.121E-05        2.574E-04             4.677E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.847E-03 &       6.394E-03 &          2.933E-02 &       1.181E-01 &            4.810E-01 \\
 Conf. 95\% [s] &      1.151E-05 &       3.065E-05 &          6.121E-05 &       2.574E-04 &            4.677E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload8 atax / ompoffload8 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           100.029         100.003             100.001           100.001              100.001
Conf. 95% [%]         2.39001         0.742343            0.451805          0.53843              0.383884
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      100.029   &      100.003    &         100.001    &       100.001   &           100.001    \\
 Conf. 95\% [\%] &        2.39001 &        0.742343 &           0.451805 &         0.53843 &             0.383884 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload8 atax / ompoffload9 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            96.1225         98.0363              97.802         100.131               100.041
Conf. 95% [%]         4.77922         0.818924             0.44923         0.455178              0.354149
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       96.1225  &       98.0363   &           97.802   &      100.131    &           100.041    \\
 Conf. 95\% [\%] &        4.77922 &        0.818924 &            0.44923 &        0.455178 &             0.354149 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload8 atax / cuda8 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            100.08          102.498            103.159          102.084                 99.9807
Conf. 95% [%]          2.0443          0.57277            0.342929         0.399833               0.35041
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       100.08   &       102.498   &         103.159    &      102.084    &             99.9807  \\
 Conf. 95\% [\%] &         2.0443 &         0.57277 &           0.342929 &        0.399833 &              0.35041 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload8 atax / cuda9 atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           108.811         107.491             105.769          104.009               102.716
Conf. 95% [%]         1.95958         0.764129            0.403601         0.456302              0.296166
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      108.811   &      107.491    &         105.769    &      104.009    &           102.716    \\
 Conf. 95\% [\%] &        1.95958 &        0.764129 &           0.403601 &        0.456302 &             0.296166 \\
\hline
\end{tabular}
----------------------------------------------------------------
