#!/bin/bash
# This script runs the python script process_data.py for this folder to generate the data
rm -rf fig
rm -rf results
mkdir results

python3 ../process_data.py ompoffload8_gemm.csv \
        ompoffload9_gemm.csv \
        cuda8_gemm.csv \
        cuda9_gemm.csv> results/comparison_gemm.txt

python3 ../process_data.py ompoffload8_doitgen.csv \
        ompoffload9_doitgen.csv \
        cuda8_doitgen.csv \
        cuda9_doitgen.csv> results/comparison_doitgen.txt

python3 ../process_data.py ompoffload8_atax.csv \
        ompoffload9_atax.csv \
        cuda8_atax.csv \
        cuda9_atax.csv> results/comparison_atax.txt

python3 ../process_data.py ompoffload8_gramschmidt.csv \
        ompoffload9_gramschmidt.csv \
        cuda8_gramschmidt.csv \
        cuda9_gramschmidt.csv> results/comparison_gramschmidt.txt

python3 ../process_data.py ompoffload8_correlation.csv \
        ompoffload9_correlation.csv \
        cuda8_correlation.csv \
        cuda9_correlation.csv> results/comparison_correlation.txt
