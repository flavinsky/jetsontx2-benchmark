* Comparison of CUDA versions using benchmarks from 310818 at 09:50 and 10:30
* 10 kernel execution per launch of the binaries
* Frequencies MAXN
* Jetsonclocks on --> All to max on board
* No -fopenmp-nonaliased-maps and  -ffp-contract=fast
* -maxregcount=64
* Processing using python script process_data.py
* CUDA 8.0 with L4T 28.2.1
* CUDA 9.0 with Jetpack 3.2.1
* Equal thread distribution for omp offloading and CUDA
