* 10 kernel execution per launch of the binaries
* Frequencies MAXN
* Jetsonclocks on --> All to max on board
* -maxregcount=64 --> reg count can now influence max number of blocks in sm
* Processing using python script process_data.py
* CUDA 8.0 with L4T 28.2.1
* Equal thread distribution for omp offloading and CUDA
