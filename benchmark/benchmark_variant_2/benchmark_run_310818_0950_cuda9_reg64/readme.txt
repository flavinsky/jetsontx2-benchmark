* 10 kernel execution per launch of the binaries
* Frequencies MAXN
* Jetsonclocks on --> All to max on board
* -maxregcount=64 --> reg count can now influence max number of blocks in sm
* -ffp-contract=fast -fopenmp-nonaliased-maps not used
* Processing using python script process_data.py
* CUDA 9.0
* Equal thread distribution for omp offloading and CUDA
