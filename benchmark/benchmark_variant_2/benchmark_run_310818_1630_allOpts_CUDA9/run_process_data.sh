#!/bin/bash
# This script runs the python script process_data.py for this folder to generate the data
rm -rf fig
rm -rf results
mkdir results
python3 ../process_data.py ompoffload_gemm.csv cuda_gemm.csv > results/comparison_gemm.txt
python3 ../process_data.py ompoffload_atax.csv cuda_atax.csv > results/comparison_atax.txt
python3 ../process_data.py ompoffload_doitgen.csv cuda_doitgen.csv > results/comparison_doitgen.txt
python3 ../process_data.py ompoffload_gramschmidt.csv cuda_gramschmidt.csv > results/comparison_gramschmidt.txt
python3 ../process_data.py ompoffload_correlation.csv cuda_correlation.csv > results/comparison_correlation.txt
