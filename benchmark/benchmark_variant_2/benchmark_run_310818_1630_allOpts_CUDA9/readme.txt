* 10 kernel execution per launch of the binaries
* Frequencies MAXN
* Jetsonclocks on --> All to max on board
* -fopenmp-nonaliased-maps and -ffp-contract=fast used
* -maxregcount=32 --> Otherwise CUDA 9 utilizes too many registers and number of blocks in SM is restricted
* Processing using python script process_data.py
* CUDA 9.0 with Jetpack 3.2.1
* Equal thread distribution for omp offloading and CUDA
