

ompoffload gramschmidt

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.108E-02        2.418E-02           7.199E-02        3.023E-01             1.170E+00
Conf. 95% [s]       1.267E-03        7.947E-04           8.808E-04        1.221E-03             1.203E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.108E-02 &       2.418E-02 &          7.199E-02 &       3.023E-01 &            1.170E+00 \\
 Conf. 95\% [s] &      1.267E-03 &       7.947E-04 &          8.808E-04 &       1.221E-03 &            1.203E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda gramschmidt

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            7.501E-03        2.182E-02           5.869E-02        3.193E-01             1.345E+00
Conf. 95% [s]       1.911E-04        1.011E-04           1.629E-03        3.494E-04             1.098E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      7.501E-03 &       2.182E-02 &          5.869E-02 &       3.193E-01 &            1.345E+00 \\
 Conf. 95\% [s] &      1.911E-04 &       1.011E-04 &          1.629E-03 &       3.494E-04 &            1.098E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload gramschmidt / ompoffload gramschmidt

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            101.327         100.108             100.015         100.002               100
Conf. 95% [%]         16.3433          4.65227             1.73041         0.571028              0.145428
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       101.327  &       100.108   &          100.015   &      100.002    &           100        \\
 Conf. 95\% [\%] &        16.3433 &         4.65227 &            1.73041 &        0.571028 &             0.145428 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload gramschmidt / cuda gramschmidt

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            147.752         110.808             122.768          94.6675               87.041
Conf. 95% [%]         17.3152          3.67821             3.72426         0.396031              0.114297
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       147.752  &       110.808   &          122.768   &       94.6675   &            87.041    \\
 Conf. 95\% [\%] &        17.3152 &         3.67821 &            3.72426 &        0.396031 &             0.114297 \\
\hline
\end{tabular}
----------------------------------------------------------------
