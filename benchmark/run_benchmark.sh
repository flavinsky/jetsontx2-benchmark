#!/bin/bash
## run_benchmark.sh
##
## Made by Flavio Kreiliger
## Contact: <kreilfla@fel.cvut.cz>
##
## Started on 24.07.18 by Flavio Kreiliger
##
set -e
# ======================================================================
# System paths
# ======================================================================
OPTIND=1

CURR_LOC=$PWD
PATH_CUDA=/usr/local/cuda
PATH_CUDA_BIN=$PATH_CUDA/bin
PATH_CUDA_LIB64=$PATH_CUDA/lib64

PATH_CLANG_ROOT=/home/kreilfla/nfs/prem/benchmark/ibm/clang-ykt
PATH_HERCULES_ROOT=/opt/hercules-compiler

PATH_HERCULES_LIBPREMNOTIFY=/opt/hercules-compiler/lib/hercules
PATH_CLANG=$PATH_CLANG_ROOT/bin
PATH_CLANG_LIB=$PATH_CLANG_ROOT/lib

# ======================================================================
# Polybench paths
# ======================================================================

#Paths to polybench-c
PATH_POLYBENCH_C=$CURR_LOC/Polybench/Polybench-c-3.2
PATH_POLYBENCH_C_UTIL=$PATH_POLYBENCH_C/utilities
POLYBENCH_C_HELPER=$PATH_POLYBENCH_C_UTIL/polybench.c

#Paths to test cases
PATH_POLYBENCH_C_LINALG=$PATH_POLYBENCH_C/linear-algebra
PATH_POLYBENCH_C_KERNELS=$PATH_POLYBENCH_C_LINALG/kernels
PATH_POLYBENCH_C_SOLVERS=$PATH_POLYBENCH_C_LINALG/solvers

PATH_POLYBENCH_C_DATAM=$PATH_POLYBENCH_C/datamining
PATH_POLYBENCH_C_COV=$PATH_POLYBENCH_C_DATAM/covariance
PATH_POLYBENCH_C_COR=$PATH_POLYBENCH_C_DATAM/correlation

PATH_POLYBENCH_C_GEMM=$PATH_POLYBENCH_C_KERNELS/gemm
PATH_POLYBENCH_C_ATAX=$PATH_POLYBENCH_C_KERNELS/atax
PATH_POLYBENCH_C_DOITGEN=$PATH_POLYBENCH_C_KERNELS/doitgen
PATH_POLYBENCH_C_GRAM=$PATH_POLYBENCH_C_SOLVERS/gramschmidt

#Paths to polybench-acc
PATH_POLYBENCH_ACC=$CURR_LOC/Polybench
PATH_POLYBENCH_ACC_UTIL=$PATH_POLYBENCH_ACC/common
POLYBENCH_ACC_HELPER=$PATH_POLYBENCH_ACC_UTIL/polybench.c
PATH_POLYBENCH_ACC_CUDA_UTIL=$PATH_POLYBENCH_ACC/CUDA/utilities
POLYBENCH_ACC_CUDA_HELPER=$PATH_POLYBENCH_ACC_CUDA_UTIL/polybench.c

#Paths to openMP test cases
PATH_POLYBENCH_OMP_LINALG=$PATH_POLYBENCH_ACC/OpenMP/linear-algebra
PATH_POLYBENCH_OMP_KERNELS=$PATH_POLYBENCH_OMP_LINALG/kernels
PATH_POLYBENCH_OMP_SOLVERS=$PATH_POLYBENCH_OMP_LINALG/solvers

PATH_POLYBENCH_OMP_DATAM=$PATH_POLYBENCH_ACC/OpenMP/datamining
PATH_POLYBENCH_OMP_COV=$PATH_POLYBENCH_OMP_DATAM/covariance
PATH_POLYBENCH_OMP_COR=$PATH_POLYBENCH_OMP_DATAM/correlation

PATH_POLYBENCH_OMP_GEMM=$PATH_POLYBENCH_OMP_KERNELS/gemm
PATH_POLYBENCH_OMP_ATAX=$PATH_POLYBENCH_OMP_KERNELS/atax
PATH_POLYBENCH_OMP_DOITGEN=$PATH_POLYBENCH_OMP_KERNELS/doitgen
PATH_POLYBENCH_OMP_GRAM=$PATH_POLYBENCH_OMP_SOLVERS/gramschmidt

#Paths to openMP-target test cases
PATH_POLYBENCH_OMPTRG_LINALG=$PATH_POLYBENCH_ACC/OpenMP-target/linear-algebra
PATH_POLYBENCH_OMPTRG_KERNELS=$PATH_POLYBENCH_OMPTRG_LINALG/kernels
PATH_POLYBENCH_OMPTRG_SOLVERS=$PATH_POLYBENCH_OMPTRG_LINALG/solvers

PATH_POLYBENCH_OMPTRG_DATAM=$PATH_POLYBENCH_ACC/OpenMP-target/datamining
PATH_POLYBENCH_OMPTRG_COV=$PATH_POLYBENCH_OMPTRG_DATAM/covariance
PATH_POLYBENCH_OMPTRG_COR=$PATH_POLYBENCH_OMPTRG_DATAM/correlation

PATH_POLYBENCH_OMPTRG_GEMM=$PATH_POLYBENCH_OMPTRG_KERNELS/gemm
PATH_POLYBENCH_OMPTRG_ATAX=$PATH_POLYBENCH_OMPTRG_KERNELS/atax
PATH_POLYBENCH_OMPTRG_DOITGEN=$PATH_POLYBENCH_OMPTRG_KERNELS/doitgen
PATH_POLYBENCH_OMPTRG_GRAM=$PATH_POLYBENCH_OMPTRG_SOLVERS/gramschmidt

#Paths to CUDA test cases
PATH_POLYBENCH_CUDA_LINALG=$PATH_POLYBENCH_ACC/CUDA/linear-algebra
PATH_POLYBENCH_CUDA_KERNELS=$PATH_POLYBENCH_CUDA_LINALG/kernels
PATH_POLYBENCH_CUDA_SOLVERS=$PATH_POLYBENCH_CUDA_LINALG/solvers

PATH_POLYBENCH_CUDA_DATAM=$PATH_POLYBENCH_ACC/CUDA/datamining
PATH_POLYBENCH_CUDA_COV=$PATH_POLYBENCH_CUDA_DATAM/covariance
PATH_POLYBENCH_CUDA_COR=$PATH_POLYBENCH_CUDA_DATAM/correlation

PATH_POLYBENCH_CUDA_GEMM=$PATH_POLYBENCH_CUDA_KERNELS/gemm
PATH_POLYBENCH_CUDA_ATAX=$PATH_POLYBENCH_CUDA_KERNELS/atax
PATH_POLYBENCH_CUDA_DOITGEN=$PATH_POLYBENCH_CUDA_KERNELS/doitgen
PATH_POLYBENCH_CUDA_GRAM=$PATH_POLYBENCH_CUDA_SOLVERS/gramschmidt

# ======================================================================
# Test case definitions
# ======================================================================

DATA_SET_SIZE=(MINI_DATASET SMALL_DATASET STANDARD_DATASET LARGE_DATASET EXTRALARGE_DATASET)
#DATA_SET_SIZE=(MINI_DATASET STANDARD_DATASET LARGE_DATASET EXTRALARGE_DATASET)
#DATA_SET_SIZE=(MINI_DATASET SMALL_DATASET STANDARD_DATASET LARGE_DATASET EXTRALARGE_DATASET DATASET_EXT1 DATASET_EXT2)
#DATA_SET_SIZE=(MINI_DATASET)

# Testbench Parameter with default paramters
NOF_REPETITIONS=5
PRINT_SIZE= #-DPRINT_SIZE
RUN_CPU= #-DRUN_ON_CPU
OMP_MEAS_KERNEL= #-DOMP_MEAS_KERNEL
KERNEL_REP=-DCOMP_REPETITION=10
REG_COUNT=32
NON_ALIASED_OMP= #-ffp-contract=fast -fopenmp-nonaliased-maps
NON_ALIASED_CUDA= #-DRESTRICT_FOR_CUDA
VERBOSE=0
STORE_BIN=0
LOAD_BIN=0
ZERO_COPY_HERCULES= #-I/usr/local/cuda/targets/aarch64-linux/include -lcudart -DUSE_ZEROCOPY
ZERO_COPY_YKT= #-I/usr/local/cuda/targets/aarch64-linux/include -lcudart -DUSE_ZEROCOPY


BIN_PATH=$HOME/build/store

TEST_CASE="omp"

#Test cases for polybench-c
TEST_CASES_POLYC=( \
                $PATH_POLYBENCH_C_GEMM gemm \
                $PATH_POLYBENCH_C_ATAX atax \
                $PATH_POLYBENCH_C_DOITGEN doitgen \
                $PATH_POLYBENCH_C_GRAM gramschmidt \
                $PATH_POLYBENCH_C_COR correlation)

#Test cases for polybench-omp
TEST_CASES_POLYOMP=( \
                $PATH_POLYBENCH_OMP_GEMM gemm \
                $PATH_POLYBENCH_OMP_ATAX atax \
                $PATH_POLYBENCH_OMP_DOITGEN doitgen \
                $PATH_POLYBENCH_OMP_GRAM gramschmidt \
                $PATH_POLYBENCH_OMP_COR correlation)

#Test cases for polybench-omp with target
TEST_CASES_POLYOMPTRG=( \
                $PATH_POLYBENCH_OMPTRG_GEMM gemm  \
                $PATH_POLYBENCH_OMPTRG_ATAX atax \
                $PATH_POLYBENCH_OMPTRG_DOITGEN doitgen \
 #           $PATH_POLYBENCH_OMPTRG_GRAM gramschmidt \
 #           #$PATH_POLYBENCH_OMPTRG_COR correlation_old)
                $PATH_POLYBENCH_OMPTRG_COR correlation)
            

#Test cases for polybench-omp
TEST_CASES_POLYCUDA=( \
$PATH_POLYBENCH_CUDA_GEMM gemm \
$PATH_POLYBENCH_CUDA_ATAX atax \
$PATH_POLYBENCH_CUDA_DOITGEN doitgen \
$PATH_POLYBENCH_CUDA_GRAM gramschmidt \
$PATH_POLYBENCH_CUDA_COR correlation)
            

# ======================================================================
# Test bench script
# ======================================================================

# Brief: Builds binary with given parameters for polybench-c
# $1: Data set size (DATA_SET_SIZE)
# $2: PATH to file
# $3: Source file name
build_poly-c(){
    clang -O3 -lc -I$PATH_POLYBENCH_ACC_UTIL -I$2 \
    $POLYBENCH_ACC_HELPER $2/$3.c -DPOLYBENCH_TIME \
    -D$1 $PRINT_SIZE $KERNEL_REP\
    $RUN_CPU -o ~/build/$3 -lm
}


# Brief: Builds binary with given parameters for polybench-ACC with openMP
# $1: Data set size (DATA_SET_SIZE)
# $2: PATH to file
# $3: Source file name
build_poly-omp(){
    clang -O3 -v -lc -fopenmp -I$PATH_POLYBENCH_ACC_UTIL -I$2 \
    $POLYBENCH_ACC_HELPER $2/$3.c -DPOLYBENCH_TIME -D$1 \
    $PRINT_SIZE $RUN_CPU $KERNEL_REP\
    -o ~/build/$3 -lm
}


# Brief: Builds binary with given parameters for polybench-ACC with openMP-target
# $1: Data set size (DATA_SET_SIZE)
# $2: PATH to file
# $3: Source file name
# If the device mapped data does not overlap, the compiler flag
# -fopenmp-nonaliased-maps will favour usage of faster non-
# coherent loads (C/C++).
# CLANG compiler flag: -ffp-contract=fast uses fuse multiplies
# and adds wherever profitable, even across statements. Doesn't
# prevent PTXAS from fusing additional multiplies and adds (C/C++).
build_poly-omptrg(){
     echo "[DEBUG] clang -O3 -v -lc -fopenmp $NON_ALIASED_OMP -fopenmp-targets=nvptx64-nvidia-cuda \
     -Xcuda-ptxas -maxrregcount=$REG_COUNT -I$PATH_POLYBENCH_ACC_UTIL -I$2 \
     $POLYBENCH_ACC_HELPER $2/$3.c -DPOLYBENCH_TIME $PRINT_SIZE $OMP_MEAS_KERNEL $KERNEL_REP\
     $ZERO_COPY_YKT \
     $RUN_CPU -D$1 -o ~/build/$3 -lm"

     clang -O3 -v -lc -fopenmp $NON_ALIASED_OMP -fopenmp-targets=nvptx64-nvidia-cuda \
     -Xcuda-ptxas -maxrregcount=$REG_COUNT -I$PATH_POLYBENCH_ACC_UTIL -I$2 \
     $POLYBENCH_ACC_HELPER $2/$3.c -DPOLYBENCH_TIME $PRINT_SIZE $OMP_MEAS_KERNEL $KERNEL_REP\
     $ZERO_COPY_YKT \
     $RUN_CPU -D$1 -o ~/build/$3 -lm
}


# Brief: Builds binary with given parameters for polybench-ACC with openMP-target
# $1: Data set size (DATA_SET_SIZE)
# $2: PATH to file
# $3: Source file name
# If the device mapped data does not overlap, the compiler flag
# -fopenmp-nonaliased-maps will favour usage of faster non-
# coherent loads (C/C++).
# CLANG compiler flag: -ffp-contract=fast uses fuse multiplies
# and adds wherever profitable, even across statements. Doesn't
# prevent PTXAS from fusing additional multiplies and adds (C/C++).
build_poly-omptrgeth(){
     echo "[DEBUG] clang -O3 $POLYBENCH_ACC_HELPER $2/$3.c -v -lc -fopenmp=libomp \
     $NON_ALIASED_OMP -fopenmp-targets=nvptx64-nvidia-cuda \
     -Xcuda-fatbinary -L$PATH_HERCULES_LIBPREMNOTIFY -Xcuda-fatbinary \
     -lpremnotify-gpu -Xcuda-fatbinary -v -Xcuda-ptxas -v \
     -Xcuda-ptxas -maxrregcount=$REG_COUNT -I$PATH_POLYBENCH_ACC_UTIL -I $2 \
     -DPOLYBENCH_TIME $PRINT_SIZE $OMP_MEAS_KERNEL $KERNEL_REP \
     $ZERO_COPY_HERCULES \
     $RUN_CPU -D$1 -o ~/build/$3 -lm"

     clang -O3 $POLYBENCH_ACC_HELPER $2/$3.c -v -lc -fopenmp=libomp \
     $NON_ALIASED_OMP -fopenmp-targets=nvptx64-nvidia-cuda \
     -Xcuda-fatbinary -L$PATH_HERCULES_LIBPREMNOTIFY -Xcuda-fatbinary \
     -lpremnotify-gpu -Xcuda-fatbinary -v -Xcuda-ptxas -v \
     -Xcuda-ptxas -maxrregcount=$REG_COUNT -I$PATH_POLYBENCH_ACC_UTIL -I $2 \
     -DPOLYBENCH_TIME $PRINT_SIZE $OMP_MEAS_KERNEL $KERNEL_REP \
     $ZERO_COPY_HERCULES \
     $RUN_CPU -D$1 -o ~/build/$3 -lm
}
# clang main.c -O3 -fopenmp=libomp -fopenmp-targets=nvptx64-nvidia-cuda -Xcuda-fatbinary -L<Path to
# HerculesCompiler Git repository>/libpremnotify -Xcuda-fatbinary -lpremnotify-gpu -Xcuda-fatbinary -v -Xcuda-
# ptxas -v

# Brief: Builds binary with given parameters for polybench-ACC with CUDA
# $1: Data set size (DATA_SET_SIZE)
# $2: PATH to filed
# $3: Source file name
# -gencode arch=compute_62,code=[sm_62,compute_62] reduces time for about 100ms
build_poly-cuda(){
    nvcc -O3 -v\
    -DPOLYBENCH_TIME $NON_ALIASED_CUDA $PRINT_SIZE $RUN_CPU $KERNEL_REP\
    -D$1 $2/$3.cu -I $PATH_POLYBENCH_ACC_CUDA_UTIL -I $2 -o ~/build/$3 -lm
}



# Brief: Deletes created binariy
# $1: Name of binary
clean_binaries(){
   rm -rf $1
}


# Brief: Runs actual benchmark
# $1: Set of Testcases 
# $2: Prefix
# $3: Build function
# $4: Nof repetitions
run_bench(){
    mkdir -p ~/build;
    mkdir -p logs;
   
    if [ $STORE_BIN -eq 1 ]; then
        # Store build binary
        mkdir -p $BIN_PATH
    fi

    # Resolve test cases
    local -n cases=$1
    
    for (( j=0; j<${#cases[@]}; j+=2)); do
        FILE_PATH=${cases[j]}
        FILE_NAME=${cases[j+1]}

        echo "[INFO] Running: $FILE_PATH, $FILE_NAME"
    
        # Open File
        > logs/$2$FILE_NAME.csv;
        # Iterate through data set sizes
        for SET_SIZE in ${DATA_SET_SIZE[@]}; do
            BINARY_NAME=$2$FILE_NAME$SET_SIZE
            echo "[INFO] $BINARY_NAME"
            echo "[INFO] Benchmark SET: $SET_SIZE"
            
            if [ $LOAD_BIN -eq 0 ]; then
                # Build binary
                if [ $VERBOSE -eq 1 ]; then
                    echo "[DEBUG] $3 $SET_SIZE $FILE_PATH $FILE_NAME"
                    $3 $SET_SIZE $FILE_PATH $FILE_NAME
                else
                    $3 $SET_SIZE $FILE_PATH $FILE_NAME >/dev/null 2>&1
                fi
            else
                # Load binary    
                echo "[INFO] Loading $BIN_PATH/$BINARY_NAME"
                cp $BIN_PATH/$BINARY_NAME ~/build/$FILE_NAME
            fi

            if [ $STORE_BIN -eq 1 ]; then
                # Store build binary
                echo "[INFO] Storing $BIN_PATH/$BINARY_NAME"
                cp ~/build/$FILE_NAME $BIN_PATH/$BINARY_NAME
            
            else
                # Execute test case with this data set
                #echo $SET_SIZE >> logs/$2$FILE_NAME.log
                for (( i = 0; i < $4; i++ )) ; do
                    # sudo setcap cap_sys_nice+ep ~/$FILE_NAME
                    result=$(~/build/$FILE_NAME)
                    echo $SET_SIZE$result >> logs/$2$FILE_NAME.csv
                    echo "[INFO] Itereation: $i Time: $result"
                done
            fi
            # Delete Binary
            clean_binaries ~/build/$FILE_NAME
        done
    done
}

# Brief: Appends variable to path
# $1: Path to add
# $2: Position. \"before\" or \"after\"
pathadd () {
        if ! echo "$PATH" | /bin/grep -Eq "(^|:)$1($|:)" ; then
           if [ "$2" = "after" ] ; then
              export PATH="$PATH:$1"
           else
              export PATH="$1:$PATH"
           fi
        fi
}



show_help () {
    echo "Help output for run_benchmark.sh"
    echo "Author: Flavio Kreiliger"
    echo "-h/?: Shows this help output"
    echo "-c  : Defines Test case (c, omp, ompgpu, cuda, all) \"-c ompgpu\""
    echo "-k  : Enables kernel only measurement for ompgpu"
    echo "-s  : Stores generated binaries"
    echo "-l  : load and Execute stored binaries"
    echo "-v  : Enables verbose output"
    echo "-r  : Defines number of repetitions (integer) \"-r 10\""
    echo "-i  : Defines number of internal kernel repetitions (integer) \"-i 10\""
    echo "-t  : Enables test of parallel execution vs. sequential execution and prints size of datasets"
    echo "-d  : Delete all corresponding files"
    echo "-m  : Sets maxregcount for OpenMP offloading. Default is 32 registers per thread"
    echo "-n  : Adds the -ffp-contract=fast -fopenmp-nonaliased-maps flag to OpenMP offloading (n like nonaliased)"
    echo "-z  : Use zerocopy with hercules compiler -z host or -z managed"
    echo "Example: \"./run_benchmark.sh -r 10 -c ompgpu -k -s -h\""
}

# argument parser. : after an argument means additional parameter is needed.
while getopts "h?c:ks:l:vr:i:tm:ndpz:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    c)  TEST_CASE=$OPTARG
        if [[ $TEST_CASE = 'ompgpu_hercules' ]]; then
            PATH_CLANG=$PATH_HERCULES_ROOT/bin
            PATH_CLANG_LIB=$PATH_HERCULES_ROOT/lib
        fi
        ;;
    k)  OMP_MEAS_KERNEL=-DOMP_MEAS_KERNEL
        ;;
    s)  STORE_BIN=1
        if [ ! -z "$OPTARG" ]; then
            BIN_PATH=$OPTARG
        fi
        ;;
    l)  LOAD_BIN=1
        if [ ! -z "$OPTARG" ]; then
            BIN_PATH=$OPTARG
        fi
        ;;
    v)  VERBOSE=1
        ;;
    r)  NOF_REPETITIONS=$OPTARG
        ;;
    i)  KERNEL_REP=-DCOMP_REPETITION=$OPTARG
        ;;
    t)  RUN_CPU=-DRUN_ON_CPU
        PRINT_SIZE=-DPRINT_SIZE
        ;;
    m)  REG_COUNT=$OPTARG
        ;;
    n)  NON_ALIASED_OMP="-ffp-contract=fast -fopenmp-nonaliased-maps"
        NON_ALIASED_CUDA=-DRESTRICT_FOR_CUDA
        ;;
    z)  if [[ $OPTARG = "host" ]]; then
            ZERO_COPY_HERCULES="-L/usr/local/cuda/lib64 -I/usr/local/cuda/targets/aarch64-linux/include -lcudart -DUSE_ZEROCOPY -DHERCULES_HOST"
            ZERO_COPY_YKT="-L/usr/local/cuda/lib64 -I/usr/local/cuda/targets/aarch64-linux/include -lcudart -DUSE_ZEROCOPY -DYKT_HOST"
        elif [[ $OPTARG = "managed" ]]; then
            ZERO_COPY_HERCULES="-L/usr/local/cuda/lib64 -I/usr/local/cuda/targets/aarch64-linux/include -lcudart -DUSE_ZEROCOPY"
            ZERO_COPY_YKT="-L/usr/local/cuda/lib64 -I/usr/local/cuda/targets/aarch64-linux/include -lcudart -DUSE_ZEROCOPY -DYKT_MANAGED"
        fi 
        ;;
    d)  rm -rf logs
        rm -rf ~/build
        exit 0
    esac
done

echo "Received Parameter:"
echo "TEST_CASE: $TEST_CASE"
echo "OMP_MEAS_KERNEL: $OMP_MEAS_KERNEL"
echo "STORE_BIN: $STORE_BIN"
echo "VERBOSE: $VERBOSE"
echo "NOF_REP: $NOF_REPETITIONS"
echo "RUN_CPU: $RUN_CPU"
echo "PRINT_SIZE: $PRINT_SIZE"
echo "ZERO_COPY_YKT: $ZERO_COPY_YKT"
echo "ZERO_COPY_HERCULES: $ZERO_COPY_HERCULES"


echo "[INFO] CUDA paths: $PATH_CUDA and $PATH_CUDA_BIN";
pathadd $PATH_CUDA;
pathadd $PATH_CUDA_BIN;

echo "[INFO] CLANG paths: $PATH_CLANG";
pathadd $PATH_CLANG;

echo "[INFO] Clang and CUDA libraries: $PATH_CLANG_LIB $PATH_CUDA_LIB64";
export LD_LIBRARY_PATH=$PATH_CLANG_LIB:$PATH_CUDA_LIB64:$LD_LIBRARY_PATH;
export LIBRARY_PATH=$PATH_CLANG_LIB:$PATH_CUDA_LIB64:$LIBRARY_PATH;


case $TEST_CASE in
    c)
        echo "[INFO] Running Polybench-C...";
        run_bench TEST_CASES_POLYC c_ build_poly-c $NOF_REPETITIONS;
        ;;

    omp)
        echo "[INFO] Running Polybench-OMP...";
        run_bench TEST_CASES_POLYOMP omp_ build_poly-omp $NOF_REPETITIONS;
        ;;
    ompgpu)
        echo "[INFO] Running Polybench-OMP with omp target...";
        run_bench TEST_CASES_POLYOMPTRG ompoffload_ build_poly-omptrg $NOF_REPETITIONS;
        ;;
    ompgpu_hercules)
        echo "[INFO] Running Polybench-OMP with omp target and hercules compiler...";
        run_bench TEST_CASES_POLYOMPTRG ompoffloadhercules_ build_poly-omptrgeth $NOF_REPETITIONS;
        ;;

    cuda)
        echo "[INFO] Running Polybench-CUDA...";
        run_bench TEST_CASES_POLYCUDA cuda_ build_poly-cuda $NOF_REPETITIONS;
        ;;

    all)
        echo "running all";

        echo "Poly-C"
        run_bench TEST_CASES_POLYC polyc_ build_poly-c $NOF_REPETITIONS;

        echo "Poly-omp"
        run_bench TEST_CASES_POLYOMP polyomp_ build_poly-omp $NOF_REPETITIONS;
        
        echo "Poly-CUDA"
        run_bench TEST_CASES_POLYCUDA polycuda_ build_poly-cuda $NOF_REPETITIONS;

        echo "Poly-omp target"
        run_bench TEST_CASES_POLYOMPTRG polyomptrg_ build_poly-omptrg $NOF_REPETITIONS;
        
        ;;
    *)
        echo "Usage: ./time_benchmarh.sh <benchmark name>";
        echo "Example: ./time_benchmarh.sh \"c\"";
        echo "Available benchmarks: - c";
        echo "                      - omp";
        echo "                      - ompgpu";
        echo "                      - ompgpu_hercules";
        echo "                      - cuda";
        exit 1;
esac
