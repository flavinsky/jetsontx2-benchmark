#!/usr/bin/env python3
import numpy as np
from scipy.stats import t
import matplotlib.pyplot as plt
from sys import argv
from tabulate import tabulate
import re
import os
from math import ceil

def load_data(filename):
    data = np.genfromtxt(filename, delimiter=',', dtype=np.str)
    return data


def load_and_prepare_data():
    records = []
    dataSets = []
    for fileName in argv[1:]:
        records.append((re.findall(r'(.+?).csv', fileName)[0].replace('_', ' '), load_data(fileName)))

    # Collect data sets
    for row in records[0][1]:
        if row[0] not in dataSets:
            dataSets.append(row[0])

    return records, dataSets


# T/N = tn_ratio, tn_conf = comparison_means(n_y, n_conf, t_y, t_conf)
def comparison_means(y1, h1, y2, h2): # Y2/Y1
    divisor = (np.square(y1)-np.square(h1))
    y_ratio = np.divide(np.multiply(y1, y2), divisor)
    h_ratio = np.divide(np.sqrt(np.square(np.multiply(y1, y2)) - np.multiply((np.square(y1) - np.square(h1)), (np.square(y2) - np.square(h2)))), divisor)
    return y_ratio, h_ratio


def mean_confidence(data):
    # get degrees of freedom
    r = data.shape[1]
    # Define t-quantile:
    tq = t.ppf(1-0.025, r - 1)
    # get mean of each coloumn
    y_i = data.mean(axis=1)
    # get global mean
    y = data.mean()
    # Calculate S^2
    s_2 = np.sum(np.square(y_i-y))/(r-1)
    # divide through r
    se = np.sqrt(s_2/r)
    # Return confidence interval
    return y, se*tq


def process_data(records, dataSets):
    ratios = []
    results = []
    for record in records:
        avg = []
        conf = []
        for currSet in dataSets:
            data = record[1][np.where(record[1][:,0] == currSet)]
            data = data[:,1:].astype(np.float)
            y, c = mean_confidence(data)
            avg.append(y)
            conf.append(c)
        results.append((record[0], dataSets, avg, conf))

    for result in results:
        ratio, conf = comparison_means(result[2], result[3], results[0][2], results[0][3])
        ratios.append((results[0][0] + " / " + result[0], ratio,conf))

    return results, ratios


def print_tables(results, ratios, dataSets):
    # Print out table
    for result in results:
        print("\n\n" + result[0] + "\n")
        avg = result[2].copy()
        co = result[3].copy()
        avg.insert(0,'Avg. [s]')
        co.insert(0,'Conf. 95% [s]')
        print(tabulate([avg, co], headers=dataSets, floatfmt=".3E"))
#        print(tabulate([avg, co], headers=dataSets, floatfmt=".3E", tablefmt="latex"))
        print("----------------------------------------------------------------")

    for ratio in ratios:
        print("\n\n" + ratio[0] +"\n")
        ra = (ratio[1]*100).tolist().copy()
        co = (ratio[2]*100).tolist().copy()
        ra.insert(0,'Ratio [%]')
        co.insert(0,'Conf. 95% [%]')
        print(tabulate([ra, co], headers=dataSets))
#        print(tabulate([ra, co], headers=dataSets, tablefmt="latex"))
        print("----------------------------------------------------------------")


def plot_mean_graphs(results, dataSets):
    #Plot execution mean times
    x_axis = range(len(dataSets))

    fig = plt.figure(frameon=True)
    ax = fig.gca()
    plt.yscale('log')
    plt.xticks(x_axis, dataSets, rotation=30)
    plt.grid(True)
    plt.ylabel("Time [s]")
    plt.title("Execution times " + results[0][0].split()[2])
    legend = [result[0] for result in results]

    for result in results:
        ax.plot(x_axis, result[2], '-o')
        #ax.errorbar(x_axis, result[2], yerr=result[3], fmt='-o')

    ax.legend(legend, loc = 'center left', bbox_to_anchor = (0, 0.8))
    fig.savefig('fig/execution_'+results[0][0].split()[2]+'.pdf', bbox_inches='tight')

def plot_ratios(ratios, dataSets, name):
    # Plot comparison bars

    fig = plt.figure(frameon=True)
    ax = fig.gca()
    # Create bars
    bars = []

    start_index = 1
    for ratio in ratios:
        bar_pos = np.arange(start_index, (len(ratios)+1)*len(dataSets), len(ratios)+1)
        start_index += 1
        bar = ax.bar(bar_pos, ratio[1], width=1, yerr=ratio[2])
        bars.append(bar)

    # Add labels
    max_h = np.max([ratio[1] for ratio in ratios])
    ax.set_yticks(np.linspace(0, int(ceil(max_h * 2)), 10))
    ax.set_ylabel('Speed up')
    ax.set_title('Speed up comparison ' + name)
    x_axis = np.arange((len(ratios)+1)/2, (len(ratios)+1)*len(dataSets), len(ratios)+1)
    plt.xticks(x_axis, dataSets, rotation=30)
    #ax.set_xticklabels(('', 'Normal', '', 'Transposed', '', '1D'))
    legend = [ratio[0] for ratio in ratios]
    ax.legend(legend, loc = 'center left', bbox_to_anchor = (0, 0.8))

    def label(bar, ratio):
        for curr_bar, error in zip(bar, ratio[2]):
            height = curr_bar.get_height()
            x = curr_bar.get_x()
            width = curr_bar.get_width()
            ax.text(x + width/2., error+height,
                '%.3f+-%.3f' % (height, error),
                ha='center', va='bottom', rotation='vertical')

    for bar, ratio in zip(bars, ratios):
        label(bar, ratio)
    fig.savefig('fig/ratio_'+name+'.pdf', bbox_inches='tight')


def main():
    
    if not os.path.exists("fig"):
        os.makedirs("fig")
    
    records, dataSets = load_and_prepare_data()
    results, ratios = process_data(records, dataSets)
    print_tables(results, ratios, dataSets)
    plot_mean_graphs(results, dataSets)
    plot_ratios(ratios, dataSets, results[0][0].split()[2])
    #plt.show()


if __name__ == "__main__":
    main()
