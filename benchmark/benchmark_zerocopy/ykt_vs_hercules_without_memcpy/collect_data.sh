#!/bin/bash
# This script runs the python script process_data.py for this folder to generate the data

#########################################################################################
# Using Hercules with disabled USE_HW_CACHES*, DONT_SPECIALIZE_EXECUTE, and PREFETCH_REPS
# Memcopy is included to kernel measurement
#########################################################################################

# Go back to root folder
LOC=$PWD
ITER=10


# Brief: Collects benchmarking data
# $1: Test case: ompgpu, ompgpu_hercules
# $2: Additional benchmark options -z host or -z managed
# $3: CSV file prefix
# $4: New CSV prefix
collect()  {
    # Delete log files and binaries
    cd $PWD/../..
    ./run_benchmark.sh -d

    # Run benchmark
    ./run_benchmark.sh -c $1 -r $ITER -i $ITER -n -k $2

    # Copy and rename files
    cp logs/*.csv $LOC

    cd $LOC
    for i in $3\_* ; do 
        mv "$i" "$4${i#$3}" ; 
    done
}

# Clang YKT map
collect ompgpu "" ompoffload ykt_map

# Hercules map
collect ompgpu_hercules "" ompoffloadhercules hercules_map

# Clang YKT host
collect ompgpu "-z host" ompoffload ykt_host

# Clang YKT managed
collect ompgpu "-z managed" ompoffload ykt_managed

# Hercules host
collect ompgpu_hercules "-z host" ompoffloadhercules hercules_host

# Hercules managed
collect ompgpu_hercules "-z managed" ompoffloadhercules hercules_managed

