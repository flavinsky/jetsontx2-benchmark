#!/bin/bash
# This script runs the python script process_data.py for this folder to generate the data
rm -rf fig
rm -rf results
mkdir results
python3 ../process_data.py ykt_map_gemm.csv ykt_host_gemm.csv ykt_managed_gemm.csv hercules_map_gemm.csv hercules_host_gemm.csv hercules_managed_gemm.csv> results/comparison_gemm.txt

python3 ../process_data.py ykt_map_atax.csv ykt_host_atax.csv ykt_managed_atax.csv hercules_map_atax.csv hercules_host_atax.csv hercules_managed_atax.csv> results/comparison_atax.txt

python3 ../process_data.py ykt_map_doitgen.csv ykt_host_doitgen.csv ykt_managed_doitgen.csv hercules_map_doitgen.csv hercules_host_doitgen.csv hercules_managed_doitgen.csv> results/comparison_doitgen.txt

python3 ../process_data.py ykt_map_correlation.csv ykt_host_correlation.csv ykt_managed_correlation.csv > results/comparison_correlation_ykt.txt

cp fig/ratio_correlation.pdf fig/ratio_correlation_ykt.pdf
cp fig/execution_correlation.pdf fig/execution_correlation_ykt.pdf

python3 ../process_data.py ykt_map_correlation_standard.csv ykt_host_correlation_standard.csv ykt_managed_correlation_standard.csv hercules_map_correlation_standard.csv hercules_host_correlation_standard.csv hercules_managed_correlation_standard.csv > results/comparison_correlation_standard.txt
