

ykt map correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Avg. [s]            1.934E-03        3.424E-03           2.277E-02
Conf. 95% [s]       3.549E-04        7.326E-04           2.002E-04
----------------------------------------------------------------


ykt host correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Avg. [s]            1.864E-03        6.113E-03           4.098E-02
Conf. 95% [s]       4.377E-05        8.005E-05           1.998E-03
----------------------------------------------------------------


ykt managed correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Avg. [s]            1.091E-03        2.090E-03           2.128E-02
Conf. 95% [s]       4.988E-05        1.744E-05           1.632E-04
----------------------------------------------------------------


hercules map correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Avg. [s]            1.401E-03        3.613E-03           2.346E-02
Conf. 95% [s]       5.976E-05        8.549E-05           6.834E-04
----------------------------------------------------------------


hercules host correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Avg. [s]            2.489E-03        8.105E-03           4.336E-02
Conf. 95% [s]       1.168E-04        1.865E-05           8.315E-04
----------------------------------------------------------------


hercules managed correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Avg. [s]            1.484E-03        3.504E-03           2.280E-02
Conf. 95% [s]       1.005E-04        8.657E-05           2.032E-04
----------------------------------------------------------------


ykt map correlation standard / ykt map correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Ratio [%]            103.485          104.798            100.008
Conf. 95% [%]         26.6291          31.3478             1.24354
----------------------------------------------------------------


ykt map correlation standard / ykt host correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Ratio [%]            103.842           56.0197            55.692
Conf. 95% [%]         19.2058          12.0083             2.75857
----------------------------------------------------------------


ykt map correlation standard / ykt managed correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Ratio [%]             177.567         163.838            107.013
Conf. 95% [%]          33.547          35.0831             1.24866
----------------------------------------------------------------


ykt map correlation standard / hercules map correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Ratio [%]            138.335           94.8065            97.1578
Conf. 95% [%]         26.0399          20.4044             2.95658
----------------------------------------------------------------


ykt map correlation standard / hercules host correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Ratio [%]             77.8689         42.2456             52.5385
Conf. 95% [%]         14.7339          9.04008             1.10838
----------------------------------------------------------------


ykt map correlation standard / hercules managed correlation standard

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET
-------------  --------------  ---------------  ------------------
Ratio [%]            130.894           97.7661            99.8724
Conf. 95% [%]         25.5515          21.0523             1.25044
----------------------------------------------------------------
