This folder contains all tests which were performed under the Jailhouse hypervisior.
* benchmark_run_240818_1300_jailhouse: Performance comparison of CUDA-9.0 and OMP offloading with and without the Jailhouse hypervisior
* jail_house_error_test_240818: Checking for numerical errors under the hypervisior
