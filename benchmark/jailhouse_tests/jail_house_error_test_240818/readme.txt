* Comparison of polybench omp offloading and CUDA 9.0 running on Jailhouse and under normal conditions
* Enabled error checking in benchmark
* 1 kernel execution per launch of the binaries
* Frequencies MAXN
* Jetsonclocks on --> All to max on board
* -maxregcount=32 --> Otherwise CUDA 9 utilizes too many registers and number of blocks in SM is restricted
* CUDA 9.0 with Jetpack 3.2.1
* Using Jailhouse Hypervisior
* Equal thread distribution for omp offloading and CUDA
* Used compiler flags -ffp-contract=fast and -fopenmp-nonaliased-map for ompenmp offloading
* No errors could be detected
