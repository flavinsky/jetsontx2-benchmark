

ompoffload atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.804E-03        5.835E-03           2.609E-02        1.010E-01             4.082E-01
Conf. 95% [s]       8.681E-05        6.014E-05           1.566E-04        3.165E-04             3.108E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.804E-03 &       5.835E-03 &          2.609E-02 &       1.010E-01 &            4.082E-01 \\
 Conf. 95\% [s] &      8.681E-05 &       6.014E-05 &          1.566E-04 &       3.165E-04 &            3.108E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffloadjailhouse atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.769E-03        5.866E-03           2.592E-02        1.004E-01             4.064E-01
Conf. 95% [s]       1.474E-05        6.787E-05           6.938E-05        4.564E-04             1.930E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.769E-03 &       5.866E-03 &          2.592E-02 &       1.004E-01 &            4.064E-01 \\
 Conf. 95\% [s] &      1.474E-05 &       6.787E-05 &          6.938E-05 &       4.564E-04 &            1.930E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


cuda atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.929E-03        6.460E-03           2.941E-02        1.180E-01             4.802E-01
Conf. 95% [s]       3.126E-05        1.600E-05           5.566E-05        3.017E-04             9.036E-04
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.929E-03 &       6.460E-03 &          2.941E-02 &       1.180E-01 &            4.802E-01 \\
 Conf. 95\% [s] &      3.126E-05 &       1.600E-05 &          5.566E-05 &       3.017E-04 &            9.036E-04 \\
\hline
\end{tabular}
----------------------------------------------------------------


cudajailhouse atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Avg. [s]            1.922E-03        6.289E-03           2.922E-02        1.177E-01             4.796E-01
Conf. 95% [s]       6.776E-06        3.216E-05           4.934E-05        2.156E-04             1.525E-03
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Avg. [s]      &      1.922E-03 &       6.289E-03 &          2.922E-02 &       1.177E-01 &            4.796E-01 \\
 Conf. 95\% [s] &      6.776E-06 &       3.216E-05 &          4.934E-05 &       2.156E-04 &            1.525E-03 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / ompoffload atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           100.232          100.011            100.004          100.001                100.006
Conf. 95% [%]         6.81825          1.45767            0.849139         0.443264               1.07691
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      100.232   &       100.011   &         100.004    &      100.001    &            100.006   \\
 Conf. 95\% [\%] &        6.81825 &         1.45767 &           0.849139 &        0.443264 &              1.07691 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / ompoffloadjailhouse atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]           101.965           99.4842           100.651          100.601               100.447
Conf. 95% [%]         4.98022          1.54139            0.661659         0.555541              0.901482
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &      101.965   &        99.4842  &         100.651    &      100.601    &           100.447    \\
 Conf. 95\% [\%] &        4.98022 &         1.54139 &           0.661659 &        0.555541 &             0.901482 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / cuda atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            93.5081         90.3254             88.7137          85.6064               85.0015
Conf. 95% [%]         4.74807         0.957445            0.558491         0.346281              0.666715
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       93.5081  &       90.3254   &          88.7137   &       85.6064   &            85.0015   \\
 Conf. 95\% [\%] &        4.74807 &        0.957445 &           0.558491 &        0.346281 &             0.666715 \\
\hline
\end{tabular}
----------------------------------------------------------------


ompoffload atax / cudajailhouse atax

                 MINI_DATASET    SMALL_DATASET    STANDARD_DATASET    LARGE_DATASET    EXTRALARGE_DATASET
-------------  --------------  ---------------  ------------------  ---------------  --------------------
Ratio [%]            93.8602          92.7955            89.273           85.8122               85.1064
Conf. 95% [%]         4.52943          1.06768            0.556798         0.311531              0.702303
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       93.8602  &        92.7955  &          89.273    &       85.8122   &            85.1064   \\
 Conf. 95\% [\%] &        4.52943 &         1.06768 &           0.556798 &        0.311531 &             0.702303 \\
\hline
\end{tabular}
----------------------------------------------------------------
