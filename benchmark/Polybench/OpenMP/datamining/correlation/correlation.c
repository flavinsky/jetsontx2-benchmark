/* POLYBENCH/GPU-OPENMP
 *
 * This file is a part of the Polybench/GPU-OpenMP suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>

/* Include benchmark-specific header. */
/* Default data type is double, default size is 4000. */
#include "correlation.h"

#include <polybenchUtilFuncts.h>

//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 1.05
#define sqrt_of_array_cell(x,j) sqrt(x[j])
#define FLOAT_N 1.2f //3214212.01f
#define EPS 0.005f
#define NOF_THREADS (16)

void correlation(int m, int n, DATA_TYPE POLYBENCH_2D(data, M, N, m, n), DATA_TYPE POLYBENCH_1D(mean, M, m), DATA_TYPE POLYBENCH_1D(stddev, M, m),
                 DATA_TYPE POLYBENCH_2D(symmat, M, N, m, n))
{
    int i, j, j1, j2;

    // Determine mean of column vectors of input data matrix
    for (j = 0; j < _PB_M; j++)
    {
        mean[j] = 0.0;

        for (i = 0; i < _PB_N; i++)
        {
            mean[j] += data[i][j];
        }

        mean[j] /= (DATA_TYPE)FLOAT_N;
    }

    // Determine standard deviations of column vectors of data matrix.
    for (j = 0; j < _PB_M; j++)
    {
        stddev[j] = 0.0;

        for (i = 0; i < _PB_N; i++)
        {
            stddev[j] += (data[i][j] - mean[j]) * (data[i][j] - mean[j]);
        }

        stddev[j] /= FLOAT_N;
        stddev[j] = sqrt_of_array_cell(stddev, j);
        stddev[j] = stddev[j] <= EPS ? 1.0 : stddev[j];
    }

    // Center and reduce the column vectors.
    for (i = 0; i < _PB_N; i++)
    {
        for (j = 0; j < _PB_M; j++)
        {
            data[i][j] -= mean[j];
            data[i][j] /= (sqrt(FLOAT_N)*stddev[j]) ;
        }
    }

    // Calculate the m * m correlation matrix.
    for (j1 = 0; j1 < _PB_M-1; j1++)
    {
        symmat[j1][j1] = 1.0;

        for (j2 = j1+1; j2 < _PB_M; j2++)
        {
            symmat[j1][j2] = 0.0;

            for (i = 0; i < _PB_N; i++)
            {
                symmat[j1][j2] += (data[i][j1] * data[i][j2]);
            }

            symmat[j2][j1] = symmat[j1][j2];
        }
    }

    symmat[M-1][M-1] = 1.0;
}


void compareResults(int m, int n, DATA_TYPE POLYBENCH_2D(symmat, M, N, m, n), DATA_TYPE POLYBENCH_2D(symmat_outputFromGpu, M, N, m, n))
{
    int i,j,fail;
    fail = 0;

    for (i=0; i < m; i++)
    {
        for (j=0; j < n; j++)
        {
            if (percentDiff(symmat[i][j], symmat_outputFromGpu[i][j]) > PERCENT_DIFF_ERROR_THRESHOLD)
            {
                fail++;
            }
        }
    }

    // print results
    printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %4.2f Percent: %d\n", PERCENT_DIFF_ERROR_THRESHOLD, fail);
}

/* Array initialization. */
static
void init_array (int m,
                 int n,
                 DATA_TYPE *float_n,
                 DATA_TYPE POLYBENCH_2D(data,M,N,m,n))
{
    int i, j;

    *float_n = 1.2;

    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            data[i][j] = ((DATA_TYPE) i*j) / M;
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int m,
                 DATA_TYPE POLYBENCH_2D(symmat,M,M,m,m))

{
    int i, j;

    for (i = 0; i < m; i++)
        for (j = 0; j < m; j++) {
            fprintf (stderr, DATA_PRINTF_MODIFIER, symmat[i][j]);
            if ((i * m + j) % 20 == 0) fprintf (stderr, "\n");
        }
    fprintf (stderr, "\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_correlation(int m, int n,
                        DATA_TYPE float_n,
                        DATA_TYPE POLYBENCH_2D(data,M,N,m,n),
                        DATA_TYPE POLYBENCH_2D(symmat,M,M,m,m),
                        DATA_TYPE POLYBENCH_1D(mean,M,m),
                        DATA_TYPE POLYBENCH_1D(stddev,M,m))
{
    int i, j, j1, j2;

    DATA_TYPE eps = 0.1f;

#define sqrt_of_array_cell(x,j) sqrt(x[j])

#pragma scop
    /* Determine mean of column vectors of input data matrix */
#pragma omp parallel
    {
#pragma omp for private (i)
        for (j = 0; j < _PB_M; j++)
        {
            mean[j] = 0.0;
            for (i = 0; i < _PB_N; i++)
                mean[j] += data[i][j];
            mean[j] /= float_n;
        }
        /* Determine standard deviations of column vectors of data matrix. */
#pragma omp for private (i)
        for (j = 0; j < _PB_M; j++)
        {
            stddev[j] = 0.0;
            for (i = 0; i < _PB_N; i++)
                stddev[j] += (data[i][j] - mean[j]) * (data[i][j] - mean[j]);
            stddev[j] /= float_n;
            stddev[j] = sqrt_of_array_cell(stddev, j);
            /* The following in an inelegant but usual way to handle
       near-zero std. dev. values, which below would cause a zero-
       divide. */
            stddev[j] = stddev[j] <= eps ? 1.0 : stddev[j];
        }

        /* Center and reduce the column vectors. */
#pragma omp for private (j)
        for (i = 0; i < _PB_N; i++)
            for (j = 0; j < _PB_M; j++)
            {
                data[i][j] -= mean[j];
                data[i][j] /= sqrt(float_n) * stddev[j];
            }

        /* Calculate the m * m correlation matrix. */
#pragma omp for private (j2, i)
        for (j1 = 0; j1 < _PB_M-1; j1++)
        {
            symmat[j1][j1] = 1.0;
            for (j2 = j1+1; j2 < _PB_M; j2++)
            {
                symmat[j1][j2] = 0.0;
                for (i = 0; i < _PB_N; i++)
                    symmat[j1][j2] += (data[i][j1] * data[i][j2]);
                symmat[j2][j1] = symmat[j1][j2];
            }
        }
    }
#pragma endscop
    symmat[_PB_M-1][_PB_M-1] = 1.0;
}

int main(int argc, char** argv)
{
    /* Retrieve problem size. */
    int n = N;
    int m = M;

    /* Variable declaration/allocation. */
    DATA_TYPE float_n;
    POLYBENCH_2D_ARRAY_DECL(data,DATA_TYPE,M,N,m,n);
    POLYBENCH_2D_ARRAY_DECL(symmat,DATA_TYPE,M,M,m,m);
    POLYBENCH_1D_ARRAY_DECL(mean,DATA_TYPE,M,m);
    POLYBENCH_1D_ARRAY_DECL(stddev,DATA_TYPE,M,m);
    POLYBENCH_2D_ARRAY_DECL(symmat_cpu,DATA_TYPE,M,N,m,n);

#ifdef PRINT_SIZE
    printf("M:%u, N:%u\n",m, n);
#endif
#ifdef RUN_ON_CPU

    init_array (m, n, &float_n, POLYBENCH_ARRAY(data));
    /* Start timer. */
    polybench_start_instruments;

    correlation(m, n, POLYBENCH_ARRAY(data), POLYBENCH_ARRAY(mean), POLYBENCH_ARRAY(stddev), POLYBENCH_ARRAY(symmat_cpu));

    /* Stop and print timer. */
    polybench_stop_instruments;
    printf("Time CPU:\n");
    polybench_print_instruments;
#endif
    for(int i= 0; i<COMP_REPETITION; i++){
        /* Initialize array(s). */
        init_array (m, n, &float_n, POLYBENCH_ARRAY(data));

        /* Start timer. */
        polybench_start_instruments;
        /* Run kernel. */
        kernel_correlation (m, n, float_n,
                            POLYBENCH_ARRAY(data),
                            POLYBENCH_ARRAY(symmat),
                            POLYBENCH_ARRAY(mean),
                            POLYBENCH_ARRAY(stddev));
        /* Stop and print timer. */
        polybench_stop_instruments;

#ifdef RUN_ON_CPU
        printf("Iteration: %u, TIME OMP:\n", i);
#endif

        polybench_print_instruments;

#ifdef RUN_ON_CPU
        compareResults(m, n, POLYBENCH_ARRAY(symmat_cpu), POLYBENCH_ARRAY(symmat));
#endif
    }

#ifndef RUN_ON_CPU //prevent dead code elimination

    /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
    polybench_prevent_dce(print_array(m, POLYBENCH_ARRAY(symmat)));

#endif //RUN_ON_CPU
    /* Be clean. */
    POLYBENCH_FREE_ARRAY(data);
    POLYBENCH_FREE_ARRAY(symmat);
    POLYBENCH_FREE_ARRAY(mean);
    POLYBENCH_FREE_ARRAY(stddev);
    POLYBENCH_FREE_ARRAY(symmat_cpu);

    return 0;
}
