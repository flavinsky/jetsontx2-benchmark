/* POLYBENCH/GPU-OPENMP
 *
 * This file is a part of the Polybench/GPU-OpenMP suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>

/* Include benchmark-specific header. */
/* Default data type is double, default size is 4000. */
#include "atax.h"


//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 0.5

//define a small float value
#define SMALL_FLOAT_VAL 0.00000001f

#define NOF_THREADS (16)

float absVal(float a)
{
    if(a < 0)
    {
        return (a * -1);
    }
    else
    {
        return a;
    }
}



float percentDiff(double val1, double val2)
{
    if ((absVal(val1) < 0.01) && (absVal(val2) < 0.01))
    {
        return 0.0f;
    }

    else
    {
        return 100.0f * (absVal(absVal(val1 - val2) / absVal(val1 + SMALL_FLOAT_VAL)));
    }
}

void compareResults(int ny, DATA_TYPE POLYBENCH_1D(z,NY,ny), DATA_TYPE POLYBENCH_1D(z_outputFromGpu,NY,ny))
{
    int i, fail;
    fail = 0;

    for (i=0; i<ny; i++)
    {
        if (percentDiff(z[i], z_outputFromGpu[i]) > PERCENT_DIFF_ERROR_THRESHOLD)
        {
            fail++;
        }
    }

    // print results
    printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %4.2f Percent: %d\n", PERCENT_DIFF_ERROR_THRESHOLD, fail);
}

void atax_cpu(int nx, int ny, DATA_TYPE POLYBENCH_2D(A,NX,NY,nx,ny), DATA_TYPE POLYBENCH_1D(x,NY,ny), DATA_TYPE POLYBENCH_1D(y,NY,ny),
              DATA_TYPE POLYBENCH_1D(tmp,NX,nx))
{
    int i,j;

    for (i= 0; i < _PB_NY; i++)
    {
        y[i] = 0;
    }

    for (i = 0; i < _PB_NX; i++)
    {
        tmp[i] = 0;

        for (j = 0; j < _PB_NY; j++)
        {
            tmp[i] = tmp[i] + A[i][j] * x[j];
        }

        for (j = 0; j < _PB_NY; j++)
        {
            y[j] = y[j] + A[i][j] * tmp[i];
        }
    }
}

/* Array initialization. */
static
void init_array (int nx, int ny,
                 DATA_TYPE POLYBENCH_2D(A,NX,NY,nx,ny),
                 DATA_TYPE POLYBENCH_1D(x,NY,ny))
{
    int i, j;

    for (i = 0; i < ny; i++)
        x[i] = i * M_PI;
    for (i = 0; i < nx; i++)
        for (j = 0; j < ny; j++)
            A[i][j] = ((DATA_TYPE) i*(j+1)) / nx;
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int nx,
                 DATA_TYPE POLYBENCH_1D(y,NX,nx))

{
    int i;

    for (i = 0; i < nx; i++) {
        fprintf (stderr, DATA_PRINTF_MODIFIER, y[i]);
        if (i % 20 == 0) fprintf (stderr, "\n");
    }
    fprintf (stderr, "\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_atax(int nx, int ny,
                 DATA_TYPE POLYBENCH_2D(A,NX,NY,nx,ny),
                 DATA_TYPE POLYBENCH_1D(x,NY,ny),
                 DATA_TYPE POLYBENCH_1D(y,NY,ny),
                 DATA_TYPE POLYBENCH_1D(tmp,NX,nx))
{
    int i, j;
#pragma scop
//#pragma omp parallel
    {
#pragma omp parallel for
        for (i = 0; i < _PB_NY; i++)
            y[i] = 0;
#pragma omp parallel for private (j)
        for (i = 0; i < _PB_NX; i++)
        {
            tmp[i] = 0;
            for (j = 0; j < _PB_NY; j++)
                tmp[i] = tmp[i] + A[i][j] * x[j];
            for (j = 0; j < _PB_NY; j++)
                y[j] = y[j] + A[i][j] * tmp[i];
        }
    }
#pragma endscop
}


int main(int argc, char** argv)
{
    /* Retrieve problem size. */
    int nx = NX;
    int ny = NY;

    /* Variable declaration/allocation. */
    POLYBENCH_2D_ARRAY_DECL(A, DATA_TYPE, NX, NY, nx, ny);
    POLYBENCH_1D_ARRAY_DECL(x, DATA_TYPE, NY, ny);
    POLYBENCH_1D_ARRAY_DECL(y, DATA_TYPE, NY, ny);
    POLYBENCH_1D_ARRAY_DECL(y_cpu, DATA_TYPE, NY, ny);
    POLYBENCH_1D_ARRAY_DECL(tmp, DATA_TYPE, NX, nx);

#ifdef PRINT_SIZE
    printf("NX:%u, NY:%u\n",nx, ny);
#endif
#ifdef RUN_ON_CPU
    init_array (nx, ny, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(x));
    /* Start timer. */
    polybench_start_instruments;

    atax_cpu(nx, ny, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(x), POLYBENCH_ARRAY(y_cpu), POLYBENCH_ARRAY(tmp));

    /* Stop and print timer. */
    polybench_stop_instruments;
    printf("Time CPU:\n");
    polybench_print_instruments;
#endif
    for(int i= 0; i<COMP_REPETITION; i++){
        /* Initialize array(s). */
        init_array (nx, ny, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(x));

        /* Start timer. */
        polybench_start_instruments;

        /* Run kernel. */
        kernel_atax (nx, ny,
                     POLYBENCH_ARRAY(A),
                     POLYBENCH_ARRAY(x),
                     POLYBENCH_ARRAY(y),
                     POLYBENCH_ARRAY(tmp));

        /* Stop and print timer. */
        polybench_stop_instruments;

#ifdef RUN_ON_CPU
    printf("Iteration: %u, TIME OMP:\n", i);
#endif

        polybench_print_instruments;

#ifdef RUN_ON_CPU
    compareResults(ny, POLYBENCH_ARRAY(y_cpu), POLYBENCH_ARRAY(y));
#endif
    }

#ifndef RUN_ON_CPU //prevent dead code elimination

    /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
    polybench_prevent_dce(print_array(nx, POLYBENCH_ARRAY(y)));

#endif //RUN_ON_CPU

    /* Be clean. */
    POLYBENCH_FREE_ARRAY(A);
    POLYBENCH_FREE_ARRAY(x);
    POLYBENCH_FREE_ARRAY(y);
    POLYBENCH_FREE_ARRAY(y_cpu);
    POLYBENCH_FREE_ARRAY(tmp);

    return 0;
}
