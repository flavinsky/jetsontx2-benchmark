/* POLYBENCH/GPU-OPENMP
 *
 * This file is a part of the Polybench/GPU-OpenMP suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>

/* Include benchmark-specific header. */
/* Default data type is double, default size is 4000. */
#include "gemm.h"

//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 0.05

//define a small float value
#define SMALL_FLOAT_VAL 0.00000001f

float absVal(float a)
{
    if(a < 0)
    {
        return (a * -1);
    }
    else
    {
        return a;
    }
}



float percentDiff(double val1, double val2)
{
    if ((absVal(val1) < 0.01) && (absVal(val2) < 0.01))
    {
        return 0.0f;
    }

    else
    {
        return 100.0f * (absVal(absVal(val1 - val2) / absVal(val1 + SMALL_FLOAT_VAL)));
    }
}


void gemm(int ni, int nj, int nk, DATA_TYPE alpha, DATA_TYPE beta, DATA_TYPE POLYBENCH_2D(A,NI,NK,ni,nk),
          DATA_TYPE POLYBENCH_2D(B,NK,NJ,nk,nj), DATA_TYPE POLYBENCH_2D(C,NI,NJ,ni,nj))
{
    int i,j,k;

    for (i = 0; i < _PB_NI; i++)
    {
        for (j = 0; j < _PB_NJ; j++)
        {
            C[i][j] *= beta;

            for (k = 0; k < _PB_NK; ++k)
            {
                C[i][j] += alpha * A[i][k] * B[k][j];
            }
        }
    }
}

/* Array initialization. */
static
void init_array(int ni, int nj, int nk,
                DATA_TYPE *alpha,
                DATA_TYPE *beta,
                DATA_TYPE POLYBENCH_2D(C,NI,NJ,ni,nj),
                DATA_TYPE POLYBENCH_2D(A,NI,NK,ni,nk),
                DATA_TYPE POLYBENCH_2D(B,NK,NJ,nk,nj))
{
    int i, j;

    *alpha = 32412;
    *beta = 2123;
    for (i = 0; i < ni; i++)
        for (j = 0; j < nj; j++)
            C[i][j] = ((DATA_TYPE) i*j) / ni;
    for (i = 0; i < ni; i++)
        for (j = 0; j < nk; j++)
            A[i][j] = ((DATA_TYPE) i*j) / ni;
    for (i = 0; i < nk; i++)
        for (j = 0; j < nj; j++)
            B[i][j] = ((DATA_TYPE) i*j) / ni;
}

void compareResults(int ni, int nj, DATA_TYPE POLYBENCH_2D(C,NI,NJ,ni,nj), DATA_TYPE POLYBENCH_2D(C_outputFromGpu,NI,NJ,ni,nj))
{
    int i, j, fail;
    fail = 0;

    // Compare CPU and GPU outputs
    for (i=0; i < ni; i++)
    {
        for (j=0; j < nj; j++)
        {
            if (percentDiff(C[i][j], C_outputFromGpu[i][j]) > PERCENT_DIFF_ERROR_THRESHOLD)
            {
                fail++;
            }
        }
    }

    // Print results
    printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %4.2f Percent: %d\n", PERCENT_DIFF_ERROR_THRESHOLD, fail);
}

/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int ni, int nj,
                 DATA_TYPE POLYBENCH_2D(C,NI,NJ,ni,nj))
{
    int i, j;

    for (i = 0; i < ni; i++)
        for (j = 0; j < nj; j++) {
            fprintf (stderr, DATA_PRINTF_MODIFIER, C[i][j]);
            if ((i * ni + j) % 20 == 0) fprintf (stderr, "\n");
        }
    fprintf (stderr, "\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_gemm(int ni, int nj, int nk,
                 DATA_TYPE alpha,
                 DATA_TYPE beta,
                 DATA_TYPE POLYBENCH_2D(C,NI,NJ,ni,nj),
                 DATA_TYPE POLYBENCH_2D(A,NI,NK,ni,nk),
                 DATA_TYPE POLYBENCH_2D(B,NK,NJ,nk,nj))
{
    int i, j, k;
#pragma scop
#pragma omp parallel
    {
        /* C := alpha*A*B + beta*C */
#pragma omp for private (j, k)
        for (i = 0; i < _PB_NI; i++)
            for (j = 0; j < _PB_NJ; j++)
            {
                C[i][j] *= beta;
                for (k = 0; k < _PB_NK; ++k)
                    C[i][j] += alpha * A[i][k] * B[k][j];
            }
    }
#pragma endscop
}


int main(int argc, char** argv)
{
    /* Retrieve problem size. */
    int ni = NI;
    int nj = NJ;
    int nk = NK;

    /* Variable declaration/allocation. */
    DATA_TYPE alpha;
    DATA_TYPE beta;
    POLYBENCH_2D_ARRAY_DECL(C_cpu,DATA_TYPE,NI,NJ,ni,nj);
    POLYBENCH_2D_ARRAY_DECL(C,DATA_TYPE,NI,NJ,ni,nj);
    POLYBENCH_2D_ARRAY_DECL(A,DATA_TYPE,NI,NK,ni,nk);
    POLYBENCH_2D_ARRAY_DECL(B,DATA_TYPE,NK,NJ,nk,nj);

#ifdef PRINT_SIZE
    printf("NI:%u, NJ:%u, NK:%u\n",nk, nj, nk);
#endif
#ifdef RUN_ON_CPU
        init_array (ni, nj, nk, &alpha, &beta,
                    POLYBENCH_ARRAY(C),
                    POLYBENCH_ARRAY(A),
                    POLYBENCH_ARRAY(B));
    /* Start timer. */
    polybench_start_instruments;

    gemm(ni, nj, nk, alpha, beta, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(B), POLYBENCH_ARRAY(C_cpu));

    /* Stop and print timer. */
    polybench_stop_instruments;
    printf("Time CPU:\n");
    polybench_print_instruments;

#endif
    for(int i= 0; i<COMP_REPETITION; i++){
        /* Initialize array(s). */
        init_array (ni, nj, nk, &alpha, &beta,
                    POLYBENCH_ARRAY(C),
                    POLYBENCH_ARRAY(A),
                    POLYBENCH_ARRAY(B));

        /* Start timer. */
        polybench_start_instruments;

        /* Run kernel. */
        kernel_gemm (ni, nj, nk,
                     alpha, beta,
                     POLYBENCH_ARRAY(C),
                     POLYBENCH_ARRAY(A),
                     POLYBENCH_ARRAY(B));

        /* Stop and print timer. */
        polybench_stop_instruments;

#ifdef RUN_ON_CPU
    printf("Iteration: %u, TIME OMP:\n", i);
#endif
        polybench_print_instruments;

#ifdef RUN_ON_CPU
    compareResults(ni, nj, POLYBENCH_ARRAY(C_cpu), POLYBENCH_ARRAY(C));
#endif
    }

#ifndef RUN_ON_CPU
//prevent dead code elimination
    /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
    polybench_prevent_dce(print_array(ni, nj,  POLYBENCH_ARRAY(C)));
#endif

    /* Be clean. */
    POLYBENCH_FREE_ARRAY(C);
    POLYBENCH_FREE_ARRAY(C_cpu);
    POLYBENCH_FREE_ARRAY(A);
    POLYBENCH_FREE_ARRAY(B);

    return 0;
}
