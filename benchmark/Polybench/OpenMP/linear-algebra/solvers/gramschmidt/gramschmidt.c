/* POLYBENCH/GPU-OPENMP
 *
 * This file is a part of the Polybench/GPU-OpenMP suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>
#include <polybenchUtilFuncts.h>

/* Include benchmark-specific header. */
/* Default data type is double, default size is 512. */
#include "gramschmidt.h"

//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 0.05

void gramschmidt(int ni, int nj, DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj), DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj), DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{
    int i,j,k;
    DATA_TYPE nrm;
    for (k = 0; k < _PB_NJ; k++)
    {
        nrm = 0;
        for (i = 0; i < _PB_NI; i++)
        {
            nrm += A[i][k] * A[i][k];
        }

        R[k][k] = sqrt(nrm);
        for (i = 0; i < _PB_NI; i++)
        {
            Q[i][k] = A[i][k] / R[k][k];
        }

        for (j = k + 1; j < _PB_NJ; j++)
        {
            R[k][j] = 0;
            for (i = 0; i < _PB_NI; i++)
            {
                R[k][j] += Q[i][k] * A[i][j];
            }
            for (i = 0; i < _PB_NI; i++)
            {
                A[i][j] = A[i][j] - Q[i][k] * R[k][j];
            }
        }
    }
}


void compareResults(int ni, int nj, DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj), DATA_TYPE POLYBENCH_2D(A_outputFromGpu,NI,NJ,ni,nj))
{
    int i, j, fail;
    fail = 0;

    for (i=0; i < ni; i++)
    {
        for (j=0; j < nj; j++)
        {
            if (percentDiff(A[i][j], A_outputFromGpu[i][j]) > PERCENT_DIFF_ERROR_THRESHOLD)
            {
                fail++;
                //printf("i: %d, j: %d - A_CPU: %f, A_GPU: %f\n",i,j,A[i][j],A_outputFromGpu[i][j]);
            }
        }
    }

    // Print results
    printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %4.2f Percent: %d\n", PERCENT_DIFF_ERROR_THRESHOLD, fail);
}

/* Array initialization. */
static
void init_array(int ni, int nj,
                DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj),
                DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj),
                DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{
    int i, j;

    for (i = 0; i < ni; i++)
        for (j = 0; j < nj; j++) {
            A[i][j] = (((DATA_TYPE) ((i*j) % ni) / ni )*100) + 10; //((DATA_TYPE) i*j) / ni;
            Q[i][j] = 0.0; //((DATA_TYPE) i*(j+1)) / nj;
        }
    for (i = 0; i < nj; i++)
        for (j = 0; j < nj; j++)
            R[i][j] = 0.0; //((DATA_TYPE) i*(j+2)) / nj;
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int ni, int nj,
                 DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj),
                 DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj),
                 DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{
    int i, j;

    for (i = 0; i < ni; i++)
        for (j = 0; j < nj; j++) {
            if ((i*nj+j) % 20 == 0) fprintf (stderr, "\n");
            fprintf (stderr, DATA_PRINTF_MODIFIER, R[i][j]);
            //fprintf (stderr, DATA_PRINTF_MODIFIER, A[i][j]);
            //if (i % 20 == 0) fprintf (stderr, "\n");
        }
    fprintf (stderr, "\n");
    for (i = 0; i < nj; i++)
        for (j = 0; j < nj; j++) {
            if ((i*nj+j) % 20 == 0) fprintf (stderr, "\n");
            fprintf (stderr, DATA_PRINTF_MODIFIER, R[i][j]);
            //fprintf (stderr, DATA_PRINTF_MODIFIER, R[i][j]);
            //if (i % 20 == 0) fprintf (stderr, "\n");
        }
    fprintf (stderr, "\n");
    for (i = 0; i < ni; i++)
        for (j = 0; j < nj; j++) {
            if ((i*nj+j) % 20 == 0) fprintf (stderr, "\n");
            fprintf (stderr, DATA_PRINTF_MODIFIER, Q[i][j]);
            //fprintf (stderr, DATA_PRINTF_MODIFIER, Q[i][j]);
            //if (i % 20 == 0) fprintf (stderr, "\n");
        }
    fprintf (stderr, "\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_gramschmidt(int ni, int nj,
                        DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj),
                        DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj),
                        DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{

#pragma scop
    for (int k = 0; k < _PB_NJ; k++)
    {
        DATA_TYPE nrm = 0;
        //#pragma omp parallel for schedule(static) reduction(+:nrm)
        for (int i = 0; i < _PB_NI; i++){
            nrm += A[i][k] * A[i][k];
        }

        R[k][k] = sqrt(nrm);

#pragma omp parallel for schedule(static)
        for (int i = 0; i < _PB_NI; i++){
            Q[i][k] = A[i][k] / R[k][k];
        }
#pragma omp parallel for schedule(static)
        for (int j = k + 1; j < _PB_NJ; j++)
        {
            R[k][j] = 0;
            for (int i = 0; i < _PB_NI; i++)
                R[k][j] += Q[i][k] * A[i][j];
            for (int i = 0; i < _PB_NI; i++)
                A[i][j] = A[i][j] - Q[i][k] * R[k][j];
        }
    }
#pragma endscop
}


int main(int argc, char** argv)
{
    /* Retrieve problem size. */
    int ni = NI;
    int nj = NJ;

    /* Variable declaration/allocation. */
    POLYBENCH_2D_ARRAY_DECL(A,DATA_TYPE,NI,NJ,ni,nj);
    POLYBENCH_2D_ARRAY_DECL(A_cpu,DATA_TYPE,NI,NJ,ni,nj);
    POLYBENCH_2D_ARRAY_DECL(R,DATA_TYPE,NJ,NJ,nj,nj);
    POLYBENCH_2D_ARRAY_DECL(Q,DATA_TYPE,NI,NJ,ni,nj);

#ifdef PRINT_SIZE
    printf("NI:%u, NJ:%u\n",ni, nj);
#endif
#ifdef RUN_ON_CPU
    /* Start timer. */
    init_array (ni, nj,
                POLYBENCH_ARRAY(A_cpu),
                POLYBENCH_ARRAY(R),
                POLYBENCH_ARRAY(Q));

    polybench_start_instruments;

    gramschmidt(ni, nj, POLYBENCH_ARRAY(A_cpu), POLYBENCH_ARRAY(R), POLYBENCH_ARRAY(Q));

    polybench_stop_instruments;
    printf("CPU time:\n");
    polybench_print_instruments;
#endif
    for(int i= 0; i<COMP_REPETITION; i++){
        /* Initialize array(s). */
        init_array (ni, nj,
                    POLYBENCH_ARRAY(A),
                    POLYBENCH_ARRAY(R),
                    POLYBENCH_ARRAY(Q));

        /* Start timer. */
        polybench_start_instruments;

        /* Run kernel. */
        kernel_gramschmidt (ni, nj,
                            POLYBENCH_ARRAY(A),
                            POLYBENCH_ARRAY(R),
                            POLYBENCH_ARRAY(Q));

        /* Stop and print timer. */
        polybench_stop_instruments;

#ifdef RUN_ON_CPU
        printf("Iteration: %u, TIME OMP:\n", i);
#endif

        polybench_print_instruments;
#ifdef RUN_ON_CPU
        compareResults(ni, nj, POLYBENCH_ARRAY(A_cpu), POLYBENCH_ARRAY(A));
#endif
    }

#ifndef RUN_ON_CPU //prevent dead code elimination
    /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
    polybench_prevent_dce(print_array(ni, nj, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(R), POLYBENCH_ARRAY(Q)));
#endif //RUN_ON_CPU

    /* Be clean. */
    POLYBENCH_FREE_ARRAY(A);
    POLYBENCH_FREE_ARRAY(A_cpu);
    POLYBENCH_FREE_ARRAY(R);
    POLYBENCH_FREE_ARRAY(Q);

    return 0;
}
