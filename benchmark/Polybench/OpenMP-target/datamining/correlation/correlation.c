/* POLYBENCH/GPU-OPENACC
 *
 * This file is a part of the Polybench/GPU-OpenACC suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */

//#define EXTRALARGE_DATASET
//#define POLYBENCH_DUMP_ARRAYS
//#define DATA_TYPE float
//#define DATA_PRINTF_MODIFIER "%0.2f "

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>

/* Include benchmark-specific header. */
/* Default data type is double, default size is 4000. */
#include "correlation.h"

#if defined(USE_ZEROCOPY) && (defined(YKT_HOST) || defined(YKT_MANAGED))
#include "cuda.h"
  #define OMP_KERNEL1_MAP is_device_ptr(mean, data)
  #define OMP_KERNEL2_MAP is_device_ptr(stddev, data, mean)
  #define OMP_KERNEL3_MAP is_device_ptr(data, mean, stddev)
  #define OMP_KERNEL4_MAP is_device_ptr(symmat, data)
#else
  #define OMP_KERNEL1_MAP
  #define OMP_KERNEL2_MAP
  #define OMP_KERNEL3_MAP
  #define OMP_KERNEL4_MAP
#endif

//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 1.05
#define sqrt_of_array_cell(x,j) sqrt(x[j])
#define FLOAT_N 1.2 //3214212.01f
#define EPS 0.005
#define NOF_THREADS 16

#ifndef COMP_REPETITION
#define COMP_REPETITION 1
#endif

//define a small float value
#define SMALL_FLOAT_VAL 0.00000001

static double absVal(double a)
{
    if(a < 0)
    {
        return (a * -1);
    }
    else
    {
        return a;
    }
}



static double percentDiff(double val1, double val2)
{
    if ((absVal(val1) < 0.01) && (absVal(val2) < 0.01))
    {
        return 0.0f;
    }

    else
    {
        return 100.0f * (absVal(absVal(val1 - val2) / absVal(val1 + SMALL_FLOAT_VAL)));
    }
}

static void correlation(int m, int n, DATA_TYPE POLYBENCH_2D(data, M, N, m, n), DATA_TYPE POLYBENCH_1D(mean, M, m), DATA_TYPE POLYBENCH_1D(stddev, M, m),
                 DATA_TYPE POLYBENCH_2D(symmat, M, N, m, n))
{
    int i, j, j1, j2;

    // Determine mean of column vectors of input data matrix
    for (j = 0; j < _PB_M; j++)
    {
        mean[j] = 0.0;

        for (i = 0; i < _PB_N; i++)
        {
            mean[j] += data[i][j];
        }

        mean[j] /= (DATA_TYPE)FLOAT_N;
    }

    // Determine standard deviations of column vectors of data matrix.
    for (j = 0; j < _PB_M; j++)
    {
        stddev[j] = 0.0;

        for (i = 0; i < _PB_N; i++)
        {
            stddev[j] += (data[i][j] - mean[j]) * (data[i][j] - mean[j]);
        }

        stddev[j] /= FLOAT_N;
        stddev[j] = sqrt_of_array_cell(stddev, j);
        stddev[j] = stddev[j] <= EPS ? 1.0 : stddev[j];
    }

    // Center and reduce the column vectors.
    for (i = 0; i < _PB_N; i++)
    {
        for (j = 0; j < _PB_M; j++)
        {
            data[i][j] -= mean[j];
            data[i][j] /= (sqrt(FLOAT_N)*stddev[j]) ;
        }
    }

    // Calculate the m * m correlation matrix.
    for (j1 = 0; j1 < _PB_M-1; j1++)
    {
        symmat[j1][j1] = 1.0;

        for (j2 = j1+1; j2 < _PB_M; j2++)
        {
            symmat[j1][j2] = 0.0;

            for (i = 0; i < _PB_N; i++)
            {
                symmat[j1][j2] += (data[i][j1] * data[i][j2]);
            }

            symmat[j2][j1] = symmat[j1][j2];
        }
    }

    symmat[M-1][M-1] = 1.0;
}


static void compareResults(int m, int n, DATA_TYPE POLYBENCH_2D(symmat, M, N, m, n), DATA_TYPE POLYBENCH_2D(symmat_outputFromGpu, M, N, m, n))
{
    int i,j,fail;
    fail = 0;

    for (i=0; i < m; i++)
    {
        for (j=0; j < n; j++)
        {
            if (percentDiff(symmat[i][j], symmat_outputFromGpu[i][j]) > PERCENT_DIFF_ERROR_THRESHOLD)
            {
                fail++;
            }
        }
    }

    // print results
    printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %4.2f Percent: %d\n", PERCENT_DIFF_ERROR_THRESHOLD, fail);
}

/* Array initialization. */
static void init_array (int m,
                 int n,
                 DATA_TYPE *float_n,
                 DATA_TYPE POLYBENCH_2D(data,M,N,m,n))
{
    int i, j;

    *float_n = 1.2;

    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            data[i][j] = ((DATA_TYPE) i*j) / M;
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static void print_array(int m,
                 DATA_TYPE POLYBENCH_2D(symmat,M,M,m,m))

{
    int i, j;

    for (i = 0; i < m; i++)
        for (j = 0; j < m; j++) {
            fprintf (stderr, DATA_PRINTF_MODIFIER, symmat[i][j]);
            if ((i * m + j) % 20 == 0) fprintf (stderr, "\n");
        }
    fprintf (stderr, "\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static void kernel_correlation(int m, int n,
                               DATA_TYPE POLYBENCH_2D(data,M,N,m,n),
                               DATA_TYPE POLYBENCH_2D(symmat,M,N,m,n),
                               DATA_TYPE POLYBENCH_1D(mean,M,m),
                               DATA_TYPE POLYBENCH_1D(stddev,M,m))
{


#if !( defined(YKT_HOST) || defined(YKT_MANAGED))
#pragma omp target data map(to: data[0:M][0:N]) map(tofrom: symmat[0:M][0:N]) map(alloc:mean[0:M], stddev[0:M])
#endif
    {
#ifdef OMP_MEAS_KERNEL
        /* Start timer. */
        polybench_start_instruments;
#endif
        /* Determine mean of column vectors of input data matrix */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_teams((size_t)(M + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS) OMP_KERNEL1_MAP
        for (int j = 0; j < M; j++)
        {
            mean[j] = 0.0;
            for (int i = 0; i < N; i++)
                mean[j] += data[i][j];
            mean[j] /= (DATA_TYPE)FLOAT_N;
        }

        /* Determine standard deviations of column vectors of data matrix. */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_teams((size_t)(M + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS) OMP_KERNEL2_MAP
        for (int j = 0; j < M; j++)
        {
            stddev[j] = 0.0;
            for (int i = 0; i < N; i++)
                stddev[j] += (data[i][j] - mean[j]) * (data[i][j] - mean[j]);
            stddev[j] /= FLOAT_N;
            stddev[j] = sqrt_of_array_cell(stddev, j);
            /* The following in an inelegant but usual way to handle
         near-zero std. dev. values, which below would cause a zero-
         divide. */
            stddev[j] = stddev[j] <= EPS ? 1.0 : stddev[j];
        }

        /* Center and reduce the column vectors. */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    collapse(2) \
    num_teams((size_t)(M*N + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS) OMP_KERNEL3_MAP
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                data[i][j] -= mean[j];
                data[i][j] /= sqrt(FLOAT_N) * stddev[j];
            }
        }

        /* Calculate the m * m correlation matrix. */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_teams((size_t)(M + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS) OMP_KERNEL4_MAP
        for (int j1 = 0; j1 < M-1; j1++)
        {
            symmat[j1][j1] = 1.0;
            for (int j2 = j1+1; j2 < M; j2++)
            {
                symmat[j1][j2] = 0.0;
                for (int i = 0; i < N; i++){
                    symmat[j1][j2] += (data[i][j1] * data[i][j2]);
                }
                symmat[j2][j1] = symmat[j1][j2];
            }
        }
#ifdef OMP_MEAS_KERNEL
        /* Stop and print timer. */
        polybench_stop_instruments;
        polybench_print_instruments;
#endif
    }
    symmat[M-1][N-1] = 1.0;
}

int main(int argc, char** argv)
{
    /* Retrieve problem size. */
    int n = N;
    int m = M;

    /* Variable declaration/allocation. */
    DATA_TYPE float_n;
    POLYBENCH_2D_ARRAY_DECL(data,DATA_TYPE,M,N,m,n);
    POLYBENCH_1D_ARRAY_DECL(mean,DATA_TYPE,M,m);
    POLYBENCH_1D_ARRAY_DECL(stddev,DATA_TYPE,M,m);
    POLYBENCH_2D_ARRAY_DECL(symmat,DATA_TYPE,M,N,m,n);
    POLYBENCH_2D_ARRAY_DECL(symmat_cpu,DATA_TYPE,M,N,m,n);

#if defined(USE_ZEROCOPY) && defined(YKT_HOST)
    DATA_TYPE POLYBENCH_2D(POLYBENCH_DECL_VAR(data_target), M, N, m, n);
    DATA_TYPE POLYBENCH_1D(POLYBENCH_DECL_VAR(mean_target), M, m);
    DATA_TYPE POLYBENCH_1D(POLYBENCH_DECL_VAR(stddev_target), M, m);
    DATA_TYPE POLYBENCH_2D(POLYBENCH_DECL_VAR(symmat_target), M, N, m, n);

    if( cudaHostGetDevicePointer((void **) &data_target, data, 0) != cudaSuccess ){
        fprintf(stderr, "Could not get device pointer: cudaHostGetDevicePointer\n");
        exit(-1);
    }
    if( cudaHostGetDevicePointer((void **) &mean_target, mean, 0) != cudaSuccess ){
        fprintf(stderr, "Could not get device pointer: cudaHostGetDevicePointer\n");
        exit(-1);
    }
    if( cudaHostGetDevicePointer((void **) &stddev_target, stddev, 0) != cudaSuccess ){
        fprintf(stderr, "Could not get device pointer: cudaHostGetDevicePointer\n");
        exit(-1);
    }
    if( cudaHostGetDevicePointer((void **) &symmat_target, symmat, 0) != cudaSuccess ){
        fprintf(stderr, "Could not get device pointer: cudaHostGetDevicePointer\n");
        exit(-1);
    }
#endif

#ifdef PRINT_SIZE
    printf("M:%u, N:%u\n",m, n);
#endif
#ifdef RUN_ON_CPU
    init_array (m, n, &float_n, POLYBENCH_ARRAY(data));
    /* Start timer. */
    polybench_start_instruments;

    correlation(m, n, POLYBENCH_ARRAY(data), POLYBENCH_ARRAY(mean), POLYBENCH_ARRAY(stddev), POLYBENCH_ARRAY(symmat_cpu));

    /* Stop and print timer. */
    printf("CPU Time in seconds:\n");
    polybench_stop_instruments;
    polybench_print_instruments;
#endif

    for(int i= 0; i<COMP_REPETITION; i++){
        /* Initialize array(s). */
        init_array (m, n, &float_n, POLYBENCH_ARRAY(data));
#ifndef OMP_MEAS_KERNEL
        /* Start timer. */
        polybench_start_instruments;
#endif

#if defined(USE_ZEROCOPY) && defined(YKT_HOST)
        /* Run kernel. */
        kernel_correlation (m, n,
                            POLYBENCH_ARRAY(data_target),
                            POLYBENCH_ARRAY(symmat_target),
                            POLYBENCH_ARRAY(mean_target),
                            POLYBENCH_ARRAY(stddev_target));
#else
        /* Run kernel. */
        kernel_correlation (m, n,
                            POLYBENCH_ARRAY(data),
                            POLYBENCH_ARRAY(symmat),
                            POLYBENCH_ARRAY(mean),
                            POLYBENCH_ARRAY(stddev));
#endif
#ifndef OMP_MEAS_KERNEL
        /* Stop and print timer. */
        polybench_stop_instruments;
        polybench_print_instruments;
#endif
#ifdef RUN_ON_CPU
        printf("Iteration: %u\n", i);
#endif
#ifdef RUN_ON_CPU
        compareResults(m, n, POLYBENCH_ARRAY(symmat_cpu), POLYBENCH_ARRAY(symmat));
#endif
    }

#ifndef RUN_ON_CPU //prevent dead code elimination

    /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
    polybench_prevent_dce(print_array(m, POLYBENCH_ARRAY(symmat)));

#endif //RUN_ON_CPU



    /* Be clean. */
    POLYBENCH_FREE_ARRAY(data);
    POLYBENCH_FREE_ARRAY(mean);
    POLYBENCH_FREE_ARRAY(stddev);
    POLYBENCH_FREE_ARRAY(symmat);
    POLYBENCH_FREE_ARRAY(symmat_cpu);

    return 0;
}
