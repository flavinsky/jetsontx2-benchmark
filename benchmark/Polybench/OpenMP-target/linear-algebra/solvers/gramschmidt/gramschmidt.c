/* POLYBENCH/GPU-OPENACC
 *
 * This file is a part of the Polybench/GPU-OpenACC suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>

/* Include benchmark-specific header. */
/* Default data type is double, default size is 512. */
#include "gramschmidt.h"

#ifndef COMP_REPETITION
#define COMP_REPETITION 1
#endif

#define NOF_THREADS 256

//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 0.05

//define a small float value
#define SMALL_FLOAT_VAL 0.00000001f


static float absVal(float a)
{
    if(a < 0)
    {
        return (a * -1);
    }
    else
    {
        return a;
    }
}

static float percentDiff(double val1, double val2)
{
    if ((absVal(val1) < 0.01) && (absVal(val2) < 0.01))
    {
        return 0.0f;
    }
    else
    {
        return 100.0f * (absVal(absVal(val1 - val2) / absVal(val1 + SMALL_FLOAT_VAL)));
    }
}

static void gramschmidt(int ni, int nj, DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj), DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj), DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{
    int i,j,k;
    DATA_TYPE nrm;
    for (k = 0; k < _PB_NJ; k++)
    {
        nrm = 0;
        for (i = 0; i < _PB_NI; i++)
        {
            nrm += A[i][k] * A[i][k];
        }

        R[k][k] = sqrt(nrm);
        for (i = 0; i < _PB_NI; i++)
        {
            Q[i][k] = A[i][k] / R[k][k];
        }

        for (j = k + 1; j < _PB_NJ; j++)
        {
            R[k][j] = 0;
            for (i = 0; i < _PB_NI; i++)
            {
                R[k][j] += Q[i][k] * A[i][j];
            }
            for (i = 0; i < _PB_NI; i++)
            {
                A[i][j] = A[i][j] - Q[i][k] * R[k][j];
            }
        }
    }
}


static void compareResults(int ni, int nj, DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj), DATA_TYPE POLYBENCH_2D(A_outputFromGpu,NI,NJ,ni,nj))
{
    int i, j, fail;
    fail = 0;

    for (i=0; i < ni; i++)
    {
        for (j=0; j < nj; j++)
        {
            if (percentDiff(A[i][j], A_outputFromGpu[i][j]) > PERCENT_DIFF_ERROR_THRESHOLD)
            {
                fail++;
                //printf("i: %d, j: %d - A_CPU: %f, A_GPU: %f\n",i,j,A[i][j],A_outputFromGpu[i][j]);
            }
        }
    }

    // Print results
    printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %4.2f Percent: %d\n", PERCENT_DIFF_ERROR_THRESHOLD, fail);
}

/* Array initialization. */
static void init_array(int ni, int nj,
                       DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj),
                       DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj),
                       DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{
    int i, j;

    for (i = 0; i < ni; i++)
        for (j = 0; j < nj; j++) {
            A[i][j] = (((DATA_TYPE) ((i*j) % ni) / ni )*100) + 10; //((DATA_TYPE) i*j) / ni;
            Q[i][j] = 0.0; //((DATA_TYPE) i*(j+1)) / nj;
        }
    for (i = 0; i < nj; i++)
        for (j = 0; j < nj; j++)
            R[i][j] = 0.0; //((DATA_TYPE) i*(j+2)) / nj;
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static void print_array(int ni, int nj,
                        DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj),
                        DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj),
                        DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{
    int i, j;

    for (i = 0; i < ni; i++)
        for (j = 0; j < nj; j++) {
            fprintf (stderr, DATA_PRINTF_MODIFIER, A[i][j]);
            if (i % 20 == 0) fprintf (stderr, "\n");
        }
    fprintf (stderr, "\n");
}

#if 1 //0 == original Polybench ACC
/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static void kernel_gramschmidt(int ni, int nj,
                               DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj),
                               DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj),
                               DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{

#pragma omp target data \
    map(tofrom: A[0:NI][0:NJ]) \
    map(alloc:Q[0:NI][0:NJ], R[0:NJ][0:NJ])
    {
#ifdef OMP_MEAS_KERNEL
        /* Start timer*/
        polybench_start_instruments;
#endif

        for (int k = 0; k < NJ; k++)
        {


#if 1 //--> Change kernel implementation to dummy for loop
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_teams(1) \
    num_threads(1)
            for (int tmp = 0; tmp < 1 ;tmp++){
                DATA_TYPE nrm = 0.0;
                for (int i = 0; i < NI; i++){
                    nrm += A[i][k] * A[i][k];
                }
                R[k][k] = sqrt(nrm);
            }
#else
#pragma omp target
            {
                DATA_TYPE nrm = 0.0;
                for (int i = 0; i < NI; i++){
                    nrm += A[i][k] * A[i][k];
                }
                R[k][k] = sqrt(nrm);
            }
#endif

#pragma omp target teams distribute parallel for\
    schedule(static, 1) \
    num_teams((size_t)(NI + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS)
            for (int i = 0; i < NI; i++){
                Q[i][k] = A[i][k] / R[k][k];
            }

#pragma omp target teams distribute parallel for\
    schedule(static, 1) \
    num_teams((size_t)(NJ + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS)
            for (int j = k + 1; j < NJ; j++){
                R[k][j] = 0.0;
                for (int i = 0; i < NI; i++)
                    R[k][j] += Q[i][k] * A[i][j];
                for (int i = 0; i < NI; i++)
                    A[i][j] -= Q[i][k] * R[k][j];
            }
        }
#ifdef OMP_MEAS_KERNEL
        /* Stop and print timer. */
        polybench_stop_instruments;
        polybench_print_instruments;
#endif
    }
}
#else
/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_gramschmidt(int ni, int nj,
                        DATA_TYPE POLYBENCH_2D(A,NI,NJ,ni,nj),
                        DATA_TYPE POLYBENCH_2D(R,NJ,NJ,nj,nj),
                        DATA_TYPE POLYBENCH_2D(Q,NI,NJ,ni,nj))
{

    //#pragma scop
    //#pragma acc data copy(A,Q,R)
#pragma omp target data \
    map(tofrom: A[0:NI], Q[0:NI], R[0:NI])
    {
        //#pragma acc parallel
        {
#pragma omp target teams distribute parallel for schedule(static, 1) \
    num_teams((size_t)(NJ + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS)
            for (int k = 0; k < NJ; k++)
            {
                DATA_TYPE nrm = 0;
                for (int i = 0; i < NI; i++)
                    nrm += A[i][k] * A[i][k];
                R[k][k] = sqrt(nrm);
                for (int i = 0; i < NI; i++)
                    Q[i][k] = A[i][k] / R[k][k];
                for (int j = k + 1; j < NJ; j++)
                {
                    R[k][j] = 0;
                    for (int i = 0; i < NI; i++)
                        R[k][j] += Q[i][k] * A[i][j];
                    for (int i = 0; i < NI; i++)
                        A[i][j] = A[i][j] - Q[i][k] * R[k][j];
                }
            }
        }
    }
    //#pragma endscop
}


#endif

int main(int argc, char** argv)
{
    /* Retrieve problem size. */
    int ni = NI;
    int nj = NJ;

    /* Variable declaration/allocation. */
    POLYBENCH_2D_ARRAY_DECL(A,DATA_TYPE,NI,NJ,ni,nj);
    POLYBENCH_2D_ARRAY_DECL(A_cpu,DATA_TYPE,NI,NJ,ni,nj);
    POLYBENCH_2D_ARRAY_DECL(R,DATA_TYPE,NJ,NJ,nj,nj);
    POLYBENCH_2D_ARRAY_DECL(Q,DATA_TYPE,NI,NJ,ni,nj);

#ifdef PRINT_SIZE
    printf("NI:%u, NJ:%u\n",ni, nj);
#endif
#ifdef RUN_ON_CPU
    /* Start timer. */
    init_array (ni, nj,
                POLYBENCH_ARRAY(A_cpu),
                POLYBENCH_ARRAY(R),
                POLYBENCH_ARRAY(Q));

    polybench_start_instruments;

    gramschmidt(ni, nj, POLYBENCH_ARRAY(A_cpu), POLYBENCH_ARRAY(R), POLYBENCH_ARRAY(Q));

    polybench_stop_instruments;
    printf("CPU time:\n");
    polybench_print_instruments;
#endif
    for(int i= 0; i<COMP_REPETITION; i++){
        /* Initialize array(s). */
        init_array (ni, nj,
                    POLYBENCH_ARRAY(A),
                    POLYBENCH_ARRAY(R),
                    POLYBENCH_ARRAY(Q));
#ifndef OMP_MEAS_KERNEL
        /* Start timer. */
        polybench_start_instruments;
#endif
        /* Run kernel. */

        kernel_gramschmidt (ni, nj,
                            POLYBENCH_ARRAY(A),
                            POLYBENCH_ARRAY(R),
                            POLYBENCH_ARRAY(Q));

#ifndef OMP_MEAS_KERNEL
        /* Stop and print timer. */
        polybench_stop_instruments;
        polybench_print_instruments;
#endif
#ifdef RUN_ON_CPU
        printf("Iteration: %u\n", i);
#endif
#ifdef RUN_ON_CPU
        compareResults(ni, nj, POLYBENCH_ARRAY(A_cpu), POLYBENCH_ARRAY(A));
#endif
    }


#ifndef RUN_ON_CPU //prevent dead code elimination

    /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
    polybench_prevent_dce(print_array(ni, nj, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(R), POLYBENCH_ARRAY(Q)));

#endif //RUN_ON_CPU


    /* Be clean. */
    POLYBENCH_FREE_ARRAY(A);
    POLYBENCH_FREE_ARRAY(A_cpu);
    POLYBENCH_FREE_ARRAY(R);
    POLYBENCH_FREE_ARRAY(Q);

    return 0;
}
