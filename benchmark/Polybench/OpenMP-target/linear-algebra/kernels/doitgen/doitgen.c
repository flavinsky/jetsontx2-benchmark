/* POLYBENCH/GPU-OPENACC
 *
 * This file is a part of the Polybench/GPU-OpenACC suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>

/* Include benchmark-specific header. */
/* Default data type is double, default size is 4000. */
#include "doitgen.h"

#if defined(USE_ZEROCOPY) && (defined(YKT_HOST) || defined(YKT_MANAGED))
#include "cuda.h"
#define OMP_KERNEL1_MAP is_device_ptr(sum, A, C4)
#define OMP_KERNEL2_MAP is_device_ptr(sum, A)
#define OMP_KERNEL3_MAP is_device_ptr(sum, A, C4)
#else
#define OMP_KERNEL1_MAP
#define OMP_KERNEL2_MAP
#define OMP_KERNEL3_MAP
#endif

//define the error threshold for the results "not matching"
#define PERCENT_DIFF_ERROR_THRESHOLD 0.05

#define NOF_THREADS 256

#ifndef COMP_REPETITION
#define COMP_REPETITION 1
#endif

//define a small float value
#define SMALL_FLOAT_VAL 0.00000001

static float absVal(float a)
{
    if(a < 0)
    {
        return (a * -1);
    }
    else
    {
        return a;
    }
}



static float percentDiff(double val1, double val2)
{
    if ((absVal(val1) < 0.01) && (absVal(val2) < 0.01))
    {
        return 0.0f;
    }

    else
    {
        return 100.0f * (absVal(absVal(val1 - val2) / absVal(val1 + SMALL_FLOAT_VAL)));
    }
}

/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static void kernel_doitgenCpu(int nr, int nq, int np,
                              DATA_TYPE POLYBENCH_3D(A,NR,NQ,NP,nr,nq,np),
                              DATA_TYPE POLYBENCH_2D(C4,NP,NP,np,np),
                              DATA_TYPE POLYBENCH_3D(sum,NR,NQ,NP,nr,nq,np))
{
    int r, q, p, s;

    for (r = 0; r < _PB_NR; r++)
    {
        for (q = 0; q < _PB_NQ; q++)
        {
            for (p = 0; p < _PB_NP; p++)
            {
                sum[r][q][p] = 0;
                for (s = 0; s < _PB_NP; s++)
                    sum[r][q][p] = sum[r][q][p] + A[r][q][s] * C4[s][p];
            }
            for (p = 0; p < _PB_NR; p++)
                A[r][q][p] = sum[r][q][p];
        }
    }

}


static void compareResults(int nr, int nq, int np, DATA_TYPE POLYBENCH_3D(sum,NR,NQ,NP,nr,nq,np),
                           DATA_TYPE POLYBENCH_3D(sum_outputFromGpu,NR,NQ,NP,nr,nq,np))
{
    int fail = 0;

    for (int r = 0; r < nr; r++)
    {
        for (int q = 0; q < nq; q++)
        {
            for (int p = 0; p < np; p++)
            {
                if (percentDiff(sum[r][q][p], sum_outputFromGpu[r][q][p]) > PERCENT_DIFF_ERROR_THRESHOLD)
                {
                    fail++;
                }
            }
        }
    }
    // Print results
    printf("Number of misses: %d\n", fail);
}

/* Array initialization. */
static void init_array(int nr, int nq, int np,
                       DATA_TYPE POLYBENCH_3D(A,NR,NQ,NP,nr,nq,np),
                       DATA_TYPE POLYBENCH_2D(C4,NP,NP,np,np))
{
    int i, j, k;

    for (i = 0; i < nr; i++)
        for (j = 0; j < nq; j++)
            for (k = 0; k < np; k++)
                A[i][j][k] = ((DATA_TYPE) i*j + k) / np;
    for (i = 0; i < np; i++)
        for (j = 0; j < np; j++)
            C4[i][j] = ((DATA_TYPE) i*j) / np;
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static void print_array(int nr, int nq, int np,
                        DATA_TYPE POLYBENCH_3D(A,NR,NQ,NP,nr,nq,np))
{
    int i, j, k;

    for (i = 0; i < nr; i++)
        for (j = 0; j < nq; j++)
            for (k = 0; k < np; k++) {
                fprintf (stderr, DATA_PRINTF_MODIFIER, A[i][j][k]);
                if (i % 20 == 0) fprintf (stderr, "\n");
            }
    fprintf (stderr, "\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static void kernel_doitgen(int nr, int nq, int np,
                           DATA_TYPE POLYBENCH_3D(A,NR,NQ,NP,nr,nq,np),
                           DATA_TYPE POLYBENCH_2D(C4,NP,NP,np,np),
                           DATA_TYPE POLYBENCH_3D(sum,NR,NQ,NP,nr,nq,np))
{
    //#pragma scop
    //#pragma acc data copy(A) copyin(C4) create(sum)
#if ! (defined(YKT_HOST) || defined(YKT_MANAGED))
#pragma omp target data \
    map(tofrom: A[0:NR][0:NQ][0:NP]) \
    map(to: C4[0:NP][0:NP]) \
    map(tofrom: sum[0:NR][0:NQ][0:NP])
#endif
    {
#ifdef OMP_MEAS_KERNEL
        polybench_start_instruments;
#endif

#if 1
        for (int r = 0; r < NR; r++) {
#pragma omp target teams distribute parallel for \
    schedule(static,1) \
    collapse(2) \
    num_teams((size_t)((NQ*NP) + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS) OMP_KERNEL1_MAP
            for (int q = 0; q < NQ; q++) {
                for (int p = 0; p < NP; p++) {
                    sum[r][q][p] = 0;
                    for (int s = 0; s < NP; s++) {
                        sum[r][q][p] = sum[r][q][p] + A[r][q][s] * C4[s][p];
                    }
                }
            }
#pragma omp target teams distribute parallel for \
    schedule(static,1) \
    collapse(2) \
    num_teams((size_t)((NQ*NP) + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS) OMP_KERNEL2_MAP
            for (int q = 0; q < NQ; q++) {
                for (int p = 0; p < NP; p++) {
                    A[r][q][p] = sum[r][q][p];
                }
            }
        }
#else
#pragma omp target teams distribute parallel for \
    schedule(static,1) \
    num_teams((size_t)((NR) + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS) \
    collapse(2) OMP_KERNEL3_MAP
        for (int r = 0; r < NR; r++) {
            for (int q = 0; q < NQ; q++) {
                //#pragma acc loop
                for (int p = 0; p < NP; p++) {
                    sum[r][q][p] = 0;
                    for (int s = 0; s < NP; s++) {
                        sum[r][q][p] = sum[r][q][p] + A[r][q][s] * C4[s][p];
                    }
                }
                //#pragma acc loop
                for (int p = 0; p < NR; p++) {
                    A[r][q][p] = sum[r][q][p];
                }
            }
        }
#endif //#if 0
#ifdef OMP_MEAS_KERNEL
        /* Stop and print timer. */
        polybench_stop_instruments;
        polybench_print_instruments;
#endif
    }
    //#pragma endscop
}

int main(int argc, char** argv)
{
    /* Retrieve problem size. */
    int nr = NR;
    int nq = NQ;
    int np = NP;

    /* Variable declaration/allocation. */
    POLYBENCH_3D_ARRAY_DECL(A,DATA_TYPE,NR,NQ,NP,nr,nq,np);
    POLYBENCH_3D_ARRAY_DECL(sum,DATA_TYPE,NR,NQ,NP,nr,nq,np);
    POLYBENCH_3D_ARRAY_DECL(sum_cpu,DATA_TYPE,NR,NQ,NP,nr,nq,np);
    POLYBENCH_2D_ARRAY_DECL(C4,DATA_TYPE,NP,NP,np,np);

#if defined(USE_ZEROCOPY) && defined(YKT_HOST)
    DATA_TYPE POLYBENCH_3D(POLYBENCH_DECL_VAR(A_target), NR,NQ,NP,nr,nq,np);
    DATA_TYPE POLYBENCH_3D(POLYBENCH_DECL_VAR(sum_target), NR,NQ,NP,nr,nq,np);
    DATA_TYPE POLYBENCH_2D(POLYBENCH_DECL_VAR(C4_target), NP,NP,np,np);

    if( cudaHostGetDevicePointer((void **) &A_target, A, 0) != cudaSuccess ){
        fprintf(stderr, "Could not get device pointer: cudaHostGetDevicePointer\n");
        exit(-1);
    }
    if( cudaHostGetDevicePointer((void **) &sum_target, sum, 0) != cudaSuccess ){
        fprintf(stderr, "Could not get device pointer: cudaHostGetDevicePointer\n");
        exit(-1);
    }
    if( cudaHostGetDevicePointer((void **) &C4_target, C4, 0) != cudaSuccess ){
        fprintf(stderr, "Could not get device pointer: cudaHostGetDevicePointer\n");
        exit(-1);
    }
#endif


#ifdef PRINT_SIZE
    printf("NR:%u, NQ:%u, NP:%u\n",nr, nq, np);
#endif
#ifdef RUN_ON_CPU

    /* Initialize array(s). */
    init_array (nr, nq, np,
                POLYBENCH_ARRAY(A),
                POLYBENCH_ARRAY(C4));
    /* Start timer. */
    polybench_start_instruments;

    /* Run kernel on CPU */
    kernel_doitgenCpu(nr, nq, np,
                      POLYBENCH_ARRAY(A),
                      POLYBENCH_ARRAY(C4),
                      POLYBENCH_ARRAY(sum_cpu));

    /* Stop and print timer. */
    polybench_stop_instruments;
    polybench_print_instruments;
#endif

    for(int i= 0; i<COMP_REPETITION; i++){
        /* Initialize array(s). */
        init_array (nr, nq, np,
                    POLYBENCH_ARRAY(A),
                    POLYBENCH_ARRAY(C4));

        /* Start timer. */
#ifndef OMP_MEAS_KERNEL
        polybench_start_instruments;
#endif

#if defined(USE_ZEROCOPY) && defined(YKT_HOST)
        /* Run kernel. */
        kernel_doitgen (nr, nq, np,
                        POLYBENCH_ARRAY(A_target),
                        POLYBENCH_ARRAY(C4_target),
                        POLYBENCH_ARRAY(sum_target));
#else
        /* Run kernel. */
        kernel_doitgen (nr, nq, np,
                        POLYBENCH_ARRAY(A),
                        POLYBENCH_ARRAY(C4),
                        POLYBENCH_ARRAY(sum));

#endif

#ifndef OMP_MEAS_KERNEL
        /* Stop and print timer. */
        polybench_stop_instruments;
        polybench_print_instruments;
#endif
#ifdef RUN_ON_CPU
        printf("Iteration: %u\n", i);
#endif
#ifdef RUN_ON_CPU
        compareResults(nr, nq, np, POLYBENCH_ARRAY(sum_cpu), POLYBENCH_ARRAY(sum));
#endif
    }

#ifndef RUN_ON_CPU //prevent dead code elimination

    /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
    polybench_prevent_dce(print_array(nr, nq, np,  POLYBENCH_ARRAY(A)));

#endif //RUN_ON_CPU

    /* Be clean. */
    POLYBENCH_FREE_ARRAY(A);
    POLYBENCH_FREE_ARRAY(sum);
    POLYBENCH_FREE_ARRAY(sum_cpu);
    POLYBENCH_FREE_ARRAY(C4);

    return 0;
}
