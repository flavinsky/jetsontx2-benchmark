
; __CLANG_OFFLOAD_BUNDLE____START__ openmp-nvptx64-nvidia-cuda
; ModuleID = '/home/kreilfla/nfs/prem/benchmark/Polybench/OpenMP-target/linear-algebra/solvers/gramschmidt/gramschmidt.c'
source_filename = "/home/kreilfla/nfs/prem/benchmark/Polybench/OpenMP-target/linear-algebra/solvers/gramschmidt/gramschmidt.c"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%struct.__openmp_nvptx_target_property_ty = type { i8, i32, i32 }
%ident_t = type { i32, i32, i32, i32, i8* }
%struct.__openmp_nvptx_target_property_ty.0 = type { i8, i32, i32 }
%struct.__openmp_nvptx_target_property_ty.1 = type { i8, i32, i32 }

@__omp_offloading_27_2b10e4a_kernel_gramschmidt_l158_property = weak local_unnamed_addr constant %struct.__openmp_nvptx_target_property_ty { i8 1, i32 0, i32 0 }, align 1
@.str = private unnamed_addr constant [23 x i8] c";unknown;unknown;0;0;;\00", align 1
@0 = private unnamed_addr constant %ident_t { i32 0, i32 2050, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0) }, align 8
@__omp_offloading_27_2b10e4a_kernel_gramschmidt_l167_property = weak local_unnamed_addr constant %struct.__openmp_nvptx_target_property_ty.0 zeroinitializer, align 1
@__omp_offloading_27_2b10e4a_kernel_gramschmidt_l175_property = weak local_unnamed_addr constant %struct.__openmp_nvptx_target_property_ty.1 zeroinitializer, align 1
@execution_param = external local_unnamed_addr addrspace(3) global i32, align 4
@ReductionScratchpadPtr = external local_unnamed_addr addrspace(3) global i8*, align 8
@omptarget_nvptx_workFn = external addrspace(3) global i8*, align 8

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #0

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #0

; Function Attrs: convergent nounwind
declare void @llvm.nvvm.barrier0() #1

; Function Attrs: nounwind
define weak void @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l158([128 x double]*, i64, [128 x double]*, i8*) local_unnamed_addr #2 {
  %5 = alloca i8*, align 8
  %6 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %7 = add nsw i32 %6, -32
  tail call fastcc void @__kmpc_kernel_init() #6
  %8 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %9 = add nsw i32 %6, -1
  %10 = and i32 %9, -32
  %11 = icmp eq i32 %8, %10
  %12 = icmp ult i32 %8, %7
  br i1 %12, label %13, label %20

; <label>:13:                                     ; preds = %4
  %14 = bitcast i8** %5 to i8*
  call void @llvm.lifetime.start(i64 8, i8* nonnull %14) #6
  br label %15

; <label>:15:                                     ; preds = %18, %13
  tail call void @llvm.nvvm.barrier0() #6
  call fastcc void @__kmpc_kernel_parallel(i8** nonnull %5) #6
  %16 = load volatile i8*, i8** %5, align 8
  %17 = icmp eq i8* %16, null
  br i1 %17, label %19, label %18

; <label>:18:                                     ; preds = %15
  tail call void @llvm.nvvm.barrier0() #6
  br label %15

; <label>:19:                                     ; preds = %15
  call void @llvm.lifetime.end(i64 8, i8* nonnull %14) #6
  br label %20

; <label>:20:                                     ; preds = %19, %4
  br i1 %11, label %21, label %55

; <label>:21:                                     ; preds = %20
  %22 = shl i64 %1, 32
  %23 = ashr exact i64 %22, 32
  br label %27

; <label>:24:                                     ; preds = %27
  %25 = tail call double @llvm.nvvm.sqrt.rn.d(double %52) #6
  %26 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %23, i64 %23
  store double %25, double* %26, align 8, !tbaa !16
  tail call fastcc void @__kmpc_kernel_deinit() #6
  tail call void @llvm.nvvm.barrier0() #6
  br label %55

; <label>:27:                                     ; preds = %27, %21
  %28 = phi i32 [ 0, %21 ], [ %53, %27 ]
  %29 = phi double [ 0.000000e+00, %21 ], [ %52, %27 ]
  %30 = sext i32 %28 to i64
  %31 = getelementptr inbounds [128 x double], [128 x double]* %0, i64 %30, i64 %23
  %32 = load double, double* %31, align 8, !tbaa !16
  %33 = fmul double %32, %32
  %34 = fadd double %29, %33
  %35 = or i32 %28, 1
  %36 = sext i32 %35 to i64
  %37 = getelementptr inbounds [128 x double], [128 x double]* %0, i64 %36, i64 %23
  %38 = load double, double* %37, align 8, !tbaa !16
  %39 = fmul double %38, %38
  %40 = fadd double %34, %39
  %41 = or i32 %28, 2
  %42 = sext i32 %41 to i64
  %43 = getelementptr inbounds [128 x double], [128 x double]* %0, i64 %42, i64 %23
  %44 = load double, double* %43, align 8, !tbaa !16
  %45 = fmul double %44, %44
  %46 = fadd double %40, %45
  %47 = or i32 %28, 3
  %48 = sext i32 %47 to i64
  %49 = getelementptr inbounds [128 x double], [128 x double]* %0, i64 %48, i64 %23
  %50 = load double, double* %49, align 8, !tbaa !16
  %51 = fmul double %50, %50
  %52 = fadd double %46, %51
  %53 = add nsw i32 %28, 4
  %54 = icmp eq i32 %53, 128
  br i1 %54, label %24, label %27

; <label>:55:                                     ; preds = %20, %24
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #3

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3

; Function Attrs: nounwind
define weak void @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l167([128 x double]*, i64, [128 x double]*, [128 x double]*, i8*) local_unnamed_addr #2 {
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  tail call fastcc void @__kmpc_kernel_init_params(i8* %4)
  tail call fastcc void @__kmpc_spmd_kernel_init() #6
  %10 = bitcast i32* %6 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %10) #6
  store i32 0, i32* %6, align 4, !tbaa !20
  %11 = bitcast i32* %7 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %11) #6
  store i32 127, i32* %7, align 4, !tbaa !20
  %12 = bitcast i32* %8 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %12) #6
  store i32 1, i32* %8, align 4, !tbaa !20
  %13 = bitcast i32* %9 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %13) #6
  store i32 0, i32* %9, align 4, !tbaa !20
  %14 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !22
  %15 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %16 = add nsw i32 %15, -32
  %17 = mul i32 %16, %14
  %18 = add nuw nsw i32 %15, 1023
  %19 = and i32 %18, 992
  %20 = add nuw nsw i32 %19, 1023
  %21 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %22 = and i32 %20, %21
  %23 = add i32 %22, %17
  call void @__kmpc_for_static_init_4_simple_spmd(i8* nonnull bitcast (%ident_t* @0 to i8*), i32 %23, i32 93, i32* nonnull %9, i32* nonnull %6, i32* nonnull %7, i32* nonnull %8, i32 1, i32 1) #6
  %24 = load i32, i32* %6, align 4, !tbaa !20
  %25 = icmp slt i32 %24, 128
  br i1 %25, label %26, label %42

; <label>:26:                                     ; preds = %5
  %27 = shl i64 %1, 32
  %28 = ashr exact i64 %27, 32
  %29 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 %28, i64 %28
  %30 = load i32, i32* %8, align 4, !tbaa !20
  br label %31

; <label>:31:                                     ; preds = %31, %26
  %32 = phi i32 [ %24, %26 ], [ %39, %31 ]
  %33 = sext i32 %32 to i66
  %34 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %33, i64 %28
  %35 = load double, double* %34, align 8, !tbaa !16
  %36 = load double, double* %29, align 8, !tbaa !16
  %37 = fdiv double %35, %36
  %38 = getelementptr inbounds [128 x double], [128 x double]* %0, i64 %33, i64 %28
  store double %37, double* %38, align 8, !tbaa !16
  %39 = add nsw i32 %32, %30
  %40 = icmp slt i32 %39, 128
  br i1 %40, label %31, label %41

; <label>:41:                                     ; preds = %31
  store i32 %39, i32* %6, align 4, !tbaa !20
  br label %42

; <label>:42:                                     ; preds = %5, %41
  call void @llvm.lifetime.end(i64 4, i8* nonnull %13) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %12) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %11) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %10) #6
  ret void
}

; Function Attrs: nounwind
define weak void @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l175(i64, [128 x double]*, [128 x double]*, [128 x double]*, i8*) local_unnamed_addr #2 {
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  tail call fastcc void @__kmpc_kernel_init_params(i8* %4)
  tail call fastcc void @__kmpc_spmd_kernel_init() #6
  %10 = trunc i64 %0 to i32
  %11 = sub nsw i32 127, %10
  %12 = icmp slt i32 %10, 127
  br i1 %12, label %18, label %13

; <label>:13:                                     ; preds = %5
  %14 = bitcast i32* %9 to i8*
  %15 = bitcast i32* %8 to i8*
  %16 = bitcast i32* %7 to i8*
  %17 = bitcast i32* %6 to i8*
  br label %88

; <label>:18:                                     ; preds = %5
  %19 = add nsw i32 %11, -1
  %20 = bitcast i32* %6 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %20) #6
  store i32 0, i32* %6, align 4, !tbaa !20
  %21 = bitcast i32* %7 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %21) #6
  store i32 %19, i32* %7, align 4, !tbaa !20
  %22 = bitcast i32* %8 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %22) #6
  store i32 1, i32* %8, align 4, !tbaa !20
  %23 = bitcast i32* %9 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %23) #6
  store i32 0, i32* %9, align 4, !tbaa !20
  %24 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !22
  %25 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %26 = add nsw i32 %25, -32
  %27 = mul i32 %26, %24
  %28 = add nuw nsw i32 %25, 1023
  %29 = and i32 %28, 992
  %30 = add nuw nsw i32 %29, 1023
  %31 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %32 = and i32 %30, %31
  %33 = add i32 %32, %27
  call void @__kmpc_for_static_init_4_simple_spmd(i8* nonnull bitcast (%ident_t* @0 to i8*), i32 %33, i32 93, i32* nonnull %9, i32* nonnull %6, i32* nonnull %7, i32* nonnull %8, i32 1, i32 1) #6
  %34 = load i32, i32* %6, align 4, !tbaa !20
  %35 = icmp slt i32 %34, %11
  br i1 %35, label %36, label %88

; <label>:36:                                     ; preds = %18
  %37 = shl i64 %0, 32
  %38 = ashr exact i64 %37, 32
  %39 = load i32, i32* %8, align 4, !tbaa !20
  %40 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 0, i64 %38
  br label %41

; <label>:41:                                     ; preds = %72, %36
  %42 = phi i32 [ %34, %36 ], [ %73, %72 ]
  %43 = sub nsw i32 127, %42
  %44 = sext i32 %43 to i64
  %45 = getelementptr inbounds [128 x double], [128 x double]* %1, i64 %38, i64 %44
  store double 0.000000e+00, double* %45, align 8, !tbaa !16
  br label %46



; <label>:46:                                     ; preds = %46, %41
  %47 = phi double [ 0.000000e+00, %41 ], [ %63, %46 ]
  %48 = phi i32 [ 0, %41 ], [ %64, %46 ]
  %49 = sext i32 %48 to i64
  %50 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %49, i64 %38
  %51 = load double, double* %50, align 8, !tbaa !16
  %52 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 %49, i64 %44
  %53 = load double, double* %52, align 8, !tbaa !16
  %54 = fmul double %51, %53
  %55 = fadd double %47, %54
  store double %55, double* %45, align 8, !tbaa !16
  %56 = or i32 %48, 1
  %57 = sext i32 %56 to i64
  %58 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %57, i64 %38
  %59 = load double, double* %58, align 8, !tbaa !16
  %60 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 %57, i64 %44
  %61 = load double, double* %60, align 8, !tbaa !16
  %62 = fmul double %59, %61
  %63 = fadd double %55, %62
  store double %63, double* %45, align 8, !tbaa !16
  %64 = add nsw i32 %48, 2
  %65 = icmp eq i32 %64, 128
  br i1 %65, label %66, label %46

; <label>:66:                                     ; preds = %46
  %67 = load double, double* %40, align 8, !tbaa !16
  %68 = fmul double %63, %67
  %69 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 0, i64 %44
  %70 = load double, double* %69, align 8, !tbaa !16
  %71 = fsub double %70, %68
  store double %71, double* %69, align 8, !tbaa !16
  br label %75

; <label>:72:                                     ; preds = %75
  %73 = add nsw i32 %42, %39
  %74 = icmp slt i32 %73, %11
  br i1 %74, label %41, label %87

; <label>:75:                                     ; preds = %93, %66
  %76 = phi i32 [ 1, %66 ], [ %102, %93 ]
  %77 = load double, double* %45, align 8, !tbaa !16
  %78 = sext i32 %76 to i64
  %79 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %78, i64 %38
  %80 = load double, double* %79, align 8, !tbaa !16
  %81 = fmul double %77, %80
  %82 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 %78, i64 %44
  %83 = load double, double* %82, align 8, !tbaa !16
  %84 = fsub double %83, %81
  store double %84, double* %82, align 8, !tbaa !16
  %85 = add nuw nsw i32 %76, 1
  %86 = icmp eq i32 %85, 128
  br i1 %86, label %72, label %93

; <label>:87:                                     ; preds = %72
  store i32 %73, i32* %6, align 4, !tbaa !20
  br label %88

; <label>:88:                                     ; preds = %13, %18, %87
  %89 = phi i8* [ %17, %13 ], [ %20, %18 ], [ %20, %87 ]
  %90 = phi i8* [ %16, %13 ], [ %21, %18 ], [ %21, %87 ]
  %91 = phi i8* [ %15, %13 ], [ %22, %18 ], [ %22, %87 ]
  %92 = phi i8* [ %14, %13 ], [ %23, %18 ], [ %23, %87 ]
  call void @llvm.lifetime.end(i64 4, i8* nonnull %92) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %91) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %90) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %89) #6
  ret void

; <label>:93:                                     ; preds = %75
  %94 = load double, double* %45, align 8, !tbaa !16
  %95 = sext i32 %85 to i64
  %96 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %95, i64 %38
  %97 = load double, double* %96, align 8, !tbaa !16
  %98 = fmul double %94, %97
  %99 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 %95, i64 %44
  %100 = load double, double* %99, align 8, !tbaa !16
  %101 = fsub double %100, %98
  store double %101, double* %99, align 8, !tbaa !16
  %102 = add nsw i32 %76, 2
  br label %75
}

; Function Attrs: nounwind readnone
declare double @llvm.nvvm.sqrt.rn.d(double) #3

; Function Attrs: nounwind
define internal void @__kmpc_for_static_init_4_simple_spmd(i8* nocapture readnone, i32, i32, i32* nocapture, i32* nocapture, i32* nocapture, i32* nocapture, i32, i32) unnamed_addr #4 {
  %10 = load i32, i32* %4, align 4, !tbaa !23
  %11 = load i32, i32* %5, align 4, !tbaa !23
  switch i32 %2, label %106 [
    i32 33, label %12
    i32 34, label %27
    i32 91, label %50
    i32 92, label %65
    i32 93, label %88
  ]

; <label>:12:                                     ; preds = %9
  %13 = icmp sgt i32 %8, 0
  br i1 %13, label %14, label %27

; <label>:14:                                     ; preds = %12
  %15 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %16 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %17 = mul nsw i32 %16, %8
  %18 = mul nsw i32 %15, %8
  %19 = add nsw i32 %18, %10
  %20 = add nsw i32 %8, -1
  %21 = add i32 %20, %19
  %22 = srem i32 %11, %8
  %23 = sub i32 %11, %22
  %24 = sub i32 %23, %19
  %25 = srem i32 %24, %17
  %26 = icmp eq i32 %25, 0
  br label %119

; <label>:27:                                     ; preds = %12, %9
  %28 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %29 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %30 = sub nsw i32 %11, %10
  %31 = add nsw i32 %30, 1
  %32 = sdiv i32 %31, %29
  %33 = mul nsw i32 %32, %29
  %34 = sub nsw i32 %31, %33
  %35 = icmp sgt i32 %34, %28
  br i1 %35, label %36, label %40

; <label>:36:                                     ; preds = %27
  %37 = add nsw i32 %32, 1
  %38 = mul nsw i32 %37, %28
  %39 = add nsw i32 %38, %10
  br label %44

; <label>:40:                                     ; preds = %27
  %41 = mul nsw i32 %32, %28
  %42 = add i32 %41, %10
  %43 = add i32 %42, %34
  br label %44

; <label>:44:                                     ; preds = %40, %36
  %45 = phi i32 [ %37, %36 ], [ %32, %40 ]
  %46 = phi i32 [ %39, %36 ], [ %43, %40 ]
  %47 = add i32 %45, -1
  %48 = add i32 %47, %46
  %49 = icmp eq i32 %48, %11
  br label %119

; <label>:50:                                     ; preds = %9
  %51 = icmp sgt i32 %8, 0
  br i1 %51, label %52, label %65

; <label>:52:                                     ; preds = %50
  %53 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !22
  %54 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #6, !range !27
  %55 = mul nsw i32 %54, %8
  %56 = mul nsw i32 %53, %8
  %57 = add nsw i32 %56, %10
  %58 = add nsw i32 %8, -1
  %59 = add i32 %58, %57
  %60 = srem i32 %11, %8
  %61 = sub i32 %11, %60
  %62 = sub i32 %61, %57
  %63 = srem i32 %62, %55
  %64 = icmp eq i32 %63, 0
  br label %119

; <label>:65:                                     ; preds = %50, %9
  %66 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !22
  %67 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #6, !range !27
  %68 = sub nsw i32 %11, %10
  %69 = add nsw i32 %68, 1
  %70 = sdiv i32 %69, %67
  %71 = mul nsw i32 %70, %67
  %72 = sub nsw i32 %69, %71
  %73 = icmp sgt i32 %72, %66
  br i1 %73, label %74, label %78

; <label>:74:                                     ; preds = %65
  %75 = add nsw i32 %70, 1
  %76 = mul nsw i32 %75, %66
  %77 = add nsw i32 %76, %10
  br label %82

; <label>:78:                                     ; preds = %65
  %79 = mul nsw i32 %70, %66
  %80 = add i32 %79, %10
  %81 = add i32 %80, %72
  br label %82

; <label>:82:                                     ; preds = %78, %74
  %83 = phi i32 [ %75, %74 ], [ %70, %78 ]
  %84 = phi i32 [ %77, %74 ], [ %81, %78 ]
  %85 = add i32 %83, -1
  %86 = add i32 %85, %84
  %87 = icmp eq i32 %86, %11
  br label %119

; <label>:88:                                     ; preds = %9
  %89 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %90 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !22
  %91 = mul nsw i32 %90, %89
  %92 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %93 = add nuw nsw i32 %91, %92
  %94 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #6, !range !27
  %95 = mul i32 %89, %8
  %96 = mul i32 %95, %94
  %97 = mul nsw i32 %93, %8
  %98 = add nsw i32 %97, %10
  %99 = add i32 %8, -1
  %100 = add i32 %99, %98
  %101 = srem i32 %11, %8
  %102 = sub i32 %11, %101
  %103 = sub i32 %102, %98
  %104 = srem i32 %103, %96
  %105 = icmp eq i32 %104, 0
  br label %119

; <label>:106:                                    ; preds = %9
  %107 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %108 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %109 = mul nsw i32 %108, %8
  %110 = mul nsw i32 %107, %8
  %111 = add nsw i32 %110, %10
  %112 = add i32 %8, -1
  %113 = add i32 %112, %111
  %114 = srem i32 %11, %8
  %115 = sub i32 %11, %114
  %116 = sub i32 %115, %111
  %117 = srem i32 %116, %109
  %118 = icmp eq i32 %117, 0
  br label %119

; <label>:119:                                    ; preds = %14, %44, %52, %82, %88, %106
  %120 = phi i1 [ %118, %106 ], [ %105, %88 ], [ %87, %82 ], [ %64, %52 ], [ %49, %44 ], [ %26, %14 ]
  %121 = phi i32 [ %111, %106 ], [ %98, %88 ], [ %84, %82 ], [ %57, %52 ], [ %46, %44 ], [ %19, %14 ]
  %122 = phi i32 [ %113, %106 ], [ %100, %88 ], [ %86, %82 ], [ %59, %52 ], [ %48, %44 ], [ %21, %14 ]
  %123 = phi i32 [ %109, %106 ], [ %96, %88 ], [ %69, %82 ], [ %55, %52 ], [ %31, %44 ], [ %17, %14 ]
  %124 = zext i1 %120 to i32
  store i32 %124, i32* %3, align 4, !tbaa !23
  store i32 %121, i32* %4, align 4, !tbaa !23
  store i32 %122, i32* %5, align 4, !tbaa !23
  store i32 %123, i32* %6, align 4, !tbaa !23
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #3

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_init_params(i8*) unnamed_addr #5 {
  store i8* %0, i8** addrspacecast (i8* addrspace(3)* @ReductionScratchpadPtr to i8**), align 8, !tbaa !28
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_init() unnamed_addr #5 {
  store i32 2, i32* addrspacecast (i32 addrspace(3)* @execution_param to i32*), align 4, !tbaa !23
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_deinit() unnamed_addr #5 {
  store volatile i8* null, i8** addrspacecast (i8* addrspace(3)* @omptarget_nvptx_workFn to i8**), align 8, !tbaa !28
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_spmd_kernel_init() unnamed_addr #5 {
  store i32 3, i32* addrspacecast (i32 addrspace(3)* @execution_param to i32*), align 4, !tbaa !23
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_parallel(i8** nocapture) unnamed_addr #5 {
  %2 = load volatile i8*, i8** addrspacecast (i8* addrspace(3)* @omptarget_nvptx_workFn to i8**), align 8, !tbaa !28
  store i8* %2, i8** %0, align 8, !tbaa !28
  ret void
}

attributes #0 = { argmemonly nounwind }
attributes #1 = { convergent nounwind }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_62" "target-features"="+ptx60,+satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_62" "target-features"="+ptx42,+satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { norecurse nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_62" "target-features"="+ptx42,+satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!omp_offload.info = !{!0, !1, !2}
!nvvm.annotations = !{!3, !4, !5, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8}
!llvm.module.flags = !{!10, !11}
!llvm.ident = !{!12, !12, !12, !12, !12, !12, !12, !12, !12, !12, !12}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13}

!0 = !{i32 0, i32 39, i32 45157962, !"kernel_gramschmidt", i32 175, i32 2}
!1 = !{i32 0, i32 39, i32 45157962, !"kernel_gramschmidt", i32 158, i32 0}
!2 = !{i32 0, i32 39, i32 45157962, !"kernel_gramschmidt", i32 167, i32 1}
!3 = !{void ([128 x double]*, i64, [128 x double]*, i8*)* @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l158, !"kernel", i32 1}
!4 = !{void ([128 x double]*, i64, [128 x double]*, [128 x double]*, i8*)* @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l167, !"kernel", i32 1}
!5 = !{void (i64, [128 x double]*, [128 x double]*, [128 x double]*, i8*)* @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l175, !"kernel", i32 1}
!6 = !{null, !"align", i32 8}
!7 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!8 = !{null, !"align", i32 16}
!9 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!10 = !{i32 1, !"PIC Level", i32 2}
!11 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!12 = !{!"clang version 4.0.0 (https://github.com/clang-ykt/clang 49d8020e03f898ea31212f6c565001e067f67d4f) (https://github.com/clang-ykt/llvm aa08e5a3c3670cd86fb4bee034a7626bb26ad57e)"}
!13 = !{i32 1, i32 2}
!14 = !{i32 1, i32 1025}
!15 = !{i32 0, i32 1024}
!16 = !{!17, !17, i64 0}
!17 = !{!"double", !18, i64 0}
!18 = !{!"omnipotent char", !19, i64 0}
!19 = !{!"Simple C/C++ TBAA"}
!20 = !{!21, !21, i64 0}
!21 = !{!"int", !18, i64 0}
!22 = !{i32 0, i32 2147483647}
!23 = !{!24, !24, i64 0}
!24 = !{!"int", !25, i64 0}
!25 = !{!"omnipotent char", !26, i64 0}
!26 = !{!"Simple C++ TBAA"}
!27 = !{i32 1, i32 -2147483648}
!28 = !{!29, !29, i64 0}
!29 = !{!"any pointer", !25, i64 0}

; __CLANG_OFFLOAD_BUNDLE____END__ openmp-nvptx64-nvidia-cuda

