
; __CLANG_OFFLOAD_BUNDLE____START__ openmp-nvptx64-nvidia-cuda
; ModuleID = '/home/kreilfla/nfs/prem/benchmark/Polybench/OpenMP-target/linear-algebra/solvers/gramschmidt/gramschmidt.c'
source_filename = "/home/kreilfla/nfs/prem/benchmark/Polybench/OpenMP-target/linear-algebra/solvers/gramschmidt/gramschmidt.c"
target datalayout = "e-i64:64-v16:16-v32:32-n16:32:64"
target triple = "nvptx64-nvidia-cuda"

%struct.__openmp_nvptx_target_property_ty = type { i8, i32, i32 }
%ident_t = type { i32, i32, i32, i32, i8* }
%struct.__openmp_nvptx_target_property_ty.0 = type { i8, i32, i32 }
%struct.__openmp_nvptx_target_property_ty.1 = type { i8, i32, i32 }

@__omp_offloading_27_2b10e4a_kernel_gramschmidt_l158_property = weak local_unnamed_addr constant %struct.__openmp_nvptx_target_property_ty { i8 1, i32 0, i32 0 }, align 1
@.str = private unnamed_addr constant [23 x i8] c";unknown;unknown;0;0;;\00", align 1
@0 = private unnamed_addr constant %ident_t { i32 0, i32 2050, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0) }, align 8
@__omp_offloading_27_2b10e4a_kernel_gramschmidt_l167_property = weak local_unnamed_addr constant %struct.__openmp_nvptx_target_property_ty.0 zeroinitializer, align 1
@__omp_offloading_27_2b10e4a_kernel_gramschmidt_l175_property = weak local_unnamed_addr constant %struct.__openmp_nvptx_target_property_ty.1 zeroinitializer, align 1
@execution_param = external local_unnamed_addr addrspace(3) global i32, align 4
@ReductionScratchpadPtr = external local_unnamed_addr addrspace(3) global i8*, align 8
@omptarget_nvptx_workFn = external addrspace(3) global i8*, align 8

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #0

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #0

; Function Attrs: convergent nounwind
declare void @llvm.nvvm.barrier0() #1

; Function Attrs: nounwind
define weak void @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l158(i64, [128 x double]*, i64, [128 x double]*, i8*) local_unnamed_addr #2 {
  %6 = alloca i8*, align 8
  %7 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %8 = add nsw i32 %7, -32
  tail call fastcc void @__kmpc_kernel_init() #6
  %9 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %10 = add nsw i32 %7, -1
  %11 = and i32 %10, -32
  %12 = icmp eq i32 %9, %11
  %13 = icmp ult i32 %9, %8
  br i1 %13, label %14, label %21

; <label>:14:                                     ; preds = %5
  %15 = bitcast i8** %6 to i8*
  call void @llvm.lifetime.start(i64 8, i8* nonnull %15) #6
  br label %16

; <label>:16:                                     ; preds = %19, %14
  tail call void @llvm.nvvm.barrier0() #6
  call fastcc void @__kmpc_kernel_parallel(i8** nonnull %6) #6
  %17 = load volatile i8*, i8** %6, align 8
  %18 = icmp eq i8* %17, null
  br i1 %18, label %20, label %19

; <label>:19:                                     ; preds = %16
  tail call void @llvm.nvvm.barrier0() #6
  br label %16

; <label>:20:                                     ; preds = %16
  call void @llvm.lifetime.end(i64 8, i8* nonnull %15) #6
  br label %21

; <label>:21:                                     ; preds = %20, %5
  br i1 %12, label %22, label %86

; <label>:22:                                     ; preds = %21
  %23 = trunc i64 %0 to i32
  %24 = icmp sgt i32 %23, 0
  %25 = shl i64 %2, 32
  %26 = ashr exact i64 %25, 32
  br i1 %24, label %27, label %54

; <label>:27:                                     ; preds = %22
  %28 = add i32 %23, -1
  %29 = and i32 %23, 3
  %30 = icmp eq i32 %29, 0
  br i1 %30, label %45, label %31

; <label>:31:                                     ; preds = %27
  br label %32

; <label>:32:                                     ; preds = %32, %31
  %33 = phi i32 [ %41, %32 ], [ 0, %31 ]
  %34 = phi double [ %40, %32 ], [ 0.000000e+00, %31 ]
  %35 = phi i32 [ %42, %32 ], [ %29, %31 ]
  %36 = sext i32 %33 to i64
  %37 = getelementptr inbounds [128 x double], [128 x double]* %1, i64 %36, i64 %26
  %38 = load double, double* %37, align 8, !tbaa !16
  %39 = fmul double %38, %38
  %40 = fadd double %34, %39
  %41 = add nuw nsw i32 %33, 1
  %42 = add i32 %35, -1
  %43 = icmp eq i32 %42, 0
  br i1 %43, label %44, label %32, !llvm.loop !20

; <label>:44:                                     ; preds = %32
  br label %45

; <label>:45:                                     ; preds = %27, %44
  %46 = phi double [ undef, %27 ], [ %40, %44 ]
  %47 = phi i32 [ 0, %27 ], [ %41, %44 ]
  %48 = phi double [ 0.000000e+00, %27 ], [ %40, %44 ]
  %49 = icmp ult i32 %28, 3
  br i1 %49, label %52, label %50

; <label>:50:                                     ; preds = %45
  br label %58

; <label>:51:                                     ; preds = %58
  br label %52

; <label>:52:                                     ; preds = %45, %51
  %53 = phi double [ %46, %45 ], [ %83, %51 ]
  br label %54

; <label>:54:                                     ; preds = %52, %22
  %55 = phi double [ 0.000000e+00, %22 ], [ %53, %52 ]
  %56 = tail call double @llvm.nvvm.sqrt.rn.d(double %55) #6
  %57 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 %26, i64 %26
  store double %56, double* %57, align 8, !tbaa !16
  tail call fastcc void @__kmpc_kernel_deinit() #6
  tail call void @llvm.nvvm.barrier0() #6
  br label %86

; <label>:58:                                     ; preds = %58, %50
  %59 = phi i32 [ %47, %50 ], [ %84, %58 ]
  %60 = phi double [ %48, %50 ], [ %83, %58 ]
  %61 = sext i32 %59 to i64
  %62 = getelementptr inbounds [128 x double], [128 x double]* %1, i64 %61, i64 %26
  %63 = load double, double* %62, align 8, !tbaa !16
  %64 = fmul double %63, %63
  %65 = fadd double %60, %64
  %66 = add nuw nsw i32 %59, 1
  %67 = sext i32 %66 to i64
  %68 = getelementptr inbounds [128 x double], [128 x double]* %1, i64 %67, i64 %26
  %69 = load double, double* %68, align 8, !tbaa !16
  %70 = fmul double %69, %69
  %71 = fadd double %65, %70
  %72 = add nsw i32 %59, 2
  %73 = sext i32 %72 to i64
  %74 = getelementptr inbounds [128 x double], [128 x double]* %1, i64 %73, i64 %26
  %75 = load double, double* %74, align 8, !tbaa !16
  %76 = fmul double %75, %75
  %77 = fadd double %71, %76
  %78 = add nsw i32 %59, 3
  %79 = sext i32 %78 to i64
  %80 = getelementptr inbounds [128 x double], [128 x double]* %1, i64 %79, i64 %26
  %81 = load double, double* %80, align 8, !tbaa !16
  %82 = fmul double %81, %81
  %83 = fadd double %77, %82
  %84 = add nsw i32 %59, 4
  %85 = icmp eq i32 %84, %23
  br i1 %85, label %51, label %58

; <label>:86:                                     ; preds = %21, %54
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #3

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #3

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.tid.x() #3

; Function Attrs: nounwind
define weak void @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l167(i64, [128 x double]*, i64, [128 x double]*, [128 x double]*, i8*) local_unnamed_addr #2 {
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  tail call fastcc void @__kmpc_kernel_init_params(i8* %5)
  tail call fastcc void @__kmpc_spmd_kernel_init() #6
  %11 = trunc i64 %0 to i32
  %12 = icmp sgt i32 %11, 0
  br i1 %12, label %18, label %13

; <label>:13:                                     ; preds = %6
  %14 = bitcast i32* %10 to i8*
  %15 = bitcast i32* %9 to i8*
  %16 = bitcast i32* %8 to i8*
  %17 = bitcast i32* %7 to i8*
  br label %52

; <label>:18:                                     ; preds = %6
  %19 = add nsw i32 %11, -1
  %20 = bitcast i32* %7 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %20) #6
  store i32 0, i32* %7, align 4, !tbaa !22
  %21 = bitcast i32* %8 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %21) #6
  store i32 %19, i32* %8, align 4, !tbaa !22
  %22 = bitcast i32* %9 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %22) #6
  store i32 1, i32* %9, align 4, !tbaa !22
  %23 = bitcast i32* %10 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %23) #6
  store i32 0, i32* %10, align 4, !tbaa !22
  %24 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !24
  %25 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %26 = add nsw i32 %25, -32
  %27 = mul i32 %26, %24
  %28 = add nuw nsw i32 %25, 1023
  %29 = and i32 %28, 992
  %30 = add nuw nsw i32 %29, 1023
  %31 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %32 = and i32 %30, %31
  %33 = add i32 %32, %27
  call void @__kmpc_for_static_init_4_simple_spmd(i8* nonnull bitcast (%ident_t* @0 to i8*), i32 %33, i32 93, i32* nonnull %10, i32* nonnull %7, i32* nonnull %8, i32* nonnull %9, i32 1, i32 1) #6
  %34 = load i32, i32* %7, align 4, !tbaa !22
  %35 = icmp slt i32 %34, %11
  br i1 %35, label %36, label %52

; <label>:36:                                     ; preds = %18
  %37 = shl i64 %2, 32
  %38 = ashr exact i64 %37, 32
  %39 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 %38, i64 %38
  %40 = load i32, i32* %9, align 4, !tbaa !22
  br label %41

; <label>:41:                                     ; preds = %41, %36
  %42 = phi i32 [ %34, %36 ], [ %49, %41 ]
  %43 = sext i32 %42 to i64
  %44 = getelementptr inbounds [128 x double], [128 x double]* %3, i64 %43, i64 %38
  %45 = load double, double* %44, align 8, !tbaa !16
  %46 = load double, double* %39, align 8, !tbaa !16
  %47 = fdiv double %45, %46
  %48 = getelementptr inbounds [128 x double], [128 x double]* %1, i64 %43, i64 %38
  store double %47, double* %48, align 8, !tbaa !16
  %49 = add nsw i32 %42, %40
  %50 = icmp slt i32 %49, %11
  br i1 %50, label %41, label %51

; <label>:51:                                     ; preds = %41
  store i32 %49, i32* %7, align 4, !tbaa !22
  br label %52

; <label>:52:                                     ; preds = %13, %18, %51
  %53 = phi i8* [ %17, %13 ], [ %20, %18 ], [ %20, %51 ]
  %54 = phi i8* [ %16, %13 ], [ %21, %18 ], [ %21, %51 ]
  %55 = phi i8* [ %15, %13 ], [ %22, %18 ], [ %22, %51 ]
  %56 = phi i8* [ %14, %13 ], [ %23, %18 ], [ %23, %51 ]
  call void @llvm.lifetime.end(i64 4, i8* nonnull %56) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %55) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %54) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %53) #6
  ret void
}

; Function Attrs: nounwind
define weak void @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l175(i64, i64, [128 x double]*, i64, [128 x double]*, [128 x double]*, i8*) local_unnamed_addr #2 {
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  tail call fastcc void @__kmpc_kernel_init_params(i8* %6)
  tail call fastcc void @__kmpc_spmd_kernel_init() #6
  %12 = trunc i64 %0 to i32
  %13 = trunc i64 %1 to i32
  %14 = trunc i64 %3 to i32
  %15 = add nsw i32 %12, -1
  %16 = sub nsw i32 %15, %13
  %17 = icmp sgt i32 %15, %13
  br i1 %17, label %23, label %18

; <label>:18:                                     ; preds = %7
  %19 = bitcast i32* %11 to i8*
  %20 = bitcast i32* %10 to i8*
  %21 = bitcast i32* %9 to i8*
  %22 = bitcast i32* %8 to i8*
  br label %172

; <label>:23:                                     ; preds = %7
  %24 = add nsw i32 %16, -1
  %25 = bitcast i32* %8 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %25) #6
  store i32 0, i32* %8, align 4, !tbaa !22
  %26 = bitcast i32* %9 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %26) #6
  store i32 %24, i32* %9, align 4, !tbaa !22
  %27 = bitcast i32* %10 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %27) #6
  store i32 1, i32* %10, align 4, !tbaa !22
  %28 = bitcast i32* %11 to i8*
  call void @llvm.lifetime.start(i64 4, i8* nonnull %28) #6
  store i32 0, i32* %11, align 4, !tbaa !22
  %29 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !24
  %30 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %31 = add nsw i32 %30, -32
  %32 = mul i32 %31, %29
  %33 = add nuw nsw i32 %30, 1023
  %34 = and i32 %33, 992
  %35 = add nuw nsw i32 %34, 1023
  %36 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %37 = and i32 %35, %36
  %38 = add i32 %37, %32
  call void @__kmpc_for_static_init_4_simple_spmd(i8* nonnull bitcast (%ident_t* @0 to i8*), i32 %38, i32 93, i32* nonnull %11, i32* nonnull %8, i32* nonnull %9, i32* nonnull %10, i32 1, i32 1) #6
  %39 = load i32, i32* %8, align 4, !tbaa !22
  %40 = icmp slt i32 %39, %16
  br i1 %40, label %41, label %172

; <label>:41:                                     ; preds = %23
  %42 = shl i64 %1, 32
  %43 = ashr exact i64 %42, 32
  %44 = icmp sgt i32 %14, 0
  %45 = load i32, i32* %10, align 4, !tbaa !22
  br i1 %44, label %47, label %46

; <label>:46:                                     ; preds = %41
  br label %160

; <label>:47:                                     ; preds = %41
  %48 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 0, i64 %43
  %49 = icmp eq i32 %14, 1
  br i1 %49, label %60, label %50

; <label>:50:                                     ; preds = %47
  %51 = add i32 %14, -1
  %52 = and i32 %14, 1
  %53 = icmp eq i32 %52, 0
  %54 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 0, i64 %43
  %55 = icmp eq i32 %51, 0
  %56 = and i32 %51, 1
  %57 = icmp eq i32 %56, 0
  %58 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 1, i64 %43
  %59 = icmp eq i32 %14, 2
  br label %77

; <label>:60:                                     ; preds = %47
  br label %61

; <label>:61:                                     ; preds = %60, %61
  %62 = phi i32 [ %75, %61 ], [ %39, %60 ]
  %63 = sub nsw i32 %15, %62
  %64 = sext i32 %63 to i64
  %65 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %43, i64 %64
  store double 0.000000e+00, double* %65, align 8, !tbaa !16
  %66 = load double, double* %48, align 8, !tbaa !16
  %67 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 0, i64 %64
  %68 = load double, double* %67, align 8, !tbaa !16
  %69 = fmul double %66, %68
  %70 = fadd double %69, 0.000000e+00
  store double %70, double* %65, align 8, !tbaa !16
  %71 = load double, double* %48, align 8, !tbaa !16
  %72 = fmul double %70, %71
  %73 = load double, double* %67, align 8, !tbaa !16
  %74 = fsub double %73, %72
  store double %74, double* %67, align 8, !tbaa !16
  %75 = add nsw i32 %62, %45
  %76 = icmp slt i32 %75, %16
  br i1 %76, label %61, label %167

; <label>:77:                                     ; preds = %50, %96
  %78 = phi i32 [ %97, %96 ], [ %39, %50 ]
  %79 = sub nsw i32 %15, %78
  %80 = sext i32 %79 to i64
  %81 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %43, i64 %80
  store double 0.000000e+00, double* %81, align 8, !tbaa !16
  br i1 %53, label %89, label %82

; <label>:82:                                     ; preds = %77
  br label %83

; <label>:83:                                     ; preds = %82
  %84 = load double, double* %54, align 8, !tbaa !16
  %85 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 0, i64 %80
  %86 = load double, double* %85, align 8, !tbaa !16
  %87 = fmul double %84, %86
  %88 = fadd double %87, 0.000000e+00
  store double %88, double* %81, align 8, !tbaa !16
  br label %89

; <label>:89:                                     ; preds = %77, %83
  %90 = phi double [ %88, %83 ], [ undef, %77 ]
  %91 = phi double [ %88, %83 ], [ 0.000000e+00, %77 ]
  %92 = phi i32 [ 1, %83 ], [ 0, %77 ]
  br label %93

; <label>:93:                                     ; preds = %89
  br i1 %55, label %141, label %94

; <label>:94:                                     ; preds = %93
  br label %120

; <label>:95:                                     ; preds = %99
  br label %96

; <label>:96:                                     ; preds = %158, %95
  %97 = add nsw i32 %78, %45
  %98 = icmp slt i32 %97, %16
  br i1 %98, label %77, label %168

; <label>:99:                                     ; preds = %99, %159
  %100 = phi i32 [ %157, %159 ], [ %118, %99 ]
  %101 = load double, double* %81, align 8, !tbaa !16
  %102 = sext i32 %100 to i64
  %103 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 %102, i64 %43
  %104 = load double, double* %103, align 8, !tbaa !16
  %105 = fmul double %101, %104
  %106 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 %102, i64 %80
  %107 = load double, double* %106, align 8, !tbaa !16
  %108 = fsub double %107, %105
  store double %108, double* %106, align 8, !tbaa !16
  %109 = add nuw nsw i32 %100, 1
  %110 = load double, double* %81, align 8, !tbaa !16
  %111 = sext i32 %109 to i64
  %112 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 %111, i64 %43
  %113 = load double, double* %112, align 8, !tbaa !16
  %114 = fmul double %110, %113
  %115 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 %111, i64 %80
  %116 = load double, double* %115, align 8, !tbaa !16
  %117 = fsub double %116, %114
  store double %117, double* %115, align 8, !tbaa !16
  %118 = add nsw i32 %100, 2
  %119 = icmp eq i32 %118, %14
  br i1 %119, label %95, label %99

; <label>:120:                                    ; preds = %120, %94
  %121 = phi double [ %91, %94 ], [ %137, %120 ]
  %122 = phi i32 [ %92, %94 ], [ %138, %120 ]
  %123 = sext i32 %122 to i64
  %124 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 %123, i64 %43
  %125 = load double, double* %124, align 8, !tbaa !16
  %126 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 %123, i64 %80
  %127 = load double, double* %126, align 8, !tbaa !16
  %128 = fmul double %125, %127
  %129 = fadd double %121, %128
  store double %129, double* %81, align 8, !tbaa !16
  %130 = add nuw nsw i32 %122, 1
  %131 = sext i32 %130 to i64
  %132 = getelementptr inbounds [128 x double], [128 x double]* %4, i64 %131, i64 %43
  %133 = load double, double* %132, align 8, !tbaa !16
  %134 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 %131, i64 %80
  %135 = load double, double* %134, align 8, !tbaa !16
  %136 = fmul double %133, %135
  %137 = fadd double %129, %136
  store double %137, double* %81, align 8, !tbaa !16
  %138 = add nsw i32 %122, 2
  %139 = icmp eq i32 %138, %14
  br i1 %139, label %140, label %120

; <label>:140:                                    ; preds = %120
  br label %141

; <label>:141:                                    ; preds = %93, %140
  %142 = phi double [ %90, %93 ], [ %137, %140 ]
  %143 = load double, double* %48, align 8, !tbaa !16
  %144 = fmul double %142, %143
  %145 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 0, i64 %80
  %146 = load double, double* %145, align 8, !tbaa !16
  %147 = fsub double %146, %144
  store double %147, double* %145, align 8, !tbaa !16
  br i1 %57, label %156, label %148

; <label>:148:                                    ; preds = %141
  br label %149

; <label>:149:                                    ; preds = %148
  %150 = load double, double* %81, align 8, !tbaa !16
  %151 = load double, double* %58, align 8, !tbaa !16
  %152 = fmul double %150, %151
  %153 = getelementptr inbounds [128 x double], [128 x double]* %5, i64 1, i64 %80
  %154 = load double, double* %153, align 8, !tbaa !16
  %155 = fsub double %154, %152
  store double %155, double* %153, align 8, !tbaa !16
  br label %156

; <label>:156:                                    ; preds = %141, %149
  %157 = phi i32 [ 2, %149 ], [ 1, %141 ]
  br label %158

; <label>:158:                                    ; preds = %156
  br i1 %59, label %96, label %159

; <label>:159:                                    ; preds = %158
  br label %99

; <label>:160:                                    ; preds = %46, %160
  %161 = phi i32 [ %165, %160 ], [ %39, %46 ]
  %162 = sub nsw i32 %15, %161
  %163 = sext i32 %162 to i64
  %164 = getelementptr inbounds [128 x double], [128 x double]* %2, i64 %43, i64 %163
  store double 0.000000e+00, double* %164, align 8, !tbaa !16
  %165 = add nsw i32 %161, %45
  %166 = icmp slt i32 %165, %16
  br i1 %166, label %160, label %169

; <label>:167:                                    ; preds = %61
  br label %170

; <label>:168:                                    ; preds = %96
  br label %170

; <label>:169:                                    ; preds = %160
  br label %170

; <label>:170:                                    ; preds = %169, %168, %167
  %171 = phi i32 [ %75, %167 ], [ %97, %168 ], [ %165, %169 ]
  store i32 %171, i32* %8, align 4, !tbaa !22
  br label %172

; <label>:172:                                    ; preds = %18, %23, %170
  %173 = phi i8* [ %22, %18 ], [ %25, %23 ], [ %25, %170 ]
  %174 = phi i8* [ %21, %18 ], [ %26, %23 ], [ %26, %170 ]
  %175 = phi i8* [ %20, %18 ], [ %27, %23 ], [ %27, %170 ]
  %176 = phi i8* [ %19, %18 ], [ %28, %23 ], [ %28, %170 ]
  call void @llvm.lifetime.end(i64 4, i8* nonnull %176) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %175) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %174) #6
  call void @llvm.lifetime.end(i64 4, i8* nonnull %173) #6
  ret void
}

; Function Attrs: nounwind readnone
declare double @llvm.nvvm.sqrt.rn.d(double) #3

; Function Attrs: nounwind
define internal void @__kmpc_for_static_init_4_simple_spmd(i8* nocapture readnone, i32, i32, i32* nocapture, i32* nocapture, i32* nocapture, i32* nocapture, i32, i32) unnamed_addr #4 {
  %10 = load i32, i32* %4, align 4, !tbaa !25
  %11 = load i32, i32* %5, align 4, !tbaa !25
  switch i32 %2, label %106 [
    i32 33, label %12
    i32 34, label %27
    i32 91, label %50
    i32 92, label %65
    i32 93, label %88
  ]

; <label>:12:                                     ; preds = %9
  %13 = icmp sgt i32 %8, 0
  br i1 %13, label %14, label %27

; <label>:14:                                     ; preds = %12
  %15 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %16 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %17 = mul nsw i32 %16, %8
  %18 = mul nsw i32 %15, %8
  %19 = add nsw i32 %18, %10
  %20 = add nsw i32 %8, -1
  %21 = add i32 %20, %19
  %22 = srem i32 %11, %8
  %23 = sub i32 %11, %22
  %24 = sub i32 %23, %19
  %25 = srem i32 %24, %17
  %26 = icmp eq i32 %25, 0
  br label %119

; <label>:27:                                     ; preds = %12, %9
  %28 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %29 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %30 = sub nsw i32 %11, %10
  %31 = add nsw i32 %30, 1
  %32 = sdiv i32 %31, %29
  %33 = mul nsw i32 %32, %29
  %34 = sub nsw i32 %31, %33
  %35 = icmp sgt i32 %34, %28
  br i1 %35, label %36, label %40

; <label>:36:                                     ; preds = %27
  %37 = add nsw i32 %32, 1
  %38 = mul nsw i32 %37, %28
  %39 = add nsw i32 %38, %10
  br label %44

; <label>:40:                                     ; preds = %27
  %41 = mul nsw i32 %32, %28
  %42 = add i32 %41, %10
  %43 = add i32 %42, %34
  br label %44

; <label>:44:                                     ; preds = %40, %36
  %45 = phi i32 [ %37, %36 ], [ %32, %40 ]
  %46 = phi i32 [ %39, %36 ], [ %43, %40 ]
  %47 = add i32 %45, -1
  %48 = add i32 %47, %46
  %49 = icmp eq i32 %48, %11
  br label %119

; <label>:50:                                     ; preds = %9
  %51 = icmp sgt i32 %8, 0
  br i1 %51, label %52, label %65

; <label>:52:                                     ; preds = %50
  %53 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !24
  %54 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #6, !range !29
  %55 = mul nsw i32 %54, %8
  %56 = mul nsw i32 %53, %8
  %57 = add nsw i32 %56, %10
  %58 = add nsw i32 %8, -1
  %59 = add i32 %58, %57
  %60 = srem i32 %11, %8
  %61 = sub i32 %11, %60
  %62 = sub i32 %61, %57
  %63 = srem i32 %62, %55
  %64 = icmp eq i32 %63, 0
  br label %119

; <label>:65:                                     ; preds = %50, %9
  %66 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !24
  %67 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #6, !range !29
  %68 = sub nsw i32 %11, %10
  %69 = add nsw i32 %68, 1
  %70 = sdiv i32 %69, %67
  %71 = mul nsw i32 %70, %67
  %72 = sub nsw i32 %69, %71
  %73 = icmp sgt i32 %72, %66
  br i1 %73, label %74, label %78

; <label>:74:                                     ; preds = %65
  %75 = add nsw i32 %70, 1
  %76 = mul nsw i32 %75, %66
  %77 = add nsw i32 %76, %10
  br label %82

; <label>:78:                                     ; preds = %65
  %79 = mul nsw i32 %70, %66
  %80 = add i32 %79, %10
  %81 = add i32 %80, %72
  br label %82

; <label>:82:                                     ; preds = %78, %74
  %83 = phi i32 [ %75, %74 ], [ %70, %78 ]
  %84 = phi i32 [ %77, %74 ], [ %81, %78 ]
  %85 = add i32 %83, -1
  %86 = add i32 %85, %84
  %87 = icmp eq i32 %86, %11
  br label %119

; <label>:88:                                     ; preds = %9
  %89 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %90 = tail call i32 @llvm.nvvm.read.ptx.sreg.ctaid.x() #6, !range !24
  %91 = mul nsw i32 %90, %89
  %92 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %93 = add nuw nsw i32 %91, %92
  %94 = tail call i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #6, !range !29
  %95 = mul i32 %89, %8
  %96 = mul i32 %95, %94
  %97 = mul nsw i32 %93, %8
  %98 = add nsw i32 %97, %10
  %99 = add i32 %8, -1
  %100 = add i32 %99, %98
  %101 = srem i32 %11, %8
  %102 = sub i32 %11, %101
  %103 = sub i32 %102, %98
  %104 = srem i32 %103, %96
  %105 = icmp eq i32 %104, 0
  br label %119

; <label>:106:                                    ; preds = %9
  %107 = tail call i32 @llvm.nvvm.read.ptx.sreg.tid.x() #6, !range !15
  %108 = tail call i32 @llvm.nvvm.read.ptx.sreg.ntid.x() #6, !range !14
  %109 = mul nsw i32 %108, %8
  %110 = mul nsw i32 %107, %8
  %111 = add nsw i32 %110, %10
  %112 = add i32 %8, -1
  %113 = add i32 %112, %111
  %114 = srem i32 %11, %8
  %115 = sub i32 %11, %114
  %116 = sub i32 %115, %111
  %117 = srem i32 %116, %109
  %118 = icmp eq i32 %117, 0
  br label %119

; <label>:119:                                    ; preds = %14, %44, %52, %82, %88, %106
  %120 = phi i1 [ %118, %106 ], [ %105, %88 ], [ %87, %82 ], [ %64, %52 ], [ %49, %44 ], [ %26, %14 ]
  %121 = phi i32 [ %111, %106 ], [ %98, %88 ], [ %84, %82 ], [ %57, %52 ], [ %46, %44 ], [ %19, %14 ]
  %122 = phi i32 [ %113, %106 ], [ %100, %88 ], [ %86, %82 ], [ %59, %52 ], [ %48, %44 ], [ %21, %14 ]
  %123 = phi i32 [ %109, %106 ], [ %96, %88 ], [ %69, %82 ], [ %55, %52 ], [ %31, %44 ], [ %17, %14 ]
  %124 = zext i1 %120 to i32
  store i32 %124, i32* %3, align 4, !tbaa !25
  store i32 %121, i32* %4, align 4, !tbaa !25
  store i32 %122, i32* %5, align 4, !tbaa !25
  store i32 %123, i32* %6, align 4, !tbaa !25
  ret void
}

; Function Attrs: nounwind readnone
declare i32 @llvm.nvvm.read.ptx.sreg.nctaid.x() #3

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_init_params(i8*) unnamed_addr #5 {
  store i8* %0, i8** addrspacecast (i8* addrspace(3)* @ReductionScratchpadPtr to i8**), align 8, !tbaa !30
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_init() unnamed_addr #5 {
  store i32 2, i32* addrspacecast (i32 addrspace(3)* @execution_param to i32*), align 4, !tbaa !25
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_deinit() unnamed_addr #5 {
  store volatile i8* null, i8** addrspacecast (i8* addrspace(3)* @omptarget_nvptx_workFn to i8**), align 8, !tbaa !30
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_spmd_kernel_init() unnamed_addr #5 {
  store i32 3, i32* addrspacecast (i32 addrspace(3)* @execution_param to i32*), align 4, !tbaa !25
  ret void
}

; Function Attrs: norecurse nounwind
define internal fastcc void @__kmpc_kernel_parallel(i8** nocapture) unnamed_addr #5 {
  %2 = load volatile i8*, i8** addrspacecast (i8* addrspace(3)* @omptarget_nvptx_workFn to i8**), align 8, !tbaa !30
  store i8* %2, i8** %0, align 8, !tbaa !30
  ret void
}

attributes #0 = { argmemonly nounwind }
attributes #1 = { convergent nounwind }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_62" "target-features"="+ptx60,+satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_62" "target-features"="+ptx42,+satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { norecurse nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="sm_62" "target-features"="+ptx42,+satom" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!omp_offload.info = !{!0, !1, !2}
!nvvm.annotations = !{!3, !4, !5, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8, !6, !7, !6, !8, !8, !8, !8, !9, !9, !8}
!llvm.module.flags = !{!10, !11}
!llvm.ident = !{!12, !12, !12, !12, !12, !12, !12, !12, !12, !12, !12}
!nvvm.internalize.after.link = !{}
!nvvmir.version = !{!13, !13, !13, !13, !13, !13, !13, !13, !13, !13, !13}

!0 = !{i32 0, i32 39, i32 45157962, !"kernel_gramschmidt", i32 175, i32 2}
!1 = !{i32 0, i32 39, i32 45157962, !"kernel_gramschmidt", i32 158, i32 0}
!2 = !{i32 0, i32 39, i32 45157962, !"kernel_gramschmidt", i32 167, i32 1}
!3 = !{void (i64, [128 x double]*, i64, [128 x double]*, i8*)* @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l158, !"kernel", i32 1}
!4 = !{void (i64, [128 x double]*, i64, [128 x double]*, [128 x double]*, i8*)* @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l167, !"kernel", i32 1}
!5 = !{void (i64, i64, [128 x double]*, i64, [128 x double]*, [128 x double]*, i8*)* @__omp_offloading_27_2b10e4a_kernel_gramschmidt_l175, !"kernel", i32 1}
!6 = !{null, !"align", i32 8}
!7 = !{null, !"align", i32 8, !"align", i32 65544, !"align", i32 131080}
!8 = !{null, !"align", i32 16}
!9 = !{null, !"align", i32 16, !"align", i32 65552, !"align", i32 131088}
!10 = !{i32 1, !"PIC Level", i32 2}
!11 = !{i32 4, !"nvvm-reflect-ftz", i32 0}
!12 = !{!"clang version 4.0.0 (https://github.com/clang-ykt/clang 49d8020e03f898ea31212f6c565001e067f67d4f) (https://github.com/clang-ykt/llvm aa08e5a3c3670cd86fb4bee034a7626bb26ad57e)"}
!13 = !{i32 1, i32 2}
!14 = !{i32 1, i32 1025}
!15 = !{i32 0, i32 1024}
!16 = !{!17, !17, i64 0}
!17 = !{!"double", !18, i64 0}
!18 = !{!"omnipotent char", !19, i64 0}
!19 = !{!"Simple C/C++ TBAA"}
!20 = distinct !{!20, !21}
!21 = !{!"llvm.loop.unroll.disable"}
!22 = !{!23, !23, i64 0}
!23 = !{!"int", !18, i64 0}
!24 = !{i32 0, i32 2147483647}
!25 = !{!26, !26, i64 0}
!26 = !{!"int", !27, i64 0}
!27 = !{!"omnipotent char", !28, i64 0}
!28 = !{!"Simple C++ TBAA"}
!29 = !{i32 1, i32 -2147483648}
!30 = !{!31, !31, i64 0}
!31 = !{!"any pointer", !27, i64 0}

; __CLANG_OFFLOAD_BUNDLE____END__ openmp-nvptx64-nvidia-cuda

