\section{Methodology}
\subsection{General}
As a starting point, Polybench-C (\cite{polyc}) and Polybench-ACC (\cite{polyacc} adapted version ETHZ: \cite{polyeth}) suites were used. Then Polybench-ACC was adapted to support OpenMP offloading. To have a more accurate comparison between OpenMP offloading and the CUDA implementation, both test suits were adapted to have similar thread distribution on the GPU by tuning the grid and block parameters in the CUDA implementation and the num\_teams and num\_threads parameters in the OpenMP target implementation. To achieve this goal, the NVIDIA Visual Profiler and its guided analysis tool were used. To enable the OpenMP offloading, clang-ykt (\cite{clang-ykt}) was used. This is a
LLVM Clang version which supports OpenMP offloading to GPU's by using the CUDA driver API.
Besides the CUDA and OpenMP offloading implementation, the original sequential c implementation and the OpenMP parallelization on the CPU implementation were measured. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Environment}
There different benchmarks with changing environment parameters have been executed. The following parameters remained the same for the described results in this document:
\begin{itemize}
\item NVIDIA Power mode MAXN with clocks on maximum
\item Thread distribution between OpenMP offloading and CUDA is as similar as possible
\item Compiler optimization -O3
\item Kernel only benchmarking --> No memcopy to the device and context creation is measured
\item OpenMP target schedule(static,1) for coalescent memory access
\item Force openMP parallelization as much as possible
\end{itemize}

\subsection{Polybench Kernels}
The following table \ref{tab:algdat} is adapted from the document \textit{Polybench 4.0} \cite{polydes} and describes which algorithms of the Polybench suite were used for this experiment. The data sizes were adapted from the Polybench-ACC suite and were extended with the data sets EX1 and EX2 to have a better comparison between the CUDA and OpenMP offloading implementations.

\begin{table}[H]
\centering
%\normalsize
\begin{tabular}{l|l|l|l|l|l|l|l|l|l}
\textbf{Name} & \textbf{Operations} & \textbf{Memory} & \textbf{MINI} & \textbf{SMALL} & \textbf{STANDARD} & \textbf{LARGE} & \textbf{EX.LARGE} & \textbf{EX1} & \textbf{EX2} \\\hline
GEMM          & $3n^3+n^2$          & $3n^2$          & 64x64         & 128x128        & 256x256           & 512x512        & 1024x1024           & 2096x2096    & 4192x4192    \\
ATAX          & $4n^2$              & $n^2+3n$        & 16x16x16      & 2048x2048      & 4096x4096         & 8192x8192      & 16384x16384         & -            & -            \\
Doitgen       & $2n^4$              & $n^3+n^2+n$     & 16x16x16      & 32x32x32       & 64x64x64          & 128x128x128    & 256x256x256         & 512x512x512  & -            \\
Gramschmidt   & $2n^3+n^2+n$        & $3n^2$          & 64x64         & 128x128        & 256x256           & 512x512        & 1024x1024           & 2096x2096    & 4192x4192    \\
Correlation   & $n^3+8n^2+3n$       & $2n^2+2n$       & 64x64         & 128x128        & 256x256           & 512x512        & 1024x1024           & 2096x2096    & 4192x4192   
\end{tabular}
\caption {Algorithms and Data sets}
 \label{tab:algdat}
\end{table}

\subsection{OpenMP target example for correlation}
The following code shows how the Polybench kernels are offloaded to the GPU using OpenMP target. It can be seen that the kernels were embedded into a \textit{target data} - region to avoid memcopy operations from the host to the device and vice versa. Data which is only needed on the target is directly allocated on the device (mean and stddev). All kernels within this data region can access the memory. Each single kernel has its own thread distribution defined by \textit{num\_teams} and \textit{num\_threads} and its schedule is set to static with a chunk size of 1. If possible, more parallelization is achieved by using the \textit{collapse} clause, which collapses to perfectly nested for loops. The second kernel which calculated the standard deviation, uses an unfortunate branching at line 46 to avoid division through zero. This can lead to an inefficient execution on the GPU since all executed threads in a block should take the same branches within their execution otherwise additional synchronization is needed..\newpage
\begin{lstlisting}
/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static void kernel_correlation(int m, int n,
                               DATA_TYPE float_n,
                               DATA_TYPE POLYBENCH_2D(data,M,N,m,n),
                               DATA_TYPE POLYBENCH_2D(symmat,M,M,m,m),
                               DATA_TYPE POLYBENCH_1D(mean,M,m),
                               DATA_TYPE POLYBENCH_1D(stddev,M,m))
{


#pragma omp target data map(to: data[0:M]) map(tofrom: symmat[0:M]) map(alloc:mean[0:M], stddev[0:M])
    {
#ifdef OMP_MEAS_KERNEL
        /* Start timer. */
        polybench_start_instruments;
#endif
        /* Determine mean of column vectors of input data matrix */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_teams((size_t)(M + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS)
        for (int j = 0; j < M; j++)
        {
            mean[j] = 0.0;
            for (int i = 0; i < N; i++)
                mean[j] += data[i][j];
            mean[j] /= (DATA_TYPE)FLOAT_N;
        }

        /* Determine standard deviations of column vectors of data matrix. */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_teams((size_t)(M + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS)
        for (int j = 0; j < M; j++)
        {
            stddev[j] = 0.0;
            for (int i = 0; i < N; i++)
                stddev[j] += (data[i][j] - mean[j]) * (data[i][j] - mean[j]);
            stddev[j] /= FLOAT_N;
            stddev[j] = sqrt_of_array_cell(stddev, j);
            /* The following in an inelegant but usual way to handle
         near-zero std. dev. values, which below would cause a zero-
         divide. */
            stddev[j] = stddev[j] <= EPS ? 1.0 : stddev[j];
        }

        /* Center and reduce the column vectors. */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    collapse(2) \
    num_teams((size_t)(M*N + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS)
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                data[i][j] -= mean[j];
                data[i][j] /= sqrt(FLOAT_N) * stddev[j];
            }
        }

        /* Calculate the m * m correlation matrix. */
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_teams((size_t)(M + NOF_THREADS - 1)/NOF_THREADS) \
    num_threads(NOF_THREADS)
        for (int j1 = 0; j1 < M-1; j1++)
        {
            symmat[j1][j1] = 1.0;
            for (int j2 = j1+1; j2 < M; j2++)
            {
                symmat[j1][j2] = 0.0;
                for (int i = 0; i < N; i++)
                    symmat[j1][j2] += (data[i][j1] * data[i][j2]);
                symmat[j2][j1] = symmat[j1][j2];            // XXX PROBLEM: LD-deps!
            }
        }
#ifdef OMP_MEAS_KERNEL
        /* Stop and print timer. */
        polybench_stop_instruments;
        polybench_print_instruments;
#endif
    }
    symmat[_PB_M-1][_PB_M-1] = 1.0;
}
\end{lstlisting}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Measurements}
The time performance was measured using following Polybench function calls which utilize \textit{gettimeofday}:
\begin{lstlisting}
...
polybench_start_instruments;

gemm(ni, nj, nk, alpha, beta, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(B), POLYBENCH_ARRAY(C_cpu));

/* Stop and print timer. */
polybench_stop_instruments;
polybench_print_instruments;
...
\end{lstlisting}

The execution time of the different implementation was measured using the default time stamping functionality of Polybench which utilizes the gettimeofday function. To just measure the execution times of the kernels and not in combination with the memcopy operations to the device, the CUDA execution time is measured around the kernel launch and the OpenMP offloading execution times were captured withing the \textit{\#pragma omp target data} block. The benchmark itself is controlled from a central script which compiles the binaries for the different data sizes and then runs each binary 10 times to collect the execution times of the runs. Each run of a binary outputs the time of one execution of the compiled algorithm. Due to the experience that this approach results in quiet big variance. There fore further benchmarking was performed measuring each kernel 10 times per binary execution which lead to better results. It is noted in the results if a benchmark was executed either way.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Aggregation of results}
Two variants for the result aggregation were used and both depend on the T-distribution to evaluate the confidence interval of the mean value.
\subsubsection{Variant 1 - Kernel execution once per run}
The aggregation of the results was performed simply by getting the mean execution time of all 10 runs per algorithm and data size. The confidence interval of 95\% of the averages was calculated by using the CONFIDENCE.T function of LibreOffice Calc which uses the T-distribution. The averaged execution times of the different execution times are visualized by a graph. One will get more accurate and representative results, if each kernel is run multiple times within one execution of the binary due to the system warm up and collection of more data. But it is assumed that current method gives enough information for a brief and fast comparison.

\subsubsection{Variant 2 - Kernel execution 10 times per run}
The tested program repeats ten times the implemented kernel calculation. The execution time of every single kernel is measured and printed to stdout. This flow is repeated 10 times to have 100 samples of the measured execution time. This procedure is repeated for all test cases. 
\newline
Then the average performance and the corresponding 95\% confidence interval are calculated according the equation \ref{eq:meanconf}: $\bar{Y}$ is the average performance over all measurements, $\bar{Y_jn}$ the average performance of each of the ten runs and $t_{1-\frac{\alpha}{2}}$ the t-quantile of the t-distribution.
\begin{equation}
\bar{Y}\pm t_{1-\frac{\alpha}{2}},v\sqrt{\frac{S_{n}^2}{r_n}}=\pm t_{1-\frac{\alpha}{2}},v\sqrt{\frac{1}{r_n(r_n-1)}\sum_{j_n=1}^{r_n}\left(\bar{Y_{j_n}}-\bar{Y}\right)^2}
\label{eq:meanconf}
 \cite{kar33611}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Comparison}
After the average performance is known, the performance comparison is performed between c, omp, omp offloading and cuda imlementations of the kernels. This is done using equation \ref{eq:percomp}:
\begin{equation}
\frac{\bar{Y'}}{\bar{Y}} = \frac{\bar{Y}\bar{Y'}\pm\sqrt{\left(\bar{Y}\bar{Y'}\right)-\left(\bar{Y}^2-h^2\right)\left(\bar{Y'}^2-h'^2\right)}}{\bar{Y}^2-h^2}
\label{eq:percomp}
 \cite{kar33611}
\end{equation}
where
\begin{equation}
h = t_{1-\frac{\alpha}{2}},v\sqrt{\frac{S_{n}^2}{r_n}}
\label{eq:meanconf}
 \cite{kar33611}
\end{equation}
\newline
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Machine specification - Jetson TX2}
\begin{itemize}
	\conciseItem
	\item GPU: NVIDIA Pascal, 256 CUDA cores
	\item CPU : HMP Dual Denver 2/2 MB L2 + Quad ARM A57/2 MB L2
	\item Memory: 8 GB 128 bit LPDDR4 - 59.7 GB/s
	\item JetPack: 3.2.1 for CUDA-9.0 and JetPack 3.1 for CUDA-8.0
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%