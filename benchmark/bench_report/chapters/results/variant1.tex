\subsection{Benchmarks variant 1}
The full benchmark results of type variant 1 are in more detail documented in the document: \textit{Benchmarking with Polybench on NVIDIA Jetson TX2} \cite{repfk}. Here, a short summary is provided.
\subsubsection{General comparison with CUDA-8.0}\noindent\newline
To see after which data set size the overhead to offload computations to the GPU is negligible due to the performance improvements with the additional parallelization, four different versions of the Polybench suite were tested: Sequential c , OpenMP parallelization on CPU, OpenMP offloading to GPU and the CUDA implementation.
The device clocks were all set to their maximum value since the normal mode influenced the results in an undesired way due to changing core frequencies. Figures \ref{pic:c8gemm}, \ref{pic:c8doitgen}, \ref{pic:c8ataxbig}, \ref{pic:c8atax}, \ref{pic:c8gram}, \ref{pic:c8cor} are presenting a brief summary of the results.
\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1general/gemm.png}                                                        
        \caption{GEMM: CUDA-8.0 general comparison}                                   
        \label{pic:c8gemm}   
\end{minipage}%
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1general/doitgen.png}                                                        
        \caption{Doitgen: CUDA-8.0 general comparison}                                   
        \label{pic:c8doitgen}            
\end{minipage}
\end{figure}

\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1general/atax_biggerblock.png}                                                        
        \caption{ATAX: CUDA-8.0 general comparison, \\256 threads per block}                                   
        \label{pic:c8ataxbig}   
\end{minipage}%
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1general/atax.png}                                                        
        \caption{ATAX: CUDA-8.0 general comparison, \\16 threads per block}                                   
        \label{pic:c8atax}            
\end{minipage}
\end{figure}

\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1general/gramschmidt.png}                                                        
        \caption{Gramschmidt: CUDA-8.0 general comparison}                                   
        \label{pic:c8gram}   
\end{minipage}%
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1general/correlation.png}                                                        
        \caption{Correlation: CUDA-8.0 general comparison}                                   
        \label{pic:c8cor}            
\end{minipage}
\end{figure}

It can be seen, that in general, the offloaded implementations are faster than the CPU implementations. Especially if the data sets are getting bigger. A special case is the ATAX algorithm, where the performance difference is smaller. In a previous run the thread-block size was set to 256 threads which lead to a much slower performance on the GPU than on the CPU (See figure \ref{pic:c8ataxbig}, which shows the performance for CUDA with thread count 256). Using the NVIDIA visual profiler it has shown, that this lead to a too small number of team which is not able to hide the memory latency. Therefore the block size was reduced to 16 threads per block for the OpenMP offload and CUDA implementation which leads to the more competing result shown in figure \ref{pic:c8atax}. But still it can be seen, that the parallelization does not add a big improvement. A similar change was applied for the correlation algorithm which lead also to a significant improvement.
The comparison for smaller data sizes is a bit more difficult and uncertain, since the execution times had to be captured at different positions due to different implementations of the kernel launches.

\begin{itemize}
\item CUDA: Around kernel launches
\item OpenMP offloading: within \textit{\#pragma omp data map clause}
\item OpenMP CPU: Around kernel launches
\item C: Around kernel launches
\end{itemize}
Especially for the OpenMP implementation it is difficult to tell which overhead (memcopies etc.) is still captured or missed due to the position of the time stamping functionality. The Intermediate Representation of LLVM was checked and it seems that the current position of the instrumentation should have the desired effects. \newline

\subsubsection{Comparison OpenMP offloading and CUDA with CUDA-Toolkit 8.0}\noindent\newline
To see if the CUDA and OpenMP offloading implementation perform similar if bigger data sets are applied, two new data set sizes were introduced and used. Figures \ref{pic:c8lgemm}, \ref{pic:c8latax}, \ref{pic:c8ldoit}, \ref{pic:c8lgram}, \ref{pic:c8lcorr} summarize the performance differences between the two implementations for bigger data sizes. As in the previous experiments, the results are more expressive for bigger data sets. It can be seen, that the performance difference then remains about the same level. For the most algorithms the CUDA implementation had a better performance. This is most probably due to the fact, that the nvcc compiler has better possibilities to optimize the kernels for the GPU. For ATAX and correlation algorithms, performance is the same or OpenMP offloading performes even better than the CUDA implementation. As discussed in the previous section, those two algorithms had problems to hide memory and other latencies due to a too small grid size. It is probable that openMP had better possibilities to hide this problem, since the kernels could use more sophisticated optimization methods for two collapsed for loops (\#pragma omp parallel for collapse(2)).
\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1large/gemm.png}                                                        
        \caption{GEMM: CUDA-8.0 comparison with\\ large data sets}                                   
        \label{pic:c8lgemm}   
\end{minipage}%
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1large/atax.png}                                                        
        \caption{ATAX: CUDA-8.0 comparison with\\ large data sets}                                   
        \label{pic:c8latax}            
\end{minipage}
\end{figure}
\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1large/doitgen.png}                                                        
        \caption{Doitgen: CUDA-8.0 comparison with\\ large data sets}                                   
        \label{pic:c8ldoit}   
\end{minipage}%
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1large/gramschmidt.png}                                                        
        \caption{Gramschidt: CUDA-8.0 comparison with\\ large data sets}                                   
        \label{pic:c8lgram}            
\end{minipage}
\end{figure}
\begin{figure}[H]                                                               
        \centering                                                              
        \includegraphics[angle=0,width=0.5\textwidth]                           
        {fig/var1large/correlation.png}                                                        
        \caption{Correlation: CUDA-8.0 comparison with\\ large data sets}                                   
        \label{pic:c8lcorr}                                                  
\end{figure} 
\newpage
\subsubsection{Comparison offloading CUDA-8.0 vs. CUDA-9.0}\noindent\newline
\label{ssec:comp89}
To see if the performance remains the same between the CUDA version 8.0 and 9.0, the benchmarks were executed on another TX2 with Jetpack 3.2.1 and CUDA 9.0 installed. Again the comparison between the implementations is uncertain for smaller data sets but for the bigger data sets some differences could be observed (Fig. \ref {pic:c89gemm}, \ref{pic:c89atax}, \ref{pic:c89doit}, \ref{pic:c89gram}, \ref{pic:c89corr}. In general can be said, that the CUDA implementation has experienced a slight performance boost with CUDA 9.0 and the Open)MP offloading implementation stayed the same or had a decrease in performance. It could not yet completely reviewed why OpenMP offloading and the CUDA implementation have different changes. Interesting is, that the ATAX and Correlation algorithms experienced the smallest slow down for the OpenMP offloading implementation. For those two algorithms the block size was decreased from 256 threads to 16 threads to hide some memory latencies with more congruently running teams. Therefore a first idea was, that the change in performance could be due to a changed memory management CUDA internally, which is hidden for those two implementation due to the bigger number of teams. But this point needs further investigation and could not be confirmed yet. There were some problems with th NVIDIA visual profiler and Ubuntu 17.10 which delayed this analysis.
\begin{table}[H]
\centering
\begin{tabular}{lrrrrr}
\hline
               &   MINI\_DATASET &   SMALL\_DATASET &   STANDARD\_DATASET &   LARGE\_DATASET &   EXTRALARGE\_DATASET \\
\hline
 Ratio [\%]     &       173.381  &        164.755  &          132.707   &       125.848   &            94.5626   \\
 Conf. 95\% [\%] &        33.9613 &         25.1664 &            5.65661 &         1.26434 &             0.470339 \\
\hline
\end{tabular}
\caption {GEMM: Speed up OpenMP offloading / CUDA }
 \label{tab:rompcuda}
\end{table}
\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1cu89/gemm.png}                                                        
        \caption{GEMM: CUDA 8.0 vs CUDA 9.0}                                   
        \label{pic:c89gemm}   
\end{minipage}%
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1cu89/atax.png}                                                        
        \caption{ATAX: CUDA 8.0 vs CUDA 9.0}                                   
        \label{pic:c89atax}            
\end{minipage}
\end{figure}
\begin{figure}[H]
\centering
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1cu89/doitgen.png}                                                        
        \caption{Doitgen: CUDA 8.0 vs CUDA 9.0}                                   
        \label{pic:c89doit}   
\end{minipage}%
\begin{minipage}{.5\textwidth}
        \centering                                                              
        \includegraphics[angle=0,width=1.0\textwidth]                           
        {fig/var1cu89/gramschmidt.png}                                                        
        \caption{Gramschidt: CUDA 8.0 vs CUDA 9.0}                                   
        \label{pic:c89gram}            
\end{minipage}
\end{figure}
\begin{figure}[H]                                                               
        \centering                                                              
        \includegraphics[angle=0,width=0.5\textwidth]                           
        {fig/var1cu89/correlation.png}                                                        
        \caption{Correlation: CUDA 8.0 vs CUDA 9.0}                                   
        \label{pic:c89corr}                                                  
\end{figure} 

\subsection{Option -maxregcount= and its problems}
The comparison between CUDA-8.0 and CUDA-9.0 shown in section \ref{ssec:comp89} revealed different changes in performance for the CUDA version and the OpenMP offloading versions of Polybench. This issue was further analyzed using the Visual profiler provided by NVIDIA. For all algorithm's the CUDA-8.0 and CUDA-9.0 versions were profiled and analyzed. This has shown that NVIDIA has applied some changes to the memory management for CUDA-9.0. To reduce the utilization of the L2 cache each thread uses more registers to store local data. Since for the OpenMP offloading the \textit{--maxregcount} was set to 64 registers per thread it was possible for the clang compiler to allocate more than 32 registers per thread. In fact it has allocated up to 44 registers per thread. This leads to a decreased number of blocks per SM since only 65536 registers \cite{anderson1} can be used per SM this lead to 5 blocks with 256 threads using 44 registers each. This leads to only 1280 threads per SM although an SM could execute up to 2048 threads in parallel \cite{anderson1}. This results in a low occupancy for the OpenMP offloading with CUDA-9.0. The nvcc compiler on the other hand did never allocate more than 32 registers per thread and therefore could improve the performance since more registers were used compared to CUDA-8.0 but not enough to influence the number of blocks per SM. Therefore it is recommended to define the  \textit{--maxregcount} for the OpenMP offloading.