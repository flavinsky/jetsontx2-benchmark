#!/bin/bash    
## \defgroup Project_Setup Benchmark
## @{
## \file setUpEnv.sh
## \brief Installting all required Packages and Tools
##
## You can execute this script with the Top-Level Makefile 'make init'
#################################################################
 
CURR_LOC=$PWD
 
POLYBENCH_SRC_PATH=$CURR_LOC/polybench
 
#################################################################
 
## \fn installer(string package_name)
## \brief Install a Package with the Linux apt tool
## \param package_name The name of the package you would like to install

function download {
    echo "-> Download $2"
    wget -nv -P $1 $2
}

function createDir {
    echo "-> Make Dir $1"
    mkdir $1
}
 
function cleanup {
    echo "-> Clean Dir $1"
    rm -fr $1/
}
 
 
#################################################################
echo "##########################################################"
echo "             Setting up Evironment"
echo "##########################################################"
echo ""
 
#################################################################
 
case $1 in
        download)
                echo "--> Downloading polybench"
 
                cleanup $POLYBENCH_SRC_PATH
                createDir $POLYBENCH_SRC_PATH
               
                download $POLYBENCH_SRC_PATH http://web.cs.ucla.edu/~pouchet/software/polybench/download/polybench-c-3.2.tar.gz
               
                download $POLYBENCH_SRC_PATH https://iis-git.ee.ethz.ch/bjoernf/PolyBench-ACC/-/archive/master/PolyBench-ACC-master.tar.gz
                ;;

        install)
                echo "Unpacking PolyBench-ACC"
                tar -xf $POLYBENCH_SRC_PATH/PolyBench-ACC-master.tar.gz -C $POLYBENCH_SRC_PATH
                echo "Unpacking PolyBench-C"
                tar -xf $POLYBENCH_SRC_PATH/polybench-c-3.2.tar.gz -C $POLYBENCH_SRC_PATH
                ;;

        clean)
                cleanup $POLYBENCH_SRC_PATH

                ;;
        *)
                echo "Paramters download, install and clean"
esac
 
 
#################################################################
echo "##########################################################"
echo "                        END "
echo "##########################################################"
echo ""
#################################################################
## @}
