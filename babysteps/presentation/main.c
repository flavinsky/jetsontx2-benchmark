#include "omp.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define NX    1024
#define NY    1024
#define NZ    1024

#define REP 1
# define CACHE_SIZE_KB 327700


double alpha;
double beta;

/* Timer code (gettimeofday). */
double t_start, t_end;

void flush_cache()
{
  int cs = CACHE_SIZE_KB * 1024 / sizeof(double);
  double* flush = (double*) calloc (cs, sizeof(double));
  int i;
  double tmp = 0.0;
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (i = 0; i < cs; i++)
    tmp += flush[i];
#pragma omp target data map(tofrom:flush[0:cs])
#pragma omp target teams distribute parallel for
  for (i = 0; i < cs; i++)
    tmp += flush[i];
  assert (tmp <= 10.0);
  free (flush);
}


static double rtclock(){
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0)
      printf ("Error return from gettimeofday: %d", stat);
    return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
}

static void timer_start()
{
    flush_cache();
    t_start = rtclock ();
}


static void timer_stop()
{
  t_end = rtclock ();
}


static void timer_print(){
      printf (", %0.6f", t_end - t_start);
}

static void init_array( double *alpha,
                        double *beta,
                        double C[NX][NY],
                        double A[NX][NZ],
                        double B[NZ][NY])
{
    int i, j;

    *alpha = 32412;
    *beta = 2123;
    for (i = 0; i < NX; i++)
        for (j = 0; j < NY; j++)
            C[i][j] = ((double) i*j) / NX;
    for (i = 0; i < NY; i++)
        for (j = 0; j < NZ; j++)
            A[i][j] = ((double) i*j) / NX;
    for (i = 0; i < NZ; i++)
        for (j = 0; j < NY; j++)
            B[i][j] = ((double) i*j) / NX;
}

static void kernel_gemm( double alpha,
                         double beta,
                         double C[NX][NY],
                         double A[NX][NZ],
                         double B[NZ][NY])
{
    
    /* C := alpha*A*B + beta*C */
//#pragma omp target data map(from:C[0:NX][0:NY]) map(to:B[0:NZ][0:NY], A[0:NX][0:NY], alpha, beta)
    {
#pragma omp target parallel for map(from:C[0:NX][0:NY]) map(to:B[0:NZ][0:NY], A[0:NX][0:NY], alpha, beta)
//#pragma omp parallel for
//        collapse(2) \
//        schedule(static,1) \
//        num_teams((NX*NY+1192-1)/1192) \
//        num_threads(1192)
        for (int i = 0; i < NX; i++){
//#pragma omp parallel for schedule(static, 1) num_threads(1024)
            for (int j = 0; j < NY; j++)
            {
                
                //int team_num = omp_get_team_num();
                //int thread_num = omp_get_thread_num();
                //int num_thread = omp_get_num_threads();
                //int num_teams = omp_get_num_teams();
                //int index = team_num * num_thread + thread_num;
                //printf("Team %d, Thread %d, Index: %d, num_threads: %d, num_teams: %d\n",team_num, thread_num,index, num_thread, num_teams);
                C[i][j] *= beta;
                for (int k = 0; k < NZ; ++k)
                    C[i][j] += alpha * A[i][k] * B[k][j];
            }
        }
    }
}




int main(int argc, char* argv[]) {

    double (*C)[NX][NY];
    double (*C_target)[NX][NY];

    C = (double(*)[NX][NY]) malloc (NX*NY * sizeof(double));
    //int ret = cudaMallocManaged(&C, NX*NY*sizeof(double), cudaMemAttachGlobal);
    //int ret = cudaHostAlloc(&C, NX*NY*sizeof(double), cudaHostAllocMapped);
    //if( cudaHostGetDevicePointer((void **) &C_target, C, 0) != cudaSuccess ){
    //    fprintf(stderr, "Could not get device pointer C: cudaHostGetDevicePointer\n");
    //    exit(-1);
    //}
    
    double (*A)[NX][NZ]; 
    double (*A_target)[NX][NZ]; 
    A = (double(*)[NX][NZ]) malloc (NX*NZ * sizeof(double));
    //ret = cudaMallocManaged(&A, NX*NZ*sizeof(double), cudaMemAttachGlobal);
    //ret = cudaHostAlloc(&A, NX*NY*sizeof(double), cudaHostAllocMapped);
    //if( cudaHostGetDevicePointer((void **) &A_target, A, 0) != cudaSuccess ){
    //    fprintf(stderr, "Could not get device pointer A: cudaHostGetDevicePointer\n");
    //    exit(-1);
    //}
    
    double (*B)[NZ][NY]; 
    double (*B_target)[NZ][NY]; 
    B = (double(*)[NZ][NY]) malloc (NZ*NY * sizeof(double));
    //ret =  cudaMallocManaged(&B, NZ*NY*sizeof(double), cudaMemAttachGlobal);
    //ret = cudaHostAlloc(&B, NX*NY*sizeof(double), cudaHostAllocMapped);
    //if( cudaHostGetDevicePointer((void **) &B_target, B, 0) != cudaSuccess ){
    //    fprintf(stderr, "Could not get device pointer B: cudaHostGetDevicePointer\n");
    //    exit(-1);
    //}

    init_array(&alpha, &beta, *C, *A, *B);

    for(int j = 0; j < REP; j++){
        timer_start();
        kernel_gemm(alpha, beta, *C, *A, *B);
        timer_stop();
        timer_print();
    }
        
    return 0;
}
