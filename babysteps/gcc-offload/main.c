#include <stdio.h>
#include <omp.h>

int main() {
    int nof_dev = 0;
	int A[1] = {-1};
    
    nof_dev = omp_get_num_devices();
    printf("Num. Devices: %d\n",nof_dev);
	
    #pragma omp target
	{
  		A[0] = omp_is_initial_device();
	}

	if (!A[0]) {
  		printf("Able to use offloading!\n");
	}
    return 0;
}
