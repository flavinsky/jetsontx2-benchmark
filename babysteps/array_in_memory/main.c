#include <stdio.h>

#define SIZE_X 10
#define SIZE_Y 10
#define SIZE_Z 10

void foo (int * bar, size_t barSizeX, size_t barSizeY){
    for(int i = 0; i < barSizeX; i++){
        for( int j = 0; j < barSizeY; j++){
            printf("BAR[%d][%d]: %d ", i,j,*(bar + i*barSizeX + j));
        }
        printf("\n");
    }

}

int main (int argc, char * argv[]){

    int data[SIZE_X][SIZE_Y];
    int data2[SIZE_X][SIZE_Y][SIZE_Z];
    
    for(int i = 0; i < SIZE_X; i++){
        for( int j = 0; j < SIZE_Y; j++){
            for(int k = 0; k < SIZE_Z; k++){
                data2[i][j][k] = i*100 + j*10 + k; 
            }
        }
    }
    for(int i = 0; i < SIZE_X; i++){
        for( int j = 0; j < SIZE_Y; j++){
            data[i][j] = i*10 + j; 
        }
    }
    
    foo((int *)data, SIZE_X, SIZE_Y);
}
