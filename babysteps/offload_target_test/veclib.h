#include <stdlib.h>

double dot_product_parallel(double a[], double b[], size_t n);
double dot_product_normal(double a[], double b[], size_t n);
