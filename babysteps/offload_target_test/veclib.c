#include "veclib.h"
#include <omp.h>
#include <stdio.h>

double dot_product_parallel(double a[], double b[], size_t n){
	double sum = 0.0;

    # pragma omp target data  map(to:a[0:n], b[0:n]) map(tofrom:sum)
    {

        # pragma omp target teams distribute parallel for reduction(+:sum) map(tofrom:sum)
        for(int i = 0; i < n; i++){
            sum += (a[i] * b[i]);
        }
    }

	return sum;
}

double dot_product_normal(double a[], double b[], size_t n){
    	double sum = 0.0;
	size_t i;
	
	for (i = 0; i < n; i++) {
		sum += (a[i] * b[i]);
	}

	return sum;
}
