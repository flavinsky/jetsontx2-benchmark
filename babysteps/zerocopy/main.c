#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdbool.h>

#define NOF_ELEM 10000000ul
#define NUM_THREADS 256

#define USE_CUDA 1


/**
 * Allocate ZeroCopy mapped memory, shared between CUDA and CPU.
 * @ingroup util
 */
static bool cudaAllocMapped( void** cpuPtr, void** gpuPtr, size_t size )
{
	if( !cpuPtr || !gpuPtr || size == 0 )
		return false;

	if( cudaHostAlloc(cpuPtr, size, cudaHostAllocMapped) != cudaSuccess)
		return false;

	if( cudaHostGetDevicePointer(gpuPtr, *cpuPtr, 0) != cudaSuccess )
		return false;

	printf("[cuda]  cudaAllocMapped %zu bytes, CPU %p GPU %p\n", size, *cpuPtr, *gpuPtr);
	return true;
}

int main(int argc, char* argv[]){
    #if USE_CUDA==0
	int * A_target = omp_target_alloc(sizeof(int) * NOF_ELEM, 0);
	int * A_host = malloc(sizeof(int) * NOF_ELEM);
    if(A_target == NULL){
        fprintf(stderr, "Could not alloc data: malloc\n");
        exit(-1);
    }
	int res = omp_target_associate_ptr(A_host, A_target,
     sizeof(int) * NOF_ELEM, 0, 0);
	if(res){
		fprintf(stderr, "Could not associate pointers\n");
		exit(-1);
	}
#else
    cudaSetDeviceFlags(cudaDeviceMapHost);
	int* A_target = NULL; //omp_target_alloc(sizeof(int) * NOF_ELEM, 0);
    int* A_host = NULL;
    //cudaMallocManaged(&A_target, sizeof(int) * NOF_ELEM, cudaMemAttachGlobal);
    cudaAllocMapped(&A_host, &A_target, sizeof(int)*NOF_ELEM);
    if(A_target == NULL || A_host == NULL){
        fprintf(stderr, "Could not alloc data: cudamalloc\n");
        exit(-1);
    }
#endif /*USE_CUDA*/

    int num = omp_get_num_devices();
    printf("Num devices: %d\n", num);
#if USE_CUDA==0
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_threads(NUM_THREADS) \
    num_teams((NOF_ELEM*NUM_THREADS+1)/NUM_THREADS) 
    for(int i = 0; i < NOF_ELEM; i++){
        A_host[i] = i;
    }
#pragma omp target update from(A_host[0:NOF_ELEM])
#else
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_threads(NUM_THREADS) \
    num_teams((unsigned long)(NOF_ELEM*NUM_THREADS+1)/NUM_THREADS) \
	is_device_ptr(A_target)
    for(int i = 0; i < NOF_ELEM; i++){
        A_target[i] = i;
    }
#endif /*USE_CUDA*/

    //printf("A_target %u\n",*A_target);
/*
#pragma omp target data map(from: A_host[0:NOF_ELEM])
#pragma omp target teams distribute parallel for \
    schedule(static, 1) \
    num_threads(NUM_THREADS) \
    num_teams((NOF_ELEM*NUM_THREADS+1)/NUM_THREADS)
    for(int i = 0; i < NOF_ELEM; i++){
        A_host[i]=i;
    }
*/
    
    for(int i = 0; i < NOF_ELEM; i+=100000){
        printf("A[%u]: %u\n",i, A_host[i]);
    }
    

 //   free(A_host);
    omp_target_free(A_target, 0);
    return 0;
}
