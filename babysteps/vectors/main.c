#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "veclib.h"
#include <math.h>
#include <omp.h>

int main() {
    double start,end;
    double result;

    double *x;
    double *y;
    int i;
    int n = 100;
    double factor;

    while ( n < 100000000 )
        {
             n = n * 10;

             x = ( double * ) malloc ( n * sizeof ( double ) );
             y = ( double * ) malloc ( n * sizeof ( double ) );

             factor = ( double ) ( n );
             factor = 1.0 / sqrt ( 2.0 * factor * factor + 3 * factor + 1.0 );

             for ( i = 0; i < n; i++ )
             {
               x[i] = ( i + 1 ) * factor;
             }

             for ( i = 0; i < n; i++ )
             {
               y[i] = ( i + 1 ) * 6 * factor;
             }

             start = omp_get_wtime();
             result = dot_product_normal(x, y, n);
             end = omp_get_wtime();            
             printf("=====\n");
             printf("Length of N: %d\n",n);
             printf("Size of Vectors: %lu[kB]\n",n*sizeof(double)/1000);
             printf("Normal dot product\n");
             printf("Result %lf\n",result);
             printf("time = %lfs\n", (end - start));
            
             printf("----\n");
 
             printf("Parallel dot product\n");
             start = omp_get_wtime();
             result = dot_product_parallel(x, y, n);
             end = omp_get_wtime();

             printf("Result %lf\n",result);
             printf("time = %lfs\n", (end - start));

             printf("----\n");
             free(x);
             free(y);
        }

    return 0;
}
