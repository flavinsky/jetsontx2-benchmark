#include "veclib.h"
#include <omp.h>
#include <stdio.h>

double dot_product_parallel(double a[], double b[], size_t n){
	double sum = 0.0;
	size_t i;
	# pragma omp parallel for reduction(+:sum)
	for (i = 0; i < n; i++) {
		//printf("%d %d\n", omp_get_thread_num(), i);
		sum += a[i] * b[i];
	}
	return sum;
}

double dot_product_normal(double a[], double b[], size_t n){
    	double sum = 0.0;
	size_t i;
	
	for (i = 0; i < n; i++) {
		sum = sum + (a[i] * b[i]);
	}

	return sum;
}
