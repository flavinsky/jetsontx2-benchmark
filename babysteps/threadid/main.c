#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h>

#define N (64)
#define DATA_TYPE unsigned long
#define NUM_THREAD 32

int main() {
    int nof_dev = omp_get_num_devices();
    printf("Num. Devices: %d\n",nof_dev);
	int A[1] = {-1};
/*	
    #pragma omp target
	{
  		A[0] = omp_is_initial_device();
	}

	if (!A[0]) {
  		printf("Able to use offloading!\n");
	}else{
        printf("A: %d\n",A[0]);
    }
    */
#if 1
    DATA_TYPE B[N];
    for(int p = 0; p<N; p++){
        B[p]=p;
    }
    int num_team = (N+NUM_THREAD-1)/NUM_THREAD;
    printf("NUM teams: %d\n",num_team);
#pragma omp target teams distribute parallel for \
    schedule(static,1) \
    num_teams(num_team) \
    num_threads(NUM_THREAD) \
    map(to:B[0:N])
    for(int i=0; i<N; i++){

        int team_num = omp_get_team_num();
        int thread_num = omp_get_thread_num();
        int index = team_num * NUM_THREAD + thread_num;
        printf("Team %d, Thread %d, Index: %d - B[index]: %lu\n",team_num, thread_num,i, B[i]);
#if 0
            int team_num = omp_get_team_num();
            int thread_num = omp_get_thread_num();
            int index = team_num * NUM_THREAD + thread_num;
            int stride = NUM_THREAD * num_team;
            int itr = 0;
            for(int s = index; s < N ; s += stride){
                printf("Team %d, Thread %d, Index: %d, itr: %d - B[index]: %d\n",team_num, thread_num,s,itr, B[s]);
                itr++;
            }
#endif
        }
#endif
    return 0;
}
