#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define NANOSECONDS_PER_SECOND (1E9)

int main() {
    //double start,end;

    int trials = 1000000, count_par = 0, count_seq=0;
    time_t t;

    struct timespec requestStart, requestEnd;
    double elapsed_s;


    int nofChunks = omp_get_max_threads();
    int chunkSize = trials/nofChunks;
   
    printf("nofChunks: %d, chunkSize: %d, trials:%d\n",nofChunks, chunkSize, nofChunks*chunkSize);
    
    //Give rand() a seed value
    srand48(((unsigned)time(&t)));
    
    clock_gettime(CLOCK_MONOTONIC, &requestStart);
    
    
    #pragma omp parallel for shared(nofChunks, chunkSize) reduction(+:count_par)
    for(int j = 0; j < nofChunks; j++){
        
        int count_thr = 0;
        
        for(int i = 0; i < chunkSize; i++) {
       	    double x_par = (double) drand48();// / RAND_MAX;
       	    double y_par = (double) drand48();// / RAND_MAX;
       	
            if(x_par*x_par + y_par*y_par <=1)
                 ++count_thr;
        }

        //printf("Thread %d has finished chunk of size %d\n",omp_get_thread_num(), count_thr);
        #pragma omp critical
        count_par += count_thr;
    }

    clock_gettime(CLOCK_MONOTONIC, &requestEnd);
    elapsed_s = ( requestEnd.tv_sec - requestStart.tv_sec )
          + ( requestEnd.tv_nsec - requestStart.tv_nsec )
            / NANOSECONDS_PER_SECOND;

    printf("Parallel:\n");
    printf("Estimate of pi= %f\n", 4.0 * count_par / trials);
    printf("True value of pi = 3.141593\n");
    printf("time = %lfs\n", elapsed_s);
    
    
    clock_gettime(CLOCK_MONOTONIC, &requestStart);
    
    for(int j = 0; j < trials; j++) {
        double x_seq = (double) drand48();// / RAND_MAX;
        double y_seq = (double) drand48(); // / RAND_MAX;

        if(x_seq * x_seq + y_seq * y_seq <=1)
            ++count_seq;
    }
    clock_gettime(CLOCK_MONOTONIC, &requestEnd);

    
    elapsed_s = ( requestEnd.tv_sec - requestStart.tv_sec )
          + ( requestEnd.tv_nsec - requestStart.tv_nsec )
            / NANOSECONDS_PER_SECOND;


    printf("Sequential:\n");
    printf("Estimate of pi= %f\n", 4.0 * count_seq / trials);
    printf("True value of pi = 3.141593\n");
    printf("time = %lfs\n", elapsed_s);

    return 0;
}
