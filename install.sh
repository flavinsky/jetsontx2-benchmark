#!/bin/sh

#
# Build GCC with support for offloading to NVIDIA GPUs.
#
set -e

work_dir=$PWD/offload2/wrk
install_dir=$PWD/offload2/install

# Location of the installed CUDA toolkit
cuda=/usr/local/cuda

export PATH=$cuda:$cuda/bin:$PATH

echo "Install dir: $install_dir"
echo "CUDA dir: $cuda"
echo "Work dir: $work_dir"

# Build assembler and linking tools
mkdir -p $work_dir
cd $work_dir
# Build binutils
mkdir build-binutils
cd  build-binutils
../binutils/configure \
    --prefix=$install_dir/binutils \
    --enable-gold \
	--enable-ld=default \
    --enable-plugins    \
    --enable-shared     \
    --disable-werror    \
    --enable-64-bit-bfd \
    --with-system-zlib

make -j6
make install

# Set PATH and LD LIBRARY PATH
export PATH=$install_dir/binutils/bin/:$PATH
export LD_LIBRARY_PATH=$install_dir/binutils/lib:$LD_LIBRARY_PATH
cd ..

cd nvptx-tools
./configure \
    --with-cuda-driver-include=$cuda/include \
    --with-cuda-driver-lib=$cuda/lib64 \
	--prefix=$install_dir
make
make install
cd ..

# Set up the GCC source tree
cd gcc
contrib/download_prerequisites
ln -s ../nvptx-newlib/newlib newlib
cd ..

target=$(gcc/config.guess)

# Build nvptx GCC
mkdir build-nvptx-gcc
cd build-nvptx-gcc
../gcc/configure \
    --target=nvptx-none \
    --with-build-time-tools=$install_dir/nvptx-none/bin \
    --enable-as-accelerator-for=$target \
    --with-newlib \
    --disable-sjlj-exceptions \
    --enable-newlib-io-long-long \
    --enable-languages="c,c++,fortran,lto" \
	--prefix=$install_dir

make -j6
make install
cd ..



# Build host GCC
mkdir build-host-gcc
cd  build-host-gcc
../gcc/configure \
    --build=$target \
    --host=$target \
    --target=$target \
    --enable-offload-targets=nvptx-none=$install_dir \
    --with-cuda-driver-include=$cuda/include \
    --with-cuda-driver-lib=$cuda/lib64 \
    --disable-bootstrap \
    --disable-multilib \
    --enable-plugins \
    --enable-languages="c,c++,fortran,lto" \
	--prefix=$install_dir
make -j6
make install
cd ..
