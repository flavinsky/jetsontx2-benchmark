#!/bin/sh

#
# Build GCC with support for offloading to NVIDIA GPUs.
#

work_dir=$PWD/offload2/wrk

# Build assembler and linking tools
mkdir -p $work_dir
cd $work_dir
git clone https://github.com/MentorEmbedded/nvptx-tools

# Set up the GCC source tree
git clone https://github.com/MentorEmbedded/nvptx-newlib

wget https://ftp.gnu.org/gnu/gcc/gcc-7.3.0/gcc-7.3.0.tar.gz
tar -xf gcc-7.3.0.tar.gz
mv gcc-7.3.0 gcc

wget https://ftp.gnu.org/gnu/binutils/binutils-2.30.tar.gz
tar -xf binutils-2.30.tar.gz
mv binutils-2.30 binutils
